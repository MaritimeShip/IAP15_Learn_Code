/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "onewire.h"
#include "iic.h"

/* 变量创建区 */
//模版变量
unsigned int Slow_Down;                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag;       //按键数码管减速专用
unsigned char Key_UP, Key_Down;         //按键捕获上升下降沿
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};        //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};              //数码管小数点显示缓冲区
unsigned char usLED[8] = {0, 0, 0, 0, 0, 0, 0, 0};                //LED显示缓冲区
//题目变量
float T, dat;                                     //接收实时温度, DAC输出电压
unsigned char TMAX = 30, TMIN = 20;               //温度上下限
unsigned char TMAX_temp = 30, TMIN_temp = 20;     //温度上下限中间变量
unsigned char Key_T_Mode = 1;                     //0 --> TMAX_temp   1 --> TMIN_temp
unsigned char Seg_Disp_Mode;                      //数码管显示界面   0 --> 数据显示界面   1 --> 参数设置界面
/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Val = 0, Key_Old = 0;  //按键获取键码
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                              //按键减速
    
    Key_Val = Key_Read();                           //获取键码
    Key_UP = ~Key_Val & (Key_Val ^ Key_Old);        //捕获上升下降沿
    Key_Old = Key_Val;                              //辅助扫描
    
    switch (Key_UP)//S4, S5
    {
        //数码管显示界面切换
        case 4: Seg_Disp_Mode ^= 1; break;
        //上下限参数切换
        case 5: Key_T_Mode ^= 1; break;
    }
    
    if (Seg_Disp_Mode)//S6, S7
    {
        switch (Key_UP)
        {
            //参数加
            case 6:
                if (Key_T_Mode == 0) 
                {
                    if(++TMAX_temp == 100)
                        TMAX_temp = 0;
                }
                else
                {
                    if(++TMIN_temp == 100)
                        TMIN_temp = 0;
                }
            break;
            //参数减
            case 7:
                if (Key_T_Mode == 0) 
                {
                    if(--TMAX_temp == 255)
                        TMAX_temp = 99;
                }
                else
                {
                    if(--TMIN_temp == 255)
                        TMIN_temp = 99;
                }
            break;
        }
    }
}

/* 数码管处理函数 */
void Seg_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;                  //数码管减速
    
    T = Tem_Read();
    
    switch (Seg_Disp_Mode)
    {
        //数据显示界面
        case 0:
            Key_T_Mode = 1;             //保证下次进入参数设置模式时默认为TMIN
            //参数合理保存
            if (TMAX_temp > TMIN_temp) 
            {
                TMAX = TMAX_temp;
                TMIN = TMIN_temp;
            }
            //参数不合理则复原
            else
            {
                TMAX_temp = TMAX;
                TMIN_temp = TMIN;
            }
            Seg_Buf[0] = 11;                            //显示C
            Seg_Buf[3] = 10;                            //熄灭
            Seg_Buf[4] = 10;                            //熄灭
            Seg_Buf[6] = (unsigned char)T / 10 % 10;    //显示温度十位
            Seg_Buf[7] = (unsigned char)T % 10;         //显示温度个位
        break;
        //参数设置界面
        case 1:
            Seg_Buf[0] = 12;                            //显示P
            Seg_Buf[3] = TMAX_temp / 10;                //显示上限十位
            Seg_Buf[4] = TMAX_temp % 10;                //显示上限个位
            Seg_Buf[6] = TMIN_temp / 10;                //显示下限十位
            Seg_Buf[7] = TMIN_temp % 10;                //显示下限个位
        break;
    }
}

/* LED处理函数 */
void LED_Proc()
{
    if (T > TMAX)
    {
        DA_Write(4 * 51);
        usLED[0] = 1;
        usLED[1] = 0;
        usLED[2] = 0;
    }
    else if (T < TMIN)
    {
        DA_Write(2 * 51);
        usLED[0] = 0;
        usLED[1] = 0;
        usLED[2] = 1;
    }
    else 
    {
        DA_Write(3 * 51);
        usLED[0] = 0;
        usLED[1] = 1;
        usLED[2] = 0;
    }
    if (TMAX_temp > TMIN_temp)
        usLED[3] = 0;
    else
        usLED[3] = 1;
}

/* 定时器初始化函数 */
void Timer0Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag =0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Disp(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Disp(Slow_Down%8, usLED[Slow_Down%8]);
}

/* 延时函数 */
void Delay_ms(unsigned int xms)		//@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 主函数 */
void main()
{
    Tem_Read();
    Delay_ms(750);
    Timer0Init();
    System_Init();
    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}





