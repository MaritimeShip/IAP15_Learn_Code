#include <STC15F2K60S2.H>
#include "LED.h"


void LED_Show(unsigned char addr, enable)
{
    static unsigned char temp_new = 0x00;
    static unsigned char temp_old = 0xff;
    
    if (enable)
        temp_new |= 0x01 << addr;
    else
        temp_new &= ~(0x01 << addr);
    
    if (temp_new != temp_old)
    {
        P0 = ~temp_new;
        P2 = P2 & 0x1f | 0x80;
        P2 &= 0x1f;
        temp_old = temp_new;
    }
}


