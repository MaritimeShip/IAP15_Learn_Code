/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "ds1302.h"
#include "iic.h"
#include "onewire.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                        //减速扫描专用变量
bit Key_Slow_Flag, Seg_Slow_Flag, Infor_Slow_Flag;             //按键数码管信息获取减速专用变量
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};   //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};         //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};           //LED显示缓冲区
unsigned char i;                                               //循环专用变量

/* 变量创建区 */
unsigned char T_Temp_Show = 30, T_Temp_Ctrl = 30;//温度参数
float T[3];                                 //实时温度, 平均（最小）温度, 最大温度
float T_Old;                                //上次采集数据
float Hum[3] = {0, 10, 10};                 //实时湿度, 平均（最小）湿度, 最大温度
unsigned char RD1_New, RD1_Old;             //光敏电阻
unsigned int Fqre, Timer1_1000ms;           //实时频率
unsigned char Rtc_Buf[3] = {13, 3, 5};      //实时时钟数组
unsigned char usRtc[3] = {13, 3, 5};        //时钟数组
unsigned char Seg_Show_Mode;                //数码管显示模式   0-实时时钟  1-回显模式  2-参数界面
unsigned char Return_Show_Mode;             //回显模式   0-温度   1-湿度   2-时钟
unsigned char Num;                          //触发次数
bit T_H_Flag;                               //触发标志位
unsigned int Timer1_3000ms;                 //3000ms计时
bit S9_Flag, S9_OK;                         //S9按下标志位
unsigned int Timer1_2000ms;                 //2000ms计时
bit L4_Star_Flag, L4_Flag;                  //L4闪烁标志位
unsigned char Timer1_100ms;                 //100ms计时

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;//按键获取
    static unsigned char Key_UP = 0, Key_Down = 0;//专用变量
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                            //减速
    
    Key_New = Key_Read();                       //获取键码
    Key_UP = ~Key_New & (Key_New ^ Key_Old);    //上升沿捕获
    Key_Down = Key_New & (Key_New ^ Key_Old);   //下降沿捕获
    Key_Old = Key_New;                          //辅助扫描
    
    if (Seg_Show_Mode == 1 && Return_Show_Mode == 2)    //回显时钟
    {
        if (Key_Old == 9)
            S9_Flag = 1;
        else
            S9_Flag = Timer1_2000ms = 0;
    }
    
    switch (Key_Down)
    {
        //模式切换
        case 4:
            if (++Seg_Show_Mode == 3)   //模式切换
                Seg_Show_Mode = 0;
            Return_Show_Mode = 0;       //模式清0
            T_Temp_Ctrl = T_Temp_Show;  //数据更新
        break;
        //回显模式切换
        case 5:
            if (Seg_Show_Mode == 1) //回显模式
            {
                if (++Return_Show_Mode == 3)
                    Return_Show_Mode = 0;
            }
        break;
        //参数加
        case 8:
            if (Seg_Show_Mode == 2) //参数模式
            {
                if (++T_Temp_Show == 100)
                    T_Temp_Show = 99;
            }
        break;
        //参数减
        case 9:
            if (Seg_Show_Mode == 2) //参数模式
            {
                if (--T_Temp_Show == 255)
                    T_Temp_Show = 0;
            }
        break;
    }
}

/* 信息处理函数 */
void Infor_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    /* 非温湿度模式 */
    switch (Seg_Show_Mode)
    {
        //实时时钟
        case 0:
            Seg_Buf[0] = Rtc_Buf[0] / 10;              //时十位
            Seg_Buf[1] = Rtc_Buf[0] % 10;              //时个位
            Seg_Buf[2] = 11;                           //-
            Seg_Buf[3] = Rtc_Buf[1] / 10;              //分十位
            Seg_Buf[4] = Rtc_Buf[1] % 10;              //分个位
            Seg_Buf[5] = 11;                           //-
            Seg_Buf[6] = Rtc_Buf[2] / 10;              //秒十位
            Seg_Buf[7] = Rtc_Buf[2] % 10;              //秒个位
        break;
        //回显模式
        case 1:
            switch (Return_Show_Mode)
            {
                //温度回显
                case 0:
                    Seg_Buf[0] = 14;                               //显示C
                    Seg_Buf[1] = 10;                               //熄灭
                    Seg_Buf[2] = (unsigned char)(T[2]+0.5) / 10;   //最大温度十位
                    Seg_Buf[3] = (unsigned char)(T[2]+0.5) % 10;   //最大温度个位
                    Seg_Buf[4] = 11;                               //显示-
                    Seg_Buf[5] = (unsigned char)T[1] / 10;         //平均温度十位
                    Seg_Buf[6] = (unsigned char)T[1] % 10;         //平均温度个位
                    Seg_Point[6] = 1;                              //小数点
                    Seg_Buf[7] = (unsigned int)(T[1]*10)%10;       //平均温度小数1位
                    if (Num == 0)
                    {
                        Seg_Point[6] = 0;
                        for (i = 1; i < 8; i++)
                            Seg_Buf[i] = 10;
                    }
                break;
                //湿度回显
                case 1:
                    Seg_Buf[0] = 18;                                 //显示H
                    Seg_Buf[1] = 10;                                 //熄灭
                    Seg_Buf[2] = (unsigned char)(Hum[2]+0.5) / 10;   //最大温度十位
                    Seg_Buf[3] = (unsigned char)(Hum[2]+0.5) % 10;   //最大温度个位
                    Seg_Buf[4] = 11;                                 //显示-
                    Seg_Buf[5] = (unsigned char)Hum[1] / 10;         //平均温度十位
                    Seg_Buf[6] = (unsigned char)Hum[1] % 10;         //平均温度个位
                    Seg_Point[6] = 1;                                //小数点
                    Seg_Buf[7] = (unsigned int)(Hum[1]*10)%10;       //平均温度小数1位
                    if (Num == 0)
                    {
                        Seg_Point[6] = 0;
                        for (i = 1; i < 8; i++)
                            Seg_Buf[i] = 10;
                    }
                break;
                //时钟回显
                case 2:
                    Seg_Point[6] = 0;                          //小数点熄灭
                    Seg_Buf[0] = 17;                           //显示F
                    Seg_Buf[1] = Num / 10;                     //触发次数十位
                    Seg_Buf[2] = Num % 10;                     //触发次数个位
                    Seg_Buf[3] = usRtc[0] / 10;                //时十位
                    Seg_Buf[4] = usRtc[0] % 10;                //时个位
                    Seg_Buf[5] = 11;                           //-
                    Seg_Buf[6] = usRtc[1] / 10;                //分十位
                    Seg_Buf[7] = usRtc[1] % 10;                //分个位
                    if (Num == 0)
                    {
                        for (i = 3; i < 8; i++)
                            Seg_Buf[i] = 10;
                    }
                break;
            }
        break;
        //参数界面
        case 2:
            Seg_Point[6] = 0;                   //小数点熄灭
            Seg_Buf[0] = 19;                    //P
            for(i = 1; i < 6; i++)
                Seg_Buf[i] = 10;                //熄灭
            Seg_Buf[6] = T_Temp_Show / 10;      //参数十位
            Seg_Buf[7] = T_Temp_Show % 10;      //参数个位
            if (T_Temp_Show == 0)
                Seg_Buf[6] = 10;                //熄灭
        break;
    }
    
    /* 温湿度模式 */
    if (T_H_Flag == 1)
    {
        Seg_Point[6] = 0;                                   //小数点熄灭
        Seg_Buf[1] = Seg_Buf[2] = 10;                       //熄灭
        Seg_Buf[0] = 16;                                    //显示E
        Seg_Buf[3] = (unsigned char)T[0] / 10 % 10;         //温度十位
        Seg_Buf[4] = (unsigned char)T[0] % 10;              //温度个位
        Seg_B；；uf[5] = 11;                                    //显示-
        Seg_Buf[6] = (unsigned char)Hum[0] / 10 % 10;       //湿度十位
        Seg_Buf[7] = (unsigned char)Hum[0] % 10;            //湿度个位
        if (Hum[0] == -1)
            Seg_Buf[6] = Seg_Buf[7] = 12;                   //无效数据显示AA
    }
}

/* 其他处理函数 */
void Other_Deal()
{
    /* LED显示区 */
    //L1 L2 L3
    for (i = 0; i < 2; i++)
        LED_Buf[i] = (i == Seg_Show_Mode);
    if (T_H_Flag == 1)
        LED_Buf[2] = 1;
    else
        LED_Buf[2] = 0;
    //L4
    if (T_H_Flag == 1 && T[0] > T_Temp_Ctrl)
        L4_Flag = 1;
    else
        L4_Flag = L4_Star_Flag = Timer1_100ms = 0;
    LED_Buf[3] = L4_Star_Flag;
    //L5
    if (T[0] == -1)
        LED_Buf[4] = 1;
    else
        LED_Buf[4] = 0;
    //L6
    if (Num >= 2 && T_Old < T[0])
        LED_Buf[5] = 1;
    else
        LED_Buf[5] = 0;
    
}

/* 信息读取函数 */
void Infor_Read()
{
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 信息获取区 */
    DS1302_Read_Rtc(Rtc_Buf);          //时钟读取
    RD1_Old = RD1_New;                 //新旧替换
    RD1_New = AD_Read(0x41);           //光敏电阻
    
    
    if (RD1_New < 20 && RD1_Old >= 20) //由亮到暗
    {
        T_Old = T[0];
        T_H_Flag = 1;
        //时钟读取
        DS1302_Read_Rtc(usRtc);
        //温度读取
        T[2] = T[1] = DS18B20_Read_T();    
        //湿度读取
        if (200 <= Fqre && Fqre <= 2000)
            Hum[2] = Hum[1] = (8.0/180)*(Fqre-200) + 10;
    }
    
    
    if (T_H_Flag == 1)  //触发
    {
        //实时湿度计算
        if (200 <= Fqre && Fqre <= 2000)
            Hum[0] = (8.0/180)*(Fqre-200) + 10;
        else
            Hum[0] = -1;    //湿度无效
        //实时温度读取
        T[0] = DS18B20_Read_T();
        
        if (T[0] > T[2])        //最大温度
            T[2] = T[0];
        else if (T[0] < T[1])   //最小温度
            T[1] = T[0];
        
        if (Hum[0] > Hum[2])    //最大湿度
            Hum[2] = Hum[0];
        else if (Hum[0] < Hum[1] && Hum[0] != -1)//最小湿度
            Hum[1] = Hum[0];
    }
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0x7F;			//定时器时钟12T模式
	TMOD &= 0xF0;			//设置定时器模式
    TMOD |= 0x05;           //NE55
	TL0 = 0x00;				//设置定时初始值
	TH0 = 0x00;				//设置定时初始值
	TF0 = 0;				//清除TF0标志
	TR0 = 1;				//定时器0开始计时
}
void Timer1_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0xBF;			//定时器时钟12T模式
	TMOD &= 0x0F;			//设置定时器模式
	TL1 = 0x18;				//设置定时初始值
	TH1 = 0xFC;				//设置定时初始值
	TF1 = 0;				//清除TF1标志
	TR1 = 1;				//定时器1开始计时
    EA  = 1;
    ET1 = 1;
}

/* 中断服务函数 */
void Timer1_Sevre() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 200 == 0) Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    if (++Timer1_1000ms == 1000)
    {
        Fqre = (TH0 << 8) | TL0;
        TH0 = TL0 = Timer1_1000ms = 0;
    }
    
    if (T_H_Flag == 1)
    {
        if (++Timer1_3000ms == 3000)
        {
            if(++Num == 6) Num = 5;  //触发次数增加
            Timer1_3000ms = T_H_Flag = 0;
            T[1] = (T[1] + T[2]) / 2.0;         //温度平均
            Hum[1] = (Hum[1] + Hum[2]) / 2.0;   //湿度平均
        }
    }
    
    if (S9_Flag == 1)
    {
        if (++Timer1_2000ms == 2000)
        {
            Timer1_2000ms = 0;
            T[0] = T[1] = T[2] = Num = 0;
            Hum[1] = Hum[2] = 10;
            T_Temp_Show = T_Temp_Ctrl = 30;
        }
    }
    
    if (L4_Flag == 1)
    {
        if (++Timer1_100ms == 100)
        {
            Timer1_100ms = 0;
            L4_Star_Flag ^= 1;
        }
    }
}

/* 主函数 */
void main()
{
    /* 初始化函数区 */
    System_Init();
    Timer0_Init();
    Timer1_Init();
    DS18B20_Read_T();
    DS1302_Write_Rtc(Rtc_Buf);
    
    while (1)
    {
        if (T_H_Flag == 0)
            Key_Deal();
        Infor_Deal();
        Other_Deal();
        Infor_Read();
    }
}


