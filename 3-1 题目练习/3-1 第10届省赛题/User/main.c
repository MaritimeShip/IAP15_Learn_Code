/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "iic.h"

/* 模版变量声明区 */
unsigned int Slow_Down;                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag;       //按键数码管减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管数字显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char usLED[8] = {0, 0, 0, 0, 0, 0, 0, 0};                      //LED显示缓冲区
/* 题目变量声明区 */
unsigned char Seg_Disp_Mode;            //数码管显示模式   0 --> 频率显示界面   1 --> 电压显示界面
bit DAC_Mode;                           //0 --> 输出2.0V   1 --> 输出AD_DA_dat
bit Seg_Control, LED_Control;           //数码管，LED功能控制  0 --> 开     1 --> 关
unsigned int Freq, Time1000ms;          //实时频率，1s计数
float AD_DA_dat;                        //DAC实时输出电压
/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Val = 0, Key_Old = 0;      //按键捕获
    static unsigned char Key_UP = 0, Key_Down = 0;      //键码专用
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                                  //按键减速
    
    Key_Val = Key_Read();                               //获取键码
    Key_UP = ~Key_Val & (Key_Val ^ Key_Old);            //捕获上升沿
    Key_Down = Key_Val & (Key_Val ^ Key_Old);           //捕获下降沿
    Key_Old = Key_Val;                                  //辅助扫描
    
    switch(Key_Down)
    {
        //控制数码管显示模式
        case 4: Seg_Disp_Mode ^= 1; break;
        //控制DAC输出模式
        case 5: DAC_Mode ^= 1; break;
        //LED 指示灯功能控制
        case 6: Seg_Control ^= 1; break;
        //数码管显示功能控制
        case 7: LED_Control ^= 1; break;
    }
}

/* 信息处理函数 */
void Seg_Proc()
{
    unsigned char i = 3;        //用于未使用熄灭
    
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;                     //减速
    
    AD_DA_dat = AD_Read(0x43);             //读取电压值

    switch (Seg_Disp_Mode)
    {
        //电压显示界面
        case 0:
            Seg_Buf[0] = 12;                                    //显示U
            Seg_Buf[3] = 10;                                    //熄灭4号
            Seg_Buf[4] = 10;                                    //熄灭5号
            Seg_Buf[5] = (unsigned char)AD_DA_dat % 10;         //显示个位
            Seg_Point[5] = 1;                                   //显示小数点
            Seg_Buf[6] = (unsigned char)(AD_DA_dat * 10) % 10;  //显示小数点1位
            Seg_Buf[7] = (unsigned int)(AD_DA_dat * 100) % 10;  //显示小数点2位
        break;
        //频率显示界面
        case 1:
            Seg_Buf[0] = 11;                        //显示F
            Seg_Buf[3] = Freq / 10000 % 10;         //万
            Seg_Buf[4] = Freq / 1000 % 10;          //千
            Seg_Buf[5] = Freq / 100 % 10;           //百
            Seg_Point[5] = 0;                       //熄灭小数点
            Seg_Buf[6] = Freq / 10 % 10;            //十
            Seg_Buf[7] = Freq % 10;                 //个
        
            while (Seg_Buf[i] == 0)                 //检测熄灭
            {
                Seg_Buf[i] = 10;
                if (++i == 7) break;
            }
//            for (i = 3; i < 7; i++)                 //检测熄灭
//            {
//                if (Seg_Buf[i] != 0)
//                    break;
//                else
//                    Seg_Buf[i] = 10;
//            }
        break;
    }
    
    
}

/* 其他处理函数 */
void LED_Proc()
{
    unsigned char i = 0;
    
    /* DAC相关 */
    if (DAC_Mode)
        DA_Write(AD_DA_dat * 51);    //输出电压
    else
        DA_Write(2 * 51);    //输出电压
    
    /* LED相关 */
    //  L1, L2
    for (i = 0; i < 2; i++)
        usLED[i] = (i == Seg_Disp_Mode);
    
    //    L3
    if (AD_DA_dat < 1.5 || (AD_DA_dat < 3.5 && AD_DA_dat >= 2.5))
        usLED[2] = 0;
    else
        usLED[2] = 1;
    
    //    L4
    if (Freq < 1000 || (Freq >= 5000 && Freq < 10000))
        usLED[3] = 0;
    else
        usLED[3] = 1;
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//0ms@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TMOD |= 0x05;			//设置定时器模式
    TL0 = 0x00;				//设置定时初始值
    TH0 = 0x00;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
}
void Timer1_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0xBF;			//定时器时钟12T模式
    TMOD &= 0x0F;			//设置定时器模式
    TL1 = 0x18;				//设置定时初始值
    TH1 = 0xFC;				//设置定时初始值
    TF1 = 0;				//清除TF1标志
    TR1 = 1;				//定时器1开始计时
    ET1 = 1;
    EA  = 1;
}

/* 中断服务函数 */
void Timer1_Serve() interrupt 3
{
    //减速控制
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    
    //数码管功能控制
    if (Seg_Control == 0)
        Seg_Disp(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    else
        Seg_Disp(Slow_Down%8, 10, 0);
    
    //LED功能控制
    if (LED_Control == 0)
        LED_Disp(Slow_Down%8, usLED[Slow_Down%8]);
    else
        LED_Disp(Slow_Down%8, 0);

    //频率计算
    if (++Time1000ms == 1000)   //1s触发一次，频率测量
    {
        Time1000ms = 0;
        Freq = (TH0 << 8) | TL0;
        TH0 = 0;
        TL0 = 0;
    }
}

/* 主函数 */
void main()
{
    System_Init();
    Timer0_Init();
    Timer1_Init();
    
    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}



    