#include <STC15F2K60S2.H>
#include "Key.h"


unsigned char Key_Read()
{
    unsigned char temp_17 = 0;
    P34 = 0; P35 = 1; P42 = 1; P44 = 1;
    if (P30 == 0) temp_17 = 19;
    if (P31 == 0) temp_17 = 18;
    if (P32 == 0) temp_17 = 17;
    if (P33 == 0) temp_17 = 16;
    P34 = 1; P35 = 0; P42 = 1; P44 = 1;
    if (P30 == 0) temp_17 = 15;
    if (P31 == 0) temp_17 = 14;
    if (P32 == 0) temp_17 = 13;
    if (P33 == 0) temp_17 = 12;
    P34 = 1; P35 = 1; P42 = 0; P44 = 1;
    if (P30 == 0) temp_17 = 11;
    if (P31 == 0) temp_17 = 10;
    if (P32 == 0) temp_17 = 9;
    if (P33 == 0) temp_17 = 8;
    P34 = 1; P35 = 1; P42 = 1; P44 = 0;
    if (P30 == 0) temp_17 = 7;
    if (P31 == 0) temp_17 = 6;
    if (P32 == 0) temp_17 = 5;
    if (P33 == 0) temp_17 = 4;
    return temp_17;
}
