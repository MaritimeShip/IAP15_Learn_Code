/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Uwave.h"
#include "iic.h"

/* 基础变量创建区 */
unsigned int Slow_Down_17;                                      //按键数码管减速专用变量
bit Key_Slow_Flag, Seg_Slow_Flag;                               //按键数码管减速标志位
unsigned char Seg_Buf[8] = {0, 10, 10, 10, 10, 10, 10, 10};     //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};          //数码管小数点缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};            //LED显示缓冲区
unsigned char i;                                                //循环专用变量

/* 变量创建区 */
unsigned char Uwave_D, AD_Data;                      //实时距离  AD数据 
unsigned char Seg_Show_Mode;                         //数码管显示模式   0-测距界面   1-参数界面   2-记录界面
unsigned char Seg_Num_Mode_Flag;                     //数码管参数界面模式值   0-按键模式    1-旋钮模式
unsigned char Num_Min = 10, Num_Max = 60;            //参数上下限专用变量
unsigned char Num_Min_Set = 10, Num_Max_Set = 60;    //参数上下限设置专用变量
unsigned char Warn_Num;                              //报警次数专用变量
bit S9_Down_Flag, S8_Down_Flag;                      //旋钮按下标志位  0-未按下   1-按下
unsigned char Time_100ms;                            //100ms计时专用
bit L8_Star_Flag;                                    //L8闪烁标志位
bit Warn_Flag_Old, Warn_Flag_New;                    //报警标志位   0-未报警   1-报警

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_Old = 0, Key_New = 0;
    static unsigned char Key_UP = 0, Key_Down = 0;
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();
    Key_UP = ~Key_New & (Key_New ^ Key_Old);
    Key_Down = Key_New & (Key_New ^ Key_Old);
    Key_Old = Key_New;
    
    switch (Key_Down)
    {
        //界面切换
        case 4:
            if(++Seg_Show_Mode == 3) Seg_Show_Mode = 0;
            S9_Down_Flag = S8_Down_Flag = 0;          //按下标志位清零
        break;
        //参数模式切换
        case 5:
            S9_Down_Flag = S8_Down_Flag = 0;          //按下标志位清零
            //参数模式
            if(Seg_Show_Mode == 1)
                Seg_Num_Mode_Flag ^= 1;
            //记录模式
            else if(Seg_Show_Mode == 2)
                Warn_Num = 0;
        break;
        //参数上限
        case 9:
            if(Seg_Show_Mode == 1)
            {
                //按键模式
                if (Seg_Num_Mode_Flag == 0)
                {
                    Num_Max_Set += 10;
                    if (Num_Max_Set == 100)
                        Num_Max_Set = 50;
                }
                //旋钮模式
                else
                {
                    S9_Down_Flag = 1;
                    S8_Down_Flag = 0;
                }
            }
            break;
        //参数下限
       case 8:
            if(Seg_Show_Mode == 1)
            {
                //按键模式
                if (Seg_Num_Mode_Flag == 0)
                {
                    Num_Min_Set += 10;
                    if (Num_Min_Set == 50)
                        Num_Min_Set = 0;
                }
                //旋钮模式
                else
                {
                    S8_Down_Flag = 1;
                    S9_Down_Flag = 0;
                }
            }
        break;
    }
}

/* 信息处理函数 */
void Infor_Deal()
{
    
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    /* 信息采集区 */
    Uwave_D = Uwave_Read();
    AD_Data = AD_Read(0x43) / 51;
        
    /* 信息显示区 */
    switch (Seg_Show_Mode)
    {
        //测距界面
        case 0:
            Seg_Buf[0] = 12;                        //显示A
            for (i = 1; i < 6; i++)
                Seg_Buf[i] = 10;                    //熄灭
            Seg_Buf[6] = Uwave_D / 10 % 10;         //显示距离十位
            Seg_Buf[7] = Uwave_D % 10;              //显示距离个位
        break;
        //参数界面
        case 1:
            Seg_Buf[0] = 18;                        //显示P
            Seg_Buf[1] = Seg_Num_Mode_Flag + 1;     //显示模式值
            Seg_Buf[2] = 10;                        //熄灭
            Seg_Buf[3] = Num_Min_Set / 10 % 10;     //显示下限十位
            Seg_Buf[4] = Num_Min_Set % 10;          //显示下限个位
            Seg_Buf[5] = 11;                        //显示-
            Seg_Buf[6] = Num_Max_Set / 10 % 10;     //显示上限十位
            Seg_Buf[7] = Num_Max_Set % 10;          //显示上限个位
        break;
        //记录界面
        case 2:
            Seg_Buf[0] = 16;                        //显示E
            for (i = 1; i < 7; i++)
                Seg_Buf[i] = 10;                    //熄灭
            if (Warn_Num <= 9)
                Seg_Buf[7] = Warn_Num;              //显示报警次数
            else
                Seg_Buf[7] = 11;                    //显示-
        break;
    }
    
}

/* 其他处理函数 */
void Other_Deal()
{
    /* 信息处理区 */
    //旋钮下限
    if (S8_Down_Flag)         
    {
        Num_Min_Set = AD_Data * 10;
        if (Num_Min_Set == 50)
            Num_Min_Set -= 10;
    }
    //旋钮上限
    else if (S9_Down_Flag)    
    {
        Num_Max_Set = AD_Data * 10 + 50;
        if (Num_Max_Set == 100)
            Num_Max_Set -= 10;
    }
    //参数保存
    if (Seg_Show_Mode != 1)
    {
        Num_Min = Num_Min_Set;
        Num_Max = Num_Max_Set;
    }
    //报警次数处理
    Warn_Flag_New = (Num_Min <= Uwave_D && Uwave_D <= Num_Max);//报警状态获取
    if (Warn_Flag_New == 0 && Warn_Flag_Old == 1)
    {
        Warn_Flag_Old = Warn_Flag_New;  //状态更新
        if (++Warn_Num == 10)           //卡住Warn_Num
            Warn_Num = 10;
    }
    else
        Warn_Flag_Old = Warn_Flag_New;  //状态更新
    
    /* LED显示 */
    //L1 L2 L3
    for (i = 0; i < 3; i++)
        LED_Buf[i] = (i == Seg_Show_Mode);
    //L8
    if (Num_Min <= Uwave_D && Uwave_D <= Num_Max)
        LED_Buf[7] = 1;
    else
        LED_Buf[7] = L8_Star_Flag ? 0 : 1;
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    EA  = 1;
    ET0 = 1;
}

/* 定时器中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down_17 == 400) Slow_Down_17 = Seg_Slow_Flag = 0;
    if (Slow_Down_17 % 10 == 0) Key_Slow_Flag = 0;
    Seg_Show(Slow_Down_17%8, Seg_Buf[Slow_Down_17%8], Seg_Point[Slow_Down_17%8]);
    LED_Show(Slow_Down_17%8, LED_Buf[Slow_Down_17%8]);
    
    if (++Time_100ms == 100)
    {
//        Warn_Flag_Old = Warn_Flag_New;
        Time_100ms = 0;
        L8_Star_Flag ^= 1;
    }
}

/* 主函数 */
void main()
{
    Init_17();       //蜂鸣器等初始化
    Timer0_Init();   //定时器0初始化
    while (1)
    {
        Key_Deal();
        Infor_Deal();
        Other_Deal();
    }
}



