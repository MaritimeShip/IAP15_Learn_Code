/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include <stdio.h>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Uwave.h"
#include "Uart.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用变量
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;       //按键数码管LED信息处理减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned char Dist;                     //实时距离
unsigned char Dist_Num = 30;            //距离参数
bit Seg_Show_Mode;                      //数码管显示模式   0-实时距离   1-距离参数
bit L3_Star_Flag, L3_Flag;              //L3闪烁标志位, L3启动标志位
unsigned char Time_200ms;               //定时器200ms计时

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;  //按键接收
    static unsigned char Key_UP = 0, Key_Down = 0;  //专用变量
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();                       //获取键码
    Key_UP = ~Key_New & (Key_New ^ Key_Old);    //获取上升沿
    Key_Down = Key_New & (Key_New ^ Key_Old);   //获取下降沿
    Key_Old = Key_New;                          //辅助扫描
    
    switch (Key_Down)
    {
        //按键切换
        case 4:
            Seg_Show_Mode ^= 1;
        break;
        //距离参数变实时距离
        case 8:
            if (Seg_Show_Mode == 0)
                Dist_Num = Dist;
        break;
        //参数加
        case 12:
            if (Seg_Show_Mode == 1)
                Dist_Num += 10;
        break;
        //参数减
        case 16:
            if (Seg_Show_Mode == 1)
            {
                Dist_Num -= 10;
                if (Dist_Num >= 245)
                    Dist_Num = 0;
            }
        break;
        //串口数据发送
        case 9:
            printf("Distance:%dcm\r\n", (unsigned int)Dist);
        break;
    }
    
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    Seg_Buf[0] = 12;        //显示U
    //实时距离
    if (Seg_Show_Mode == 0)
    {
        Seg_Buf[1] = 1;                         //模式1
        Seg_Buf[5] = Dist / 100 % 10;           //实时距离百位
        Seg_Buf[6] = Dist / 10 % 10;            //实时距离十位
        Seg_Buf[7] = Dist % 10;                 //实时距离个位
        if (Seg_Buf[5] == 0) Seg_Buf[5] = 10;   //高位熄灭
        if (Seg_Buf[5] == 10 && Seg_Buf[6] == 0) Seg_Buf[6] = 10;
    }
    else
    {
        Seg_Buf[1] = 2;                         //模式1
        Seg_Buf[5] = Dist_Num / 100 % 10;       //距离参数百位
        Seg_Buf[6] = Dist_Num / 10 % 10;        //距离参数十位
        Seg_Buf[7] = Dist_Num % 10;             //距离参数个位
        if (Seg_Buf[5] == 0) Seg_Buf[5] = 10;   //高位熄灭
        if (Seg_Buf[5] == 10 && Seg_Buf[6] == 0) Seg_Buf[6] = 10;
    }
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;
    
    //L1, L2  互斥点亮
    for (i = 0; i < 2; i++)
        LED_Buf[i] = (i == Seg_Show_Mode);
    //L3
    LED_Buf[2] = L3_Star_Flag;
}

/* 信息处理函数 */
void Infor_Deal()
{
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 信息读取区 */
    Dist = Uwave_Read();
    
    /* 信息处理区 */
    if (Dist_Num < Dist)
        L3_Flag = 1;
    else
        L3_Flag = Time_200ms = L3_Star_Flag = 0;
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1ms@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    EA  = 1;
    ET0 = 1;
}

/* 中断服务函数 */
void Timer0_Sevre() interrupt 1
{
    if (++Slow_Down == 300) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 200 == 0) Seg_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    //L3闪烁
    if (L3_Flag == 1 && ++Time_200ms == 200)
    {
        Time_200ms = 0;
        L3_Star_Flag ^= 1;
    }
}
void Uart_Serve() interrupt 4
{
    if (RI == 1)
    {
        RI = 0;
    }
}

/* 主函数 */
void main()
{
    System_Init();
    Timer0_Init();
    Uart_Init();
    
    while (1)
    {
        Key_Deal();
        Seg_Deal();
        LED_Deal();
        Infor_Deal();
    }
}

