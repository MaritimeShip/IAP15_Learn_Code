/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "iic.h"
#include "onewire.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag;                                       //按键数码管减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
bit Seg_Show_Mode;                //数码管显示模式   0-界面模式   1-输出界面
bit Seg_Mode_Set;                 //数码管界面模式设置   0-温度控制模式   1-光照度控制模式
float T;                          //实时温度
unsigned char AD_Light;           //光敏电阻数据
float DA_Data;                    //DA输出数据

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Down = 0, Key_UP = 0;  //变量
    static unsigned char Key_New = 0, Key_Old = 0;  //创建

    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                              //减速
    
    Key_New = Key_Read();                           //获取键码
    Key_Down = Key_New & ( Key_New ^ Key_Old);      //捕获下降沿
    Key_UP = ~Key_New & ( Key_New ^ Key_Old);       //捕获上升沿
    Key_Old = Key_New;                              //辅助扫描
    
    switch (Key_Down)
    {
        //模式切换
        case 4:
            if (Seg_Show_Mode == 0)
                Seg_Mode_Set ^=1;
        break;
        //界面切换
        case 5:
            Seg_Show_Mode ^= 1;
        break;
    }
}

/* 信息处理函数 */
void Infor_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;                              //减速
    
    /* 信息读取区 */
    T = DS18B20_Read_T();
    AD_Light = AD_Read(0x41);
    
    /* 信息显示区 */
    //界面模式
    if (Seg_Show_Mode == 0)
    {
        //温度控制模式
        if (Seg_Mode_Set == 0)
        {
            Seg_Buf[0] = 1;                         //显示1
            for (i = 1; i < 5; i++)
                Seg_Buf[i] = 10;                    //熄灭
            Seg_Buf[5] = (unsigned char)T / 10;     //温度十位
            Seg_Buf[6] = (unsigned char)T % 10;     //温度个位
            Seg_Point[6] = 1;                       //小数点
            Seg_Buf[7] = (unsigned int)(T*10) % 10; //温度小数1位
        }
        //光照度控制模式
        else
        {
            Seg_Buf[0] = 2;                         //显示2
            for (i = 1; i < 3; i++)
                Seg_Buf[i] = 10;                    //熄灭
            Seg_Buf[5] = AD_Light / 100 % 10;       //光照度百位
            Seg_Buf[6] = AD_Light / 10 % 10;        //光照度十位
            Seg_Point[6] = 0;                       //小数点熄灭
            Seg_Buf[7] = AD_Light % 10;             //光照度个位
            for (i = 5; i < 7; i++)                 //高位熄灭
            {
                if (Seg_Buf[i] == 0)
                    Seg_Buf[i] = 10;
                else
                    break;
            }
        }
    }
    //输出界面
    else
    {
        Seg_Buf[0] = 18;                                  //显示U
        for (i = 1; i < 6; i++)
            Seg_Buf[i] = 10;                              //熄灭
        Seg_Buf[6] = (unsigned char)DA_Data % 10;         //显示输出个位
        Seg_Point[6] = 1;                                 //小数点
        Seg_Buf[7] = (unsigned char)(DA_Data*10) % 10;    //显示输出个位
    }
}

/* 其他处理函数 */
void Other_Proc()
{
    /* 数据处理区 */
    //温度控制DA
    if (Seg_Mode_Set == 0)
    {
        if (T >= 10 && T <= 40)
            DA_Data = (4.0/30)*(T-10)+1;
        else if (T < 10)
            DA_Data = 1;
        else if (T > 40)
            DA_Data = 5;
    }
    //光照度控制DA
    else
    {
        if (AD_Light >= 10 && AD_Light <= 240)
            DA_Data = (4.0/230)*(AD_Light-10)+1;
        else if (AD_Light < 10)
            DA_Data = 1;
        else if (AD_Light > 240)
            DA_Data = 5;
    }
    /* 数据输出 */
    DA_Write(DA_Data * 51);
    
    /* LED显示 */
    for (i = 0; i < 2; i++)
        LED_Buf[i] = (i == Seg_Mode_Set);
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
}

/* 延时函数 */
void Delay_ms(unsigned int xms)		//@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 主函数 */
void main()
{
    Init();
    Timer0_Init();
    DS18B20_Read_T();
    Delay_ms(750);
    while (1)
    {
        Key_Proc();
        Infor_Proc();
        Other_Proc();
    }
}



