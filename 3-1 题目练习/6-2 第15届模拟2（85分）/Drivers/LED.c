#include <STC15F2K60S2.H>
#include "LED.h"


static unsigned char temp_new = 0x00;
static unsigned char temp_old = 0xFF;


void LED_Show(unsigned char addr, enable)
{
    if (enable)
        temp_new |= 0x01 << addr;
    else
        temp_new &= ~(0x01 << addr);
    
    if (temp_new != temp_old)
    {
        P0 = ~temp_new;
        P2 = P2 & 0x1f | 0x80;
        P2 &= 0x1f;
        temp_old = temp_new;
    }
}


void Beep(unsigned char flag)
{
    if (flag)
        temp_new |= 0x40;
    else
        temp_new &= ~(0x40);
    
    if (temp_new != temp_old)
    {
        P0 = temp_new;
        P2 = P2 & 0x1f | 0xa0;
        P2 &= 0x1f;
        temp_old = temp_new;
    }
}


void Relay(unsigned char flag)
{
    if (flag)
        temp_new |= 0x10;
    else
        temp_new &= ~(0x10);
    
    if (temp_new != temp_old)
    {
        P0 = temp_new;
        P2 = P2 & 0x1f | 0xa0;
        P2 &= 0x1f;
        temp_old = temp_new;
    }
}






