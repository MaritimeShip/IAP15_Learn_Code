/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "onewire.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag;       //按键数码管减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};        //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};              //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                //LED显示缓冲区
unsigned char i;                        //循环专用变量

/* 变量创建区 */
unsigned char T;                       //实时温度专用变量
bit Seg_Show_Mode;                     //数码管显示模式    0-工作模式   1-温度模式
unsigned char Seg_Work_Index;          //数码管工作模式指针  0-模式1  1-模式2  2-模式3
unsigned int Work_Count_Time;          //工作模式计时
unsigned int Time_1000ms;              //1000ms计时
unsigned char Time_1000us;             //1000us计时
unsigned char PWM;                     //PWM控制

/* 按键处理区 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;      //按键
    static unsigned char Key_UP = 0, Key_Down = 0;      //变量
    static unsigned char S5_Set;    //按键5计次
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                  //减速
    
    Key_New = Key_Read();                           //按键获取
    Key_UP = ~Key_New & (Key_New ^ Key_Old);        //上升沿
    Key_Down = Key_New & (Key_New ^ Key_Old);       //下降沿
    Key_Old = Key_New;                              //辅助扫描
    
    switch (Key_Down)
    {
        case 7:
            Seg_Show_Mode ^= 1;     //温度工作切换
        break;
        case 4:
            if (++Seg_Work_Index == 3)        //工作模式切换
                Seg_Work_Index = 0;
        break;
        case 5:
            if (++S5_Set == 3) S5_Set = Time_1000ms = 0;
            Work_Count_Time = S5_Set * 60;
        break;
        case 6:
            Work_Count_Time = S5_Set = Time_1000ms = 0;
        break;
    }
}

/* 信息处理区 */
void Infor_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    /* 信息获取区 */
    T = DS18B20_Read_T();
    
    /* 信息显示区域 */
    //工作模式
    Seg_Buf[0] = Seg_Buf[2] = 11;       //显示-
    if (Seg_Show_Mode == 0)
    {
        Seg_Buf[1] = Seg_Work_Index + 1;             //显示工作模式
        
        Seg_Buf[4] = Work_Count_Time / 1000 % 10;    //显示千位
        Seg_Buf[5] = Work_Count_Time / 100 % 10;     //显示百位
        Seg_Buf[6] = Work_Count_Time / 10 % 10;      //显示十位
        Seg_Buf[7] = Work_Count_Time % 10;           //显示个位
    }
    else
    {
        Seg_Buf[1] = 4;                  //显示4
        Seg_Buf[3] = Seg_Buf[4] = 10;    //熄灭
        Seg_Buf[5] = T / 10 % 10;        //显示温度十位
        Seg_Buf[6] = T % 10;             //显示温度个位
        Seg_Buf[7] = 14;                 //显示C
    }
}

/* 其他处理区 */
void Other_Deal()
{
    /* 信息处理区域 */
    if (Work_Count_Time)
    {
        if (Seg_Work_Index == 0) PWM = 2;   //睡眠风
        if (Seg_Work_Index == 1) PWM = 3;   //自然风
        if (Seg_Work_Index == 2) PWM = 7;   //常风
    }
    else
        PWM = 0;                            //PWM停止

    /* LED显示区域 */
    for (i = 0; i < 3; i++)
        LED_Buf[i] = (i == Seg_Work_Index);
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0x7F;			//定时器时钟12T模式
	TMOD &= 0xF0;			//设置定时器模式
	TL0 = 0x18;				//设置定时初始值
	TH0 = 0xFC;				//设置定时初始值
	TF0 = 0;				//清除TF0标志
	TR0 = 1;				//定时器0开始计时
    EA  = 1;
    ET0 = 1;
}
void Timer1_Init(void)		//100微秒@12.000MHz
{
	AUXR &= 0xBF;			//定时器时钟12T模式
	TMOD &= 0x0F;			//设置定时器模式
	TL1 = 0x9C;				//设置定时初始值
	TH1 = 0xFF;				//设置定时初始值
	TF1 = 0;				//清除TF1标志
	TR1 = 1;				//定时器1开始计时
    ET1 = 1;
}
/* 中断服务函数 */
void Timer0_Sevre() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    
    if (Work_Count_Time)
    {
        LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
        if (++Time_1000ms == 1000)
        {
            Work_Count_Time--;
            Time_1000ms = 0;
        }
    }
    else
        LED_Show(Slow_Down%8, 0);
}
void Timer1_Sevre() interrupt 3
{
    if (++Time_1000us == 10) Time_1000us = 0;
    if (Time_1000us <= PWM)
        P34 = 1;
    else
        P34 = 0;
    
}
    
/* 主函数 */
void main()
{
    System_Init();
    Timer0_Init();
    Timer1_Init();
    while (1)
    {
        Key_Deal();
        Infor_Deal();
        Other_Deal();
    }
}


