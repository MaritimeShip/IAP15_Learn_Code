/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "iic.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                     //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag;            //按键数码管减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10}; //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};      //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {1, 0, 0, 0, 0, 0, 0, 0};        //LED显示缓冲区
unsigned char i;                                            //循环专用变量

/* 变量创建区 */
unsigned char AD_LED_Light, PWM;                             //AD-LED亮度等级，PWM控制专用变量
bit Seg_Show_Mode;                                           //数码管显示模式 0-设置模式 1-亮度等级
unsigned int Roarm_Time_Show[4] = {400, 400, 400, 400};      //流转时间显示数组
unsigned int Roarm_Time_Set[4] = {400, 400, 400, 400};       //流转时间设置数组
unsigned char Roarm_Index;                                   //数码管模式单元指针  0-1  1-2  2-3  3-4
unsigned char Seg_Set_Index;                                 //数码管设置指针   0-参数保存  1-编号设置  2-流转时间
bit Seg_Star_Flag;                                           //数码管闪烁标志位
unsigned int Time_800ms;                                     //计时800ms
unsigned char EEPROM_Arr[4];                                 //EEPROM数组
bit LED_Flag;                                                //LED启动标志位 1-启动  0-暂停
unsigned char LED_Timer_Count;                               //LED定时器计数
unsigned int Ms_Tick;                                        //系统计时
unsigned char LED_Roarm_Index;                               //LED流转指针  0-模式1  1-模式2  2-模式3  3-模式4  
unsigned char LED_Pos;                                       //LED亮度设置

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;          //获取键码
    static unsigned char Key_UP = 0, Key_Down = 0;          //专用变量
    
    if (Key_Slow_Flag) return;                              //减速
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();                                   //获取键码
    Key_UP = ~Key_New & (Key_New ^ Key_Old);                //捕获上升沿
    Key_Down = Key_New & (Key_New ^ Key_Old);               //捕获下降沿
    Key_Old = Key_New;                                      //辅助扫描
    
    //亮度等级显示
    if (Seg_Set_Index == 0)
    {
        if (Key_Old == 4)
            Seg_Show_Mode = 1;
        else
        {
            Seg_Show_Mode = 0;
            Seg_Buf[6] = Seg_Buf[7] = 10;   //覆盖之前的数据
        }
    }
    
    switch (Key_Down)
    {
        case 6:
            if (++Seg_Set_Index == 3) 
                Seg_Set_Index = 0;
            if (Seg_Set_Index == 0)
            {
                Roarm_Index = 0;    //参数编号归1
                for (i = 0; i < 4; i++)
                {
                    EEPROM_Arr[i] = Roarm_Time_Show[i] / 100;
                    Roarm_Time_Set[i] = Roarm_Time_Show[i];//数据更新
                }
                EEPROM_Write(EEPROM_Arr, 0, 4);//参数保存
            }
        break;
        case 4:
             if (Seg_Set_Index == 1)
             {
                 if (--Roarm_Index == 255)
                    Roarm_Index = 0;
             }
             else if (Seg_Set_Index == 2)
             {
                 Roarm_Time_Show[Roarm_Index] -= 100;
                if (Roarm_Time_Show[Roarm_Index] < 400)
                    Roarm_Time_Show[Roarm_Index] = 400;
             }
        break;
        case 5:
             if (Seg_Set_Index == 1)
             {
                 if (++Roarm_Index == 4)
                    Roarm_Index = 3;
             }
             else if (Seg_Set_Index == 2)
             {
                 Roarm_Time_Show[Roarm_Index] += 100;
                if (Roarm_Time_Show[Roarm_Index] > 1200)
                    Roarm_Time_Show[Roarm_Index] = 1200;
             }
        break;
        case 7://启动暂停
            LED_Flag ^= 1;
        break;
    }
}

/* 信息处理函数 */
void Infor_Deal()
{
    if (Seg_Slow_Flag) return;            //减速
    Seg_Slow_Flag = 1;
    
    /* 信息获取区 */
    AD_LED_Light = AD_Read(0x43) / 85 + 1;     // 1  2  3  4
    
    /* 信息显示区 */
    //设置模式
    if (Seg_Show_Mode == 0 && Seg_Set_Index != 0)
    {
        Seg_Buf[3] = 10;                    //熄灭
        //编号
        Seg_Buf[0] = Seg_Buf[2] = 11;       //显示-
        Seg_Buf[1] = Roarm_Index + 1;       //编号显示
        //时间
        Seg_Buf[5] = Roarm_Time_Show[Roarm_Index] / 100 % 10;       //时间百位
        Seg_Buf[6] = Roarm_Time_Show[Roarm_Index] / 10 % 10;        //时间十位
        Seg_Buf[7] = Roarm_Time_Show[Roarm_Index] % 10;             //时间个位
        Seg_Buf[4] = (Roarm_Time_Show[Roarm_Index] >= 1000)?1:10;   //时间千位
        //闪烁
        if (Seg_Set_Index == 1) //编号闪烁
        {
            Seg_Buf[0] = Seg_Buf[2] = Seg_Star_Flag?10:11;       //显示-
            Seg_Buf[1] = Seg_Star_Flag?10:(Roarm_Index + 1);     //编号显示
        }
        else                    //时间闪烁
        {
            Seg_Buf[5] = Seg_Star_Flag?10:Roarm_Time_Show[Roarm_Index] / 100 % 10;       //时间百位
            Seg_Buf[6] = Seg_Star_Flag?10:Roarm_Time_Show[Roarm_Index] / 10 % 10;        //时间十位
            Seg_Buf[7] = Seg_Star_Flag?10:Roarm_Time_Show[Roarm_Index] % 10;             //时间个位
            Seg_Buf[4] = (Roarm_Time_Show[Roarm_Index] >= 1000 && Seg_Star_Flag == 0)?1:10;   //时间千位
        }
    }
    //亮度等级
    else if (Seg_Show_Mode == 1)    
    {
        for (i = 0; i < 6; i++)
            Seg_Buf[i] = 10;    //熄灭
        Seg_Buf[6] = 11;        // -
        Seg_Buf[7] = AD_LED_Light;//亮度等级
    }
    
}

/* 其他处理函数 */
void Other_Deal()
{
    /* LED处理 */
    if (LED_Flag == 1)
    {
        if (Roarm_Time_Set[LED_Roarm_Index] == Ms_Tick)
        {
            Ms_Tick = 0;
            switch(LED_Roarm_Index)
            {
                case 0:
                    if(++LED_Pos == 8)
                    {
                        LED_Pos = 7;
                        LED_Roarm_Index = 1;
                    }
                break;
                case 1:
                    if(--LED_Pos == 255)
                    {
                        LED_Pos = 7;
                        LED_Roarm_Index = 2;
                    }
                break;
                case 2:
                    LED_Pos += 9;
                    if(LED_Pos > 34)
                    {
                        LED_Pos = 34;
                        LED_Roarm_Index = 3;
                    }
                break;
                case 3:
                    LED_Pos -= 9;
                    if(LED_Pos > 200)
                    {
                        LED_Pos = 0;
                        LED_Roarm_Index = 0;
                    }
                break;
            }
        }
    }
    /* LED显示 */
    if (LED_Roarm_Index < 2)//12模式
    {
        for (i = 0; i < 8; i++)
            LED_Buf[i] = (i == LED_Pos);
    }
    else                    //34模式
    {
        for (i = 0; i < 8; i++)
            LED_Buf[i] = (i == (LED_Pos / 10) || i == (LED_Pos % 10));
    }
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 100 == 0) LED_Slow_Flag = 0;
    if (++LED_Timer_Count == 12) LED_Timer_Count = 0;
    if (LED_Flag == 1) Ms_Tick++;
    
    /* LED显示 */
    if (LED_Timer_Count <= AD_LED_Light * 3)
        LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    else
        LED_Show(Slow_Down%8, 0);
    
    /* 数码管显示 */
    if (Seg_Set_Index != 0 || Seg_Show_Mode == 1)
        Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    else
        Seg_Show(Slow_Down%8, 10, 0);    //数码管熄灭
    
    if (++Time_800ms == 800)
    {
        Time_800ms = 0;
        Seg_Star_Flag ^= 1;
    }
}

/* 主函数 */
void main()
{
    unsigned char PID = 0;
    System_Init();
    Timer0_Init();
    /* PID确认 */
    EEPROM_Read(&PID, 16, 1);
    if (PID == 66)  //PID正确
    {
        EEPROM_Read(EEPROM_Arr, 0, 4);
        for (i = 0; i < 4; i++)
            Roarm_Time_Show[i] = Roarm_Time_Set[i] = EEPROM_Arr[i] * 100;
    }
    else            //PID错误
    {
        PID = 66;
        EEPROM_Write(&PID, 16, 1);
    }
    
    
//    Timer1_Init();
    while (1)
    {
        Key_Deal();
        Infor_Deal();
        Other_Deal();
    }
}



