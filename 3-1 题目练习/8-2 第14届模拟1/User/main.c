/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include <string.h>
#include <stdio.h>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "iic.h"
#include "Uart.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                     //减速扫描专用变量
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag;            //按键，数码管，LED减速专用变量
bit Uart_Slow_Flag, Infor_Slow_Flag;                        //串口，信息减速专用变量
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};//数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};      //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};        //LED显示缓冲区
unsigned char i;                                            //循环专用变量

/* 变量创建区 */
float dB, AD_Data;                                   //噪音数据 ,AD读取数据
unsigned char dB_Temp_Show = 65, dB_Temp_Ctrl = 65;  //噪音参数
bit Seg_Show_Mode;                                   //数码管显示模式 0-显示界面  1-参数界面
unsigned char Uart_Rx_Buf[7];                        //串口接收数组
unsigned char Uart_Rx_Index;                         //串口接收指针
bit L8_Flag;                                         //L8标志位
bit L8_Star_Flag;                                    //L8闪烁标志位
unsigned int Timer0_100ms;                           //计时100ms

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;
    static unsigned char Key_UP = 0, Key_Down = 0;
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();
    Key_UP = ~Key_New & (Key_New ^ Key_Old);
    Key_Down = Key_New & (Key_New ^ Key_Old);
    Key_Old = Key_New;
    
    switch (Key_Down)
    {
        //数码管模式切换
        case 12:
            Seg_Show_Mode ^= 1;
        break;
        //参数加
        case 16:
            if (Seg_Show_Mode == 1)//参数界面
            {
                dB_Temp_Show += 5;
                if (dB_Temp_Show == 95)
                    dB_Temp_Show = 90;
            }
        break;
        //参数减
        case 17:
            if (Seg_Show_Mode == 1)//参数界面
            {
                dB_Temp_Show -= 5;
                if (dB_Temp_Show > 200)
                    dB_Temp_Show = 0;
            }
        break;
    }
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    Seg_Buf[0] = 18;
    //显示界面
    if (Seg_Show_Mode == 0)
    {
        Seg_Buf[1] = 1;                                 //显示1
        Seg_Buf[5] = (unsigned char)dB / 10 % 10;       //噪音数据十位
        Seg_Buf[6] = (unsigned char)dB % 10;            //噪音数据个位
        Seg_Point[6] = 1;                               //小数点
        Seg_Buf[7] = (unsigned int)(dB*10) % 10;       //噪音数据小数1位
    }
    //参数界面
    else
    {
        Seg_Buf[1] = 2;                             //显示1
        Seg_Buf[5] = 10;                            //熄灭
        Seg_Buf[6] = dB_Temp_Show / 10 % 10;             //噪音参数十位
        Seg_Point[6] = 0;                           //小数点熄灭
        Seg_Buf[7] = dB_Temp_Show % 10;                  //噪音参数个位
    }
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;
    
    //L1 L2
    for (i = 0; i < 2; i++)
        LED_Buf[i] = (i == Seg_Show_Mode);
    ///L8
    if (L8_Flag == 1)
        LED_Buf[7] = L8_Star_Flag;  //闪烁
    else
        LED_Buf[7] = 0;             //熄灭
}

/* 串口处理函数 */
void Uart_Deal()
{
    if (Uart_Slow_Flag) return;
    Uart_Slow_Flag = 1;
    
    if (Uart_Rx_Index == 6)
    {
        if (strcmp(Uart_Rx_Buf, (unsigned char*)"Return") == 0) //判断数组
        {
            printf("Noises:%.1fdB", dB);
        }
        Uart_Rx_Index = 0;
    } 
}

/* 信息处理函数 */
void Infor_Deal()
{
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 信息读取区 */
    AD_Data = AD_Read(0x43) / 51.0;
    
    /* 信息处理区 */
    //噪音数据
    dB = 18*AD_Data;
    //更新数据
    if (Seg_Show_Mode == 0)
        dB_Temp_Ctrl = dB_Temp_Show;
    //噪音参数
    if (dB_Temp_Ctrl < dB)
        L8_Flag = 1;
    else
        L8_Flag = 0;
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0x7F;			//定时器时钟12T模式
	TMOD &= 0xF0;			//设置定时器模式
	TL0 = 0x18;				//设置定时初始值
	TH0 = 0xFC;				//设置定时初始值
	TF0 = 0;				//清除TF0标志
	TR0 = 1;				//定时器0开始计时
    EA  = 1;
    ET0 = 1;
}

/* 中断服务函数 */
void Timer0_Sevre() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 200 == 0) Seg_Slow_Flag = 0;
    if (Slow_Down % 100 == 0) Uart_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    if (L8_Flag == 1)
    {
        if (++Timer0_100ms == 100)
        {
            Timer0_100ms = 0;
            L8_Star_Flag ^= 1;
        }
    }
    
}
void Uart_Sevre() interrupt 4
{
    if (RI == 1)
    {
        Uart_Rx_Buf[Uart_Rx_Index++] = SBUF;
        RI = 0;
    }
}
/* 延时函数 */
void Delay500ms()		//@12.000MHz
{
    unsigned char i, j, k;
    i = 23;
    j = 205;
    k = 120;
    do
    {
        do
        {
            while (--k);
        } while (--j);
    } while (--i);
}

/* 主函数 */
void main()
{
    System_Init();
    Timer0_Init();
    Uart_Init();
    AD_Data = AD_Read(0x43) / 51.0;
    Delay500ms();
    while (1)
    {
        Key_Deal();
        Seg_Deal();
        LED_Deal();
        Uart_Deal();
        Infor_Deal();
    }
}



