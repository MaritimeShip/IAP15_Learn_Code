/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "onewire.h"
#include "iic.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;       //按键数码管LED信息处理减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned char Seg_Show_Mode;                //数码管显示模式   0-信号界面  1-温度界面  2-参数界面
unsigned int Freq, Time_1000ms;             //实时频率，1000ms计时
float T, DA_Data;                           //实时温度, DA输出数据
unsigned char T_Show = 25, T_Set = 25;      //温度显示参数，温度控制参数
bit L8_Flag, L8_Star_Flag;                  //L8标志位，L8闪烁标志位
unsigned char Time_100ms;                   //100ms计时

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;  //按键获取
    static unsigned char Key_UP = 0, Key_Down = 0;  //专用变量
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();
    Key_UP = ~Key_New & (Key_New ^ Key_Old);
    Key_Down = Key_New & (Key_New ^ Key_Old);
    Key_Old = Key_New;
    
    switch (Key_Down)
    {
        //界面切换
        case 4:
            if (++Seg_Show_Mode == 3)
                Seg_Show_Mode = 0;
        break;
        //参数加
        case 9:
            if (Seg_Show_Mode == 2)
            {
                if (++T_Show == 36)
                    T_Show = 20;
            }
        break;
        //参数减
        case 8:
            if (Seg_Show_Mode == 2)
            {
                if (--T_Show == 19)
                    T_Show = 35;
            }
        break;
    }
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    Seg_Point[6] = (Seg_Show_Mode == 1);        //小数点控制
    switch (Seg_Show_Mode)
    {
        //信号界面
        case 0:
            Seg_Buf[0] = 12;                    //显示P
            Seg_Buf[3] = Freq / 10000 % 10;     //频率万位
            Seg_Buf[4] = Freq / 1000 % 10;      //频率千位
            Seg_Buf[5] = Freq / 100 % 10;       //频率百位
            Seg_Buf[6] = Freq / 10 % 10;        //频率十位
            Seg_Buf[7] = Freq % 10;             //频率个位
            for (i = 3; i < 7; i++)             //高位熄灭
            {
                if (Seg_Buf[i] == 0)
                    Seg_Buf[i] = 10;
                else
                    break;
            }
        break;
        //温度界面
        case 1:
            Seg_Buf[0] = 13;                            //显示C
            for (i = 1; i < 5; i++)                     //熄灭
                Seg_Buf[i] = 10;
            Seg_Buf[5] = (unsigned char)T / 10 % 10;    //温度十位
            Seg_Buf[6] = (unsigned char)T % 10;         //温度个位
            Seg_Buf[7] = (unsigned int)(T*10) % 10;     //温度小数1位
        break;
        //参数界面
        case 2:
            Seg_Buf[0] = 14;                  //显示U
            Seg_Buf[5] = 10;                  //熄灭
            Seg_Buf[6] = T_Show / 10 % 10;    //温度参数十位
            Seg_Buf[7] = T_Show % 10;         //温度参数个位
        break;
    }
    
    
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;
    
    //L1, L2互斥点亮
    LED_Buf[0] = (Seg_Show_Mode == 0);
    LED_Buf[1] = (Seg_Show_Mode == 1);
    
    //L8
    LED_Buf[7] = L8_Star_Flag;
}

/* 信息处理函数 */
void Infor_Deal()
{
    /* 数据输出 */
    DA_Wirte(DA_Data * 51);
    Relay((T >= T_Set));
    
    /* 减速 */
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 数据读取 */
    T = DS18B20_Read_T();
    
    /* 数据处理 */
    
    //数据保存
    if (Seg_Show_Mode == 0)
        T_Set = T_Show;
    
    //DA输出数据
    if (Freq >= 2000)       //频率大于2000Hz
        DA_Data = 4.5;      //输出4.5V
    else if (Freq <= 200)   //频率小于2000Hz
        DA_Data = 0.5;      //输出0.5V
    else                    //范围内计算
        DA_Data = (4.0 / 1800) * (Freq - 200) + 0.5;
    
    //L8控制
    if (T > 30)
        L8_Flag = 1;
    else
        L8_Flag = L8_Star_Flag = Time_100ms = 0;
}

/* 定时器初始化函数 */
void Timer1_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0xBF;			//定时器时钟12T模式
	TMOD &= 0x0F;			//设置定时器模式
	TL1 = 0x18;				//设置定时初始值
	TH1 = 0xFC;				//设置定时初始值
	TF1 = 0;				//清除TF1标志
	TR1 = 1;				//定时器1开始计时
    EA  = 1;
    ET1 = 1;
}
void Timer0_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0x7F;			//定时器时钟12T模式
	TMOD &= 0xF0;			//设置定时器模式
    TMOD |= 0x05;
	TL0 = 0x18;				//设置定时初始值
	TH0 = 0xFC;				//设置定时初始值
	TF0 = 0;				//清除TF0标志
	TR0 = 1;				//定时器0开始计时
}

/* 中断服务函数 */
void Timer1_Sevre() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 200 == 0) Seg_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    if (++Time_1000ms == 1000)
    {
        Freq = (TH0 << 8) | TL0;
        Time_1000ms = TH0 = TL0 = 0;
    }
    
    if (L8_Flag == 1 && ++Time_100ms == 100)
    {
        Time_100ms = 0;
        L8_Star_Flag ^= 1;
    }
}

/* 主函数 */
void main()
{
    System_Init();
    Timer1_Init();
    Timer0_Init();
    DS18B20_Read_T();

    while (1)
    {
        Infor_Deal();
        Key_Deal();
        Seg_Deal();
        LED_Deal();
    }
}


