#include <STC15F2K60S2.H>
#include "LED.h"

void LED_Show(unsigned char addr, enable)
{
    static unsigned char LED_temp_new = 0x00;
    static unsigned char LED_temp_old = 0xff;
    
    if (enable)
        LED_temp_new |= 0x01 << addr;
    else
        LED_temp_new &= ~(0x01 << addr);
    
    if (LED_temp_new != LED_temp_old)
    {
        P0 = ~LED_temp_new;
        P2 = P2 & 0x1f | 0x80;
        P2 &= 0x1f;
        LED_temp_old = LED_temp_new;
    }
}
void Relay(unsigned char enable)
{
    static unsigned char Relay_temp_new = 0x00;
    static unsigned char Relay_temp_old = 0xff;
    
    if (enable)
        Relay_temp_new |= 0x10;
    else
        Relay_temp_new &= ~0x10;
    
    if (Relay_temp_new != Relay_temp_old)
    {
        P0 = Relay_temp_new;
        P2 = P2 & 0x1f | 0xa0;
        P2 &= 0x1f;
        Relay_temp_old = Relay_temp_new;
    }
}
