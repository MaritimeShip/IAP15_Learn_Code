/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "ds1302.h"
#include "iic.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag;                                       //按键数码管减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned char Seg_Show_Mode;                        //数码管显示模式   0-时间界面   1-输入界面   2-记录界面
unsigned char Rtc_Show[3] = {23, 9, 55};            //时钟显示数组
unsigned char Rtc_Set[3] = {23, 9, 55};             //时钟设置数组
unsigned char Rtc_Hour, Rtc_Min;                    //记录时分
unsigned int Output_Num, Output_Old, Output_New;    //输入数
unsigned char Remark_H = 0x00, Remark_L = 0x00;     //记录高低
unsigned char Key_Count;                            //输入计次

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Down = 0, Key_UP = 0;  //变量
    static unsigned char Key_New = 0, Key_Old = 0;  //创建

    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                              //减速
    
    Key_New = Key_Read();                           //获取键码
    Key_Down = Key_New & ( Key_New ^ Key_Old);      //捕获下降沿
    Key_UP = ~Key_New & ( Key_New ^ Key_Old);       //捕获上升沿
    Key_Old = Key_New;                              //辅助扫描
    
    if (Key_Down == 4)
    {
        if (++Seg_Show_Mode == 3) Seg_Show_Mode = 0;    //界面切换
        if (Seg_Show_Mode == 1)                         //输入界面
        {
            Output_Num = 0;     //数码管熄灭,输入数据清零
        }
        if (Seg_Show_Mode == 2)                         //记录界面
        {
            Key_Count = 0;
            EEPROM_Write(&Rtc_Set[0], 0, 1);        //保存时
            EEPROM_Write(&Rtc_Set[1], 1, 1);        //保存分
            if (Output_Num >= 1000)
            {
                Output_Old = Remark_H * 256 + Remark_L;  //上次保存
                Remark_L = (Output_Num % 256) | 0x00;    //低位
                Remark_H = (Output_Num / 256) | 0x00;    //高位
                EEPROM_Write(&Remark_H, 2, 1);           //保存高位
                EEPROM_Write(&Remark_L, 3, 1);           //保存低位
                Output_New = Remark_H * 256 + Remark_L;  //新保存
            }
        }
    }
                    
    if (Seg_Show_Mode == 1 )    //输入模式
    {
        if (Key_Down == 5)
            Output_Num = Key_Count = 0;
        if (Output_Num < 1000)  //当按下值不超过1000时
        {
            switch (Key_Down)   //输入键码
            {
                case  6: Output_Num = Output_Num * 10 + 0; break;
                case 10: Output_Num = Output_Num * 10 + 1; break;
                case 14: Output_Num = Output_Num * 10 + 2; break;
                case 18: Output_Num = Output_Num * 10 + 3; break;
                case  9: Output_Num = Output_Num * 10 + 4; break;
                case 13: Output_Num = Output_Num * 10 + 5; break;
                case 17: Output_Num = Output_Num * 10 + 6; break;
                case  8: Output_Num = Output_Num * 10 + 7; break;
                case 12: Output_Num = Output_Num * 10 + 8; break;
                case 16: Output_Num = Output_Num * 10 + 9; break;
            }
        }
        //按键判断
        if (Key_Down != 0 && Key_Down != 4 && Key_Down != 5 && Key_Down != 7 && Key_Down != 11 && Key_Down != 15 && Key_Down != 19)
        {
            if (++Key_Count == 5)       //按键计次
                Key_Count = 4;
        }
        if (Key_Count == 1)
            DS1302_Read_Rtc(Rtc_Set);   //时间记录
    }
}

/* 信息处理函数 */
void Infor_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;                              //减速
    
    /* 信息读取区域 */
    DS1302_Read_Rtc(Rtc_Show);  //时钟读取
    
    /* 信息显示区 */
    switch (Seg_Show_Mode)
    {
        //时钟显示
        case 0:
            Seg_Buf[0] = Rtc_Show[0] / 10;      //显示时十位
            Seg_Buf[1] = Rtc_Show[0] % 10;      //显示时个位
            Seg_Buf[2] = 11;                    //显示-
            Seg_Buf[3] = Rtc_Show[1] / 10;      //显示分十位
            Seg_Buf[4] = Rtc_Show[1] % 10;      //显示分个位
            Seg_Buf[5] = 11;                    //显示-
            Seg_Buf[6] = Rtc_Show[2] / 10;      //显示秒十位
            Seg_Buf[7] = Rtc_Show[2] % 10;      //显示秒个位
        break;
        //输入界面
        case 1:
            Seg_Buf[0] = 14;                     //显示C
            for (i = 1; i < 4; i++)
                Seg_Buf[i] = 10;                 //熄灭
            Seg_Buf[4] = Output_Num / 1000 % 10; //显示千
            Seg_Buf[5] = Output_Num / 100 % 10;  //显示百
            Seg_Buf[6] = Output_Num / 10 % 10;   //显示十
            Seg_Buf[7] = Output_Num % 10;        //显示个
            for (i = 4; i < 8; i++)              //高位熄灭
            {
                if (Seg_Buf[i] == 0) 
                    Seg_Buf[i] = 10;
                else
                    break;
            }
        break;
        //记录界面
        case 2:
            Seg_Buf[0] = 16;                         //显示E
            Seg_Buf[1] = Seg_Buf[2] = 10;            //熄灭
            Seg_Buf[3] = Rtc_Set[0] / 10;            //显示时十位
            Seg_Buf[4] = Rtc_Set[0] % 10;            //显示时个位
            Seg_Buf[5] = 11;                         //显示-
            Seg_Buf[6] = Rtc_Set[1] / 10;            //显示分十位
            Seg_Buf[7] = Rtc_Set[1] % 10;            //显示分个位
        break;
    }
}

/* 其他处理函数 */
void Other_Proc()
{    
    /* LED显示区 */
    for (i = 0; i < 3; i++)
        LED_Buf[i] = (i == Seg_Show_Mode);
    if (Output_Old < Output_New)
        LED_Buf[3] = 1;
    else
        LED_Buf[3] = 0;
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
}

/* 延时函数 */
void Delay_ms(unsigned int xms)		//@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 主函数 */
void main()
{
    Init();
    DS1302_Write_Rtc(Rtc_Show);
    Timer0_Init();
    while (1)
    {
        Key_Proc();
        Infor_Proc();
        Other_Proc();
    }
}



