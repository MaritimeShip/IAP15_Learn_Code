/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include <intrins.h>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "iic.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Show_Flag;                        //按键数码管减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区

/* 变量创建区 */
unsigned char AD_Data, LED_i;                                   //AD数据，LED专用扫描
unsigned int LED_Time = 400, LED_Time_Temp = 400;               //LED流转时间设置
unsigned char Seg_Disp_Mode;                                    // 0 --> 数码管熄灭   1 --> 模式运行模式   2 --> 流转模式
bit Seg_Mode
unsigned char LED_Disp_Mode;                                    // 0 --> 模式1        1 --> 模式2          2 --> 模式3      3 --> 模式4
unsigned char LED_Poc = 3;                                      //LED数组指针
unsigned char LED_Mode_Arr[4] = {0x81, 0x42, 0x24, 0x18};       //LED显示数组

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Down = 0, Key_UP = 0;  //变量
    static unsigned char Key_New = 0, Key_Old = 0;  //创建

    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                              //减速
    
    Key_New = Key_Read();                           //获取键码
    Key_Down = Key_New & ( Key_New ^ Key_Old);      //捕获下降沿
    Key_UP = ~Key_New & ( Key_New ^ Key_Old);       //捕获上升沿
    Key_Old = Key_New;                              //辅助扫描
    
    switch (Key_UP)
    {
        case 4:
            
        break;
        
    }
    
}

/* 信息处理函数 */
void Infor_Proc()
{
    unsigned char Seg_i = 0;        //数码管专用扫描
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;                              //减速
    
    AD_Data = AD_Read(0x43);
    
//    Seg_Buf[0] = AD_Data / 100 % 10;
//    Seg_Buf[1] = AD_Data / 10 % 10;
//    Seg_Buf[2] = AD_Data % 10;
    
    switch (Seg_Disp_Mode)
    {
        //数码管全熄灭
        case 0:
            //LED流转时间判断
            if (LED_Time < 400 || LED_Time > 1200)
                LED_Time = LED_Time_Temp;
            else
                LED_Time_Temp = LED_Time;
            //参数保存
            
            //数码管显示
            for (Seg_i = 0; Seg_i < 8; Seg_i++0)
                Seg_Buf[Seg_i] = 0;
        break;
        //运行模式
        case 1:
            Seg_Buf[0] = 11;                    //显示 -
            Seg_Buf[1] = LED_Disp_Mode;         //显示运行编号
            Seg_Buf[2] = 11;                    //显示 -
            Seg_Buf[3] = 10;                    //熄灭
            if (LED_Time / 1000 == 0)
                Seg_Buf[4] = 10;                //熄灭
            else
                Seg_Buf[4] = 1;                 //显示千位
            Seg_Buf[5] = LED_Time / 100 % 10;   //显示百位
            Seg_Buf[6] = LED_Time / 10 % 10;    //显示十位
            Seg_Buf[7] = LED_Time % 10;         //显示个位
        break;
        //流转模式
        case 2:
            
        break;
    }
    
}

/* 其他处理函数 */
void Other_Proc()
{
    if (LED_Show_Flag) return;
    LED_Show_Flag = 1;                              //减速
    /* LED模块 */
    //LED显示
    switch (LED_Disp_Mode)
    {
        case 0: LED_Show(_crol_(0x01, LED_i++)); break;
        case 1: LED_Show(_cror_(0x01, ++LED_i)); break;
        case 2:
            LED_Show(LED_Mode_Arr[LED_i++]);
            LED_i %= 4;
        break;
        case 3:
            LED_Show(LED_Mode_Arr[LED_Poc--]);
            if (LED_Poc == 255) LED_Poc = 3;
        break;
    }
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer0_Serve() interrupt 1
{
    static unsigned int Timer0_Count = 0;
    if (++Timer0_Count == LED_Time_Temp) LED_Show_Flag = Timer0_Count = 0;
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    
    
}

/* 延时函数 */
void Delay_ms(unsigned int xms)		//@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 主函数 */
void main()
{
    Init();
    Timer0_Init();
    while (1)
    {
        Key_Proc();
        Infor_Proc();
        Other_Proc();
    }
}



