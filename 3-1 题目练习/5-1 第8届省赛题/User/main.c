/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "ds1302.h"
#include "onewire.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag;                        //按键数码管LED减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区

/* 变量创建区 */
bit Seg_Show_Mode;                              //数码管显示模式  0-时间显示模式  1-温度显示模式
unsigned char T;                                //实时温度
unsigned char Set_Flag;                         //设置模式标志位  0-时钟  1-时钟设置 2-闹钟设置
unsigned char usRtc[3] = {0x23, 0x59, 0x58};    //实时时钟数组
unsigned char Alarm[3] = {0x00, 0x00, 0x00};    //实际闹钟数组
unsigned char Set_Index;                        //设置模式指针  0-时  1-分  2-秒
unsigned char usRtc_Set[3] = {0x23, 0x59, 0x50};//时钟设置数组
unsigned char Alarm_Set[3] = {0x00, 0x00, 0x00};//闹钟设置数组
unsigned char* Clock_Poc[3] = {usRtc, usRtc_Set, Alarm_Set};//指针数组
unsigned int Time_500ms;                        //0.5秒计时专用
bit Seg_Star_Flag;                              //数码管闪烁标志位
unsigned char Time_100ms;                       //100ms计时专用
bit LED_Star_Flag;                              //LED显示标志位
bit Alarm_Hint_Flag;                            //闹钟提示标志位  0-非提示   1-提示
unsigned int Time_5s;                           //5s计时专用

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Down = 0, Key_UP = 0;  //变量
    static unsigned char Key_New = 0, Key_Old = 0;  //创建
    unsigned char i = 0;
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                              //减速
    
    if (usRtc[0] == Alarm[0] && usRtc[1] == Alarm[1] && usRtc[2] == Alarm[2])//闹钟提示
        Alarm_Hint_Flag = 1;    //打开提示
    
    Key_New = Key_Read();                           //获取键码
    Key_Down = Key_New & ( Key_New ^ Key_Old);      //捕获下降沿
    Key_UP = ~Key_New & ( Key_New ^ Key_Old);       //捕获上升沿
    Key_Old = Key_New;                              //辅助扫描
    
    if (Alarm_Hint_Flag == 1 && Key_Down != 0)      //按下关闭提示
        Alarm_Hint_Flag = 0;
        
    
    //温度显示    S4
    if (Key_Old == 4 && Set_Flag == 0)
        Seg_Show_Mode = 1;
    else
        Seg_Show_Mode = 0;
    //参数设置    S5 S4
    if (Set_Flag)
    {
        //参数加
        if (Key_Down == 5)
        {
            if (++Clock_Poc[Set_Flag][Set_Index] % 16 == 0x0a)
                Clock_Poc[Set_Flag][Set_Index] += 6;
            if (Set_Index != 0 && Clock_Poc[Set_Flag][Set_Index] == 0x60)//分秒
                Clock_Poc[Set_Flag][Set_Index] = 0x59;
            else if (Set_Index == 0 && Clock_Poc[Set_Flag][Set_Index] == 0x24)//时
                Clock_Poc[Set_Flag][Set_Index] = 0x23;
        }
        //参数减
        else if(Key_Down == 4)
        {
            if (--Clock_Poc[Set_Flag][Set_Index] % 16 == 0x0f)
                Clock_Poc[Set_Flag][Set_Index] -= 6;
            if (Clock_Poc[Set_Flag][Set_Index] == 0xf9)
                Clock_Poc[Set_Flag][Set_Index] = 0;
        }
    }
    //时钟设置     S7
    if (Key_Down == 7 && Set_Flag != 2)
    {
        if (Set_Flag == 0)
        {
            Set_Flag = 1;
            DS1302_Rtc_Read(usRtc_Set);    //数据取出
        }
        else
        {
            Set_Index++;
            if (Set_Index == 3)
            {
                Set_Index = 0;
                Set_Flag = 0;
                DS1302_Rtc_Set(usRtc_Set);    //数据保存
            }
        }
    }
    //闹钟设置     S6
    if (Key_Down == 6 && Set_Flag != 1)
    {
        if (Set_Flag == 0)
            Set_Flag = 2;
        else
        {
            Set_Index++;
            if (Set_Index == 3)
            {
                Set_Index = 0;
                Set_Flag = 0;
                for (i = 0; i < 3; i++)
                    Alarm[i] = Alarm_Set[i];
            }
        }
    }
}

/* 信息处理函数 */
void Infor_Proc()
{
    unsigned char i = 0;
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;                              //减速
    
    /* 信息获取区 */
    DS1302_Rtc_Read(usRtc);     //时钟读取
    T = DS18B20_Read_T();       //温度读取

    /* 信息显示区 */
    //时钟显示
    if (Seg_Show_Mode == 0)
    {
        Seg_Buf[5] = Seg_Buf[2] = 11;              //显示-
        for (i = 0; i < 3; i++)
        {
            Seg_Buf[3*i] = Clock_Poc[Set_Flag][i] / 16;         //显示十位
            Seg_Buf[3*i + 1] = Clock_Poc[Set_Flag][i] % 16;     //显示个位
            
        }
        //闪烁控制
        if (Set_Flag != 0)
        {
            Seg_Buf[Set_Index*i] = Seg_Star_Flag?10:(Clock_Poc[Set_Flag][Set_Index] / 16);         //显示十位
            Seg_Buf[Set_Index*i + 1] = Seg_Star_Flag?10:(Clock_Poc[Set_Flag][Set_Index] % 16);     //显示个位
        }
    }
    //温度显示
    else
    {
            for(i = 0; i < 5; i++)
                Seg_Buf[i] = 10;        //熄灭
            Seg_Buf[5] = T / 10 % 10;   //显示温度十位
            Seg_Buf[6] = T % 10;        //显示温度个位
            Seg_Buf[7] = 14;            //显示C
    }
}

/* 其他处理函数 */
void Other_Proc()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;                              //减速
    
    if (Alarm_Hint_Flag)
        LED_Buf[0] = LED_Star_Flag?0:1;
    else
    {
        LED_Buf[0] = 0;
        Time_5s = 0;
    }
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down == 200) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 50 == 0) LED_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    if (++Time_500ms == 500)
    {
        Time_500ms = 0;
        Seg_Star_Flag ^= 1;
    }
    if (++Time_100ms == 100)
    {
        Time_100ms = 0;
        LED_Star_Flag ^= 1;
    }
    if (Alarm_Hint_Flag)
    {
        if (++Time_5s == 5000)
            Alarm_Hint_Flag = 0;
    }
}

/* 延时函数 */
void Delay_ms(unsigned int xms)		//@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 主函数 */
void main()
{
    Init();
    Timer0_Init();
    DS1302_Rtc_Set(usRtc);
    DS18B20_Read_T();
    Delay_ms(750);
    while (1)
    {
        Key_Proc();
        Infor_Proc();
        Other_Proc();
    }
}
