#include <STC15F2K60S2.H>
#include "LED.h"




void LED_Show(unsigned char addr, enable)
{
    static unsigned char temp_new = 0x00;
    static unsigned char temp_old = 0xFF;
    
    if (enable)
        temp_new |= 0x01 << addr;
    else
        temp_new &= ~(0x01 << addr);
    
    if (temp_new != temp_old)
    {
        P0 = ~temp_new;
        P2 = P2 & 0x1f | 0x80;
        P2 &= 0x1f;
        temp_old = temp_new;
    }
}

void Beep(unsigned char flag)
{
    static unsigned char temp_new_beep = 0x00;
    static unsigned char temp_old_beep = 0xFF;
    
    if (flag)
        temp_new_beep |= 0x40;
    else
        temp_new_beep &= ~(0x40);
    
    if (temp_new_beep != temp_old_beep)
    {
        P0 = temp_new_beep;
        P2 = P2 & 0x1f | 0xa0;
        P2 &= 0x1f;
        temp_old_beep = temp_new_beep;
    }
}

void Relay(unsigned char flag)
{
    static unsigned char temp_new_relay = 0x00;
    static unsigned char temp_old_relay = 0xFF;
    if (flag)
        temp_new_relay |= 0x10;
    else
        temp_new_relay &= ~(0x10);
    
    if (temp_new_relay != temp_old_relay)
    {
        P0 = temp_new_relay;
        P2 = P2 & 0x1f | 0xa0;
        P2 &= 0x1f;
        temp_old_relay = temp_new_relay;
    }
}





