/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "iic.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                     //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag;            //按键数码管减速标志位
unsigned char Seg_Buf[8] = {0, 10, 10, 10, 10, 10, 10, 10}; //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};      //数码管小数点显示缓冲区
unsigned char i;                                            //循环专用变量

/* 变量创建区 */
bit Seg_Show_Flag;                  //数码管显示标志位 0-熄灭 1-点亮
bit Seg_Show_Mode;                  //数码管显示模式 0-非设置模式 1-设置模式
unsigned int Roam_Time[4] = {400, 400, 400, 400};
unsigned int Roam_Set_Time[4] = {400, 400, 400, 400};
unsigned char Time_Set_Index;       //流转设置指针   0   1   2   3
bit LED_Show_Flag;                  //LED暂停标志位  0-启动   1-暂停
bit Roam_Time_Flag;                 //流转时间标志位  0-流转   1-不流转
bit Seg_Set_Index;                  //数码管设置指针    0-运行模式   1-流转模式
unsigned char AD_Data;              //AD数据
unsigned char PWM;                  //PWM控制专用变量  20  40  60  80
unsigned char LED_Light_Grade;      //亮度等级专用变量  0   1   2   3
unsigned char Timer1_80;            //定时器1计数80
unsigned int Timer0_800ms;          //定时器0计时800ms
unsigned int Timer0_1200ms;         //定时器0计时1200ms
bit Seg_Star_Flag;                  //数码管闪烁标志位
unsigned char LED_Arr_34[4] = {0x81, 0x42, 0x24, 0x18};
unsigned char LED_Arr_12[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
unsigned char LED_Poc, LED_Temp;    //LED显示指针 中间变量

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;          //获取键码
    static unsigned char Key_UP = 0, Key_Down = 0;          //专用变量
    
    if (Key_Slow_Flag) return;                              //减速
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();                                   //获取键码
    Key_UP = ~Key_New & (Key_New ^ Key_Old);                //捕获上升沿
    Key_Down = Key_New & (Key_New ^ Key_Old);               //捕获下降沿
    Key_Old = Key_New;                                      //辅助扫描
    
    //S4 显示亮度等级 控制参数
    Seg_Show_Flag = 0;
    if(Key_Old == 4 && Seg_Show_Mode == 0)
        Seg_Show_Flag = 1;
    else if (Seg_Show_Mode == 1)
    {
        //参数减
        if (Key_Down == 4)
        {
            if (Seg_Set_Index == 0)
            {
                if (--Time_Set_Index == 255) //限制
                    Time_Set_Index = 0;
                else
                    Timer0_1200ms = 0;       //计时清零
                //模式流转初始化Poc
                if (Time_Set_Index == 0 || Time_Set_Index == 2)
                    LED_Poc = 0;
                else if (Time_Set_Index == 1)
                    LED_Poc = 7;
                else if (Time_Set_Index == 3)
                    LED_Poc = 3;
            }
            else
            {
                Roam_Set_Time[Time_Set_Index] -= 100;
                if (Roam_Set_Time[Time_Set_Index] < 400)    //限制
                    Roam_Set_Time[Time_Set_Index] = 400;
            }
        }
        //参数加
        else if (Key_Down == 5)
        {
            if (Seg_Set_Index == 0)
            {
                if (++Time_Set_Index == 4) //限制
                    Time_Set_Index = 3;
                else
                    Timer0_1200ms = 0;     //计时清零
                //模式流转初始化Poc
                if (Time_Set_Index == 0 || Time_Set_Index == 2)
                    LED_Poc = 0;
                else if (Time_Set_Index == 1)
                    LED_Poc = 7;
                else if (Time_Set_Index == 3)
                    LED_Poc = 3;
            }
            else
            {
                Roam_Set_Time[Time_Set_Index] += 100;
                if (Roam_Set_Time[Time_Set_Index] > 1200)   //限制
                    Roam_Set_Time[Time_Set_Index] = 1200;
            }
        }
    }
    //S7 LED启动暂停
    if (Key_Down == 7)
        LED_Show_Flag ^= 1;
    //S6 切换数码管显示单元
    if(Key_Down == 6)
    {
        if(Seg_Show_Mode == 0)          //数码管不显示切换到编号模式
            Seg_Show_Mode = 1;
        else if (Seg_Show_Mode == 1 && Seg_Set_Index == 0)
            Seg_Set_Index = 1;
        else if (Seg_Show_Mode == 1 && Seg_Set_Index == 1)
        {
            Seg_Show_Mode = Seg_Set_Index = 0;  //退出
            EEPROM_Write(Roam_Set_Time, 8, 4);  //保存数据
            EEPROM_Read(Roam_Time, 8, 4);       //数据更新
        }
    
    }
}

/* 信息处理函数 */
void Infor_Deal()
{
    if (Seg_Slow_Flag) return;            //减速
    Seg_Slow_Flag = 1;
    
    /* 信息获取区 */
    AD_Data = AD_Read(0x43) / 85;       // 0  1  2  3
    
    /* 信息显示区 */
    //非设置模式
    if(Seg_Show_Mode == 0)
    {
        for(i = 0; i < 6; i++)
            Seg_Buf[i] = 10;              //熄灭
        Seg_Buf[6] = 11;                  //显示杠
        Seg_Buf[7] = LED_Light_Grade;     //显示亮度等级
    }
    //设置模式
    else
    {
        Seg_Buf[3] = 10;                        //熄灭
        Seg_Buf[0] = Seg_Buf[2] = 11;           //显示-
        Seg_Buf[1] = Time_Set_Index+1;          //显示流转模式
        Seg_Buf[4] = (Roam_Set_Time[Time_Set_Index] >= 1000) ? 1 : 10;  //显示千位
        Seg_Buf[5] = Roam_Set_Time[Time_Set_Index] / 100 % 10;          //显示百位
        Seg_Buf[6] = Roam_Set_Time[Time_Set_Index] / 10 % 10;           //显示十位
        Seg_Buf[7] = Roam_Set_Time[Time_Set_Index] % 10;                //显示个位
        if (Seg_Set_Index == 0)//闪烁控制
        {
            Seg_Buf[0] = Seg_Buf[2] = Seg_Star_Flag?10:11;              //显示-
            Seg_Buf[1] = Seg_Star_Flag?10:(Time_Set_Index+1);           //显示流转模式
        }
        else
        {
            Seg_Buf[4] = Seg_Star_Flag?10:((Roam_Set_Time[Time_Set_Index] >= 1000) ? 1 : 10);  //显示千位
            Seg_Buf[5] = Seg_Star_Flag?10:Roam_Set_Time[Time_Set_Index] / 100 % 10;            //显示百位
            Seg_Buf[6] = Seg_Star_Flag?10:Roam_Set_Time[Time_Set_Index] / 10 % 10;             //显示十位
            Seg_Buf[7] = Seg_Star_Flag?10:Roam_Set_Time[Time_Set_Index] % 10;                  //显示个位
        }
    }
}

/* 其他处理函数 */
void Other_Deal()
{
    if (LED_Slow_Flag) return;                              //减速
    LED_Slow_Flag = 1;

    /* 信息处理 */
    if(AD_Data == 0)                        //亮度等级1
        LED_Light_Grade = 1;
    else if(AD_Data > 0 && AD_Data <= 1)    //亮度等级2
        LED_Light_Grade = 2;            
    else if(AD_Data > 1 && AD_Data <= 2)    //亮度等级3
        LED_Light_Grade = 3;            
    else if(AD_Data > 2 && AD_Data <= 3)    //亮度等级4
        LED_Light_Grade = 4;            
    PWM = LED_Light_Grade*20;               //PWM控制亮度
    
    /* LED处理 */
    if (LED_Show_Flag == 0)                     //启动暂停
    {
        if (Roam_Time_Flag == 0)                //启动流转
        {
            switch (Time_Set_Index)
            {
                //模式1
                case 0:
                    LED_Temp = LED_Arr_12[LED_Poc++];
                    if (LED_Poc == 8) LED_Poc = 0;
                break;
                //模式2
                case 1:
                    LED_Temp = LED_Arr_12[LED_Poc--];
                    if (LED_Poc == 255) LED_Poc = 7;
                break;
                //模式3
                case 2:
                    LED_Temp = LED_Arr_34[LED_Poc++];
                    if (LED_Poc == 4) LED_Poc = 0;
                break;
                //模式4
                case 3:
                    LED_Temp = LED_Arr_34[LED_Poc--];
                    if (LED_Poc == 255) LED_Poc = 3;
                break;
            }
            Roam_Time_Flag = 1;                     //暂停流转
        }
    }
    else
        Timer0_1200ms = 0;
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}
void Timer1_Init(void)		//100微秒@12.000MHz
{
	AUXR &= 0xBF;			//定时器时钟12T模式
	TMOD &= 0x0F;			//设置定时器模式
	TL1 = 0x9C;				//设置定时初始值
	TH1 = 0xFF;				//设置定时初始值
	TF1 = 0;				//清除TF1标志
	TR1 = 1;				//定时器1开始计时
    ET1 = 1;
    EA  = 1;
}

/* 定时器中断函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 100 == 0) LED_Slow_Flag = 0;
    
    if (Seg_Show_Flag != 0 || Seg_Show_Mode != 0)   //数码管显示
        Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    else                                            //数码管熄灭
        Seg_Show(Slow_Down%8, 10, 0);

    if(++Timer0_800ms == 800)
    {
        Timer0_800ms = 0;
        Seg_Star_Flag ^= 1;
    }
    
    if (++Timer0_1200ms >= Roam_Time[Time_Set_Index])
        Timer0_1200ms = Roam_Time_Flag = 0; //启动
}

void Timer1_Serve() interrupt 3
{
    if(++Timer1_80 == 80) Timer1_80 = 0;
    
    if(PWM > Timer1_80)
        LED_Show(LED_Temp);
    else
        LED_Show(0x00);
}

/* 主函数 */
void main()
{
    System_Init();
    EEPROM_Read(Roam_Time, 8, 4);
    EEPROM_Read(Roam_Set_Time, 8, 4);
    Timer0_Init();
    Timer1_Init();
    while (1)
    {
        Key_Deal();
        Infor_Deal();
        Other_Deal();
    }
}



