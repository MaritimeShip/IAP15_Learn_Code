#include "Seg.h"
#include <STC15F2K60S2.H>
#include "Latch.h"


unsigned char Seg_Buf[9] = { 0, 10, 10, 10, 10, 10, 10, 10, 10 };

code unsigned char Seg_Table[] = { 0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90, 0xFF };



void Seg_Scan(unsigned char Seg_Location, unsigned char Seg_Num)
{
    switch (Seg_Location)
    {
        case 1:  P0 = 0x01;  Latch(6);  break;
        case 2:  P0 = 0x02;  Latch(6);  break;
        case 3:  P0 = 0x04;  Latch(6);  break;
        case 4:  P0 = 0x08;  Latch(6);  break;
        case 5:  P0 = 0x10;  Latch(6);  break;
        case 6:  P0 = 0x20;  Latch(6);  break;
        case 7:  P0 = 0x40;  Latch(6);  break;
        case 8:  P0 = 0x80;  Latch(6);  break;
    }
    P0 = Seg_Table[Seg_Num];
    Latch(7);
}



void Seg(unsigned char Seg_Location, unsigned char Seg_Num)
{
    Seg_Buf[Seg_Location] = Seg_Num;
}



void Seg_Loop()
{
    static unsigned char i = 1;
    Seg_Scan(i, Seg_Buf[i]);
    i++;
    if (i > 8)
    {
        i = 1;
    }
}


