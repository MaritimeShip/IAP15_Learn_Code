#include <STC15F2K60S2.H>
#include "Key.h"

unsigned char Key_Temp;

unsigned char Key(void)
{
    unsigned char Temp = 0;
    Temp = Key_Temp;
    Key_Temp = 0;
    return Temp;
}

unsigned char Key_GetNum(void)
{
    unsigned char KeyNumber = 0;
    if (P30 == 0) { KeyNumber = 7; }
    if (P31 == 0) { KeyNumber = 6; }
    if (P32 == 0) { KeyNumber = 5; }
    if (P33 == 0) { KeyNumber = 4; }
    
    return KeyNumber;
}


void Key_Loop(void)
{
    static unsigned char NowState = 0;
    static unsigned char LastState = 0;
    LastState = NowState;
    NowState = Key_GetNum();
    if (LastState == 0 && NowState == 7)  { Key_Temp = 7; }
    if (LastState == 0 && NowState == 6)  { Key_Temp = 6; }
    if (LastState == 0 && NowState == 5)  { Key_Temp = 5; }
    if (LastState == 0 && NowState == 4)  { Key_Temp = 4; }

}




