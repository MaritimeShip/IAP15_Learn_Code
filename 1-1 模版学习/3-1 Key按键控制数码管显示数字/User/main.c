#include <STC15F2K60S2.H>
#include <intrins.h>
#include "Latch.h"
#include "Seg.h"
#include "Delay.h"
#include "Timer0.h"
#include "Output.h"
#include "Key.h"


unsigned char KeyNum;

void main()
{
    char Num = 0;
    
    Timer0_Init();

    while (1)
    {
        KeyNum = Key();
        if (KeyNum == 7) { Num++; }
        if (KeyNum == 6) { Num--; }
        Seg(1, Num);
    }
}


void Timer0_Interrupt() interrupt 1
{
    static unsigned char Seg_Count = 0;
    static unsigned char Key_Count = 0;
    
    Seg_Count++;
    if (Seg_Count == 2)
    {
        Seg_Count = 0;
        Seg_Loop();
    }
    
    Key_Count++;
    if (Key_Count == 20)
    {
        Key_Count = 0;
        Key_Loop();
    }
}




































