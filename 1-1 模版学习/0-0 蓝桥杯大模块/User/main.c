/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Latch.h"
#include "Output.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"

/* 变量创建区 */
unsigned char Key_Show_Down;//按键减速专用
unsigned char Key_Val, Key_Down, Key_UP, Key_Old;//捕获按键下降沿专用
unsigned char Seg_Show_Down;//数码管减速专用
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};//数码管缓冲显示区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};//数码管小数点缓冲显示
unsigned char Seg_Poc;//数码管扫描专用
unsigned char usLED[8] = {0, 0, 0, 0, 0, 0, 0, 0};//LED显示缓冲显示区

/* 按键处理函数 */
void Key_Proc()
{
    if (Key_Show_Down) return;
    Key_Show_Down = 1;//按键减速
    
    Key_Val = Key_Read();//读取键码
    Key_Down = Key_Val & (Key_Val ^ Key_Old);//捕获下降沿
    Key_UP = Key_Val & (Key_Val ^ Key_Old);//捕获上升沿
    Key_Old = Key_Val;//辅助扫描
    
    switch (Key_Down)
    {
    
    
    }
}

/* 信息显示函数 */
void Seg_Proc()
{
    if (Seg_Show_Down) return;
    Seg_Show_Down = 1;//数码管减速
    
    
}

/* 其他显示函数 */
void LED_Proc()
{
    
}

/* 定时器初始化函数 */
void Timer0Init(void)       //1毫秒@12.000MHz
{
    AUXR &= 0x7F;           //定时器时钟12T模式
    TMOD &= 0xF0;           //设置定时器模式
    TL0 = 0x18;             //设置定时初始值
    TH0 = 0xFC;             //设置定时初始值
    TF0 = 0;                //清除TF0标志
    TR0 = 1;                //定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Key_Show_Down == 10) Key_Show_Down = 0;
    if (++Seg_Show_Down == 500) Seg_Show_Down = 0;
    if (++Seg_Poc == 8) Seg_Poc = 0;
    Seg_Disp(Seg_Poc, Seg_Buf[Seg_Poc], Seg_Point[Seg_Poc]);
    LED_Disp(Seg_Poc, usLED[Seg_Poc]);
    
}

/* 主函数 */
void main()
{
    Output(0xFF, 4);    //关闭LED
    Output(0x00, 5);    //关闭蜂鸣器
    Timer0Init();       //定时器0初始化
    
    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}


