#include "LED.h"



void LED_Disp(unsigned char addr, bit enable)
{
    static unsigned char temp = 0x00;
    static unsigned char temp_old = 0xff;
    
    if (enable)
        temp |= 0x01 << addr;
    else
        temp &= ~(0x01 << addr);
    
    if (temp != temp_old)
    {
        Output(~temp, 4);
        temp_old = temp;
    }
}

















