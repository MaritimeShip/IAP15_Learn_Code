#include <STC15F2K60S2.H>
#include <intrins.h>

#include "Delay.h"
#include "Latch.h"


void main()
{
    unsigned char i = 0;
    P0 = ~0x01;
    Latch(4);
    Delay_ms(1000);
    while (1)
    {
        for (i = 1; i < 8; i++)
        {
            P0 = _crol_(P0, 1);
            Latch(4);
            Delay_ms(1000);
        }
    }
}

















