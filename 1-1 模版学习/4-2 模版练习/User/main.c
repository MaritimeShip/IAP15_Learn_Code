/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"

/* 基础变量创建区 */
unsigned int Slow_Down;             //减速扫描专用变量
bit Key_Slow_Flag, Seg_Slow_Flag;   //按键数码管减速标志位
unsigned char Seg_Buf[8] = {0, 10, 10, 10, 10, 10, 10, 10};
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};

/* 变量创建区 */


/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;
    static unsigned char Key_UP = 0, Key_Down = 0;
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();
    Key_UP = ~Key_New & (Key_New ^ Key_Old);
    Key_Down = Key_New & (Key_New ^ Key_Old);
    Key_Old = Key_New;
    
    switch (Key_Down)
    {
        case 4:
            Seg_Buf[0] ^= 1;
            LED_Buf[0] ^= 1;
        break;
    }
    
}

/* 信息处理函数 */
void Infor_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    
}

/* 其他处理函数 */
void Other_Deal()
{
    
    
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0x7F;			//定时器时钟12T模式
	TMOD &= 0xF0;			//设置定时器模式
	TL0 = 0x18;				//设置定时初始值
	TH0 = 0xFC;				//设置定时初始值
	TF0 = 0;				//清除TF0标志
	TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 中断服务函数 */
void Timer0_Sevre() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
}

/* 主函数 */
void main()
{
    System_Init();
    Timer0_Init();
    
    while (1)
    {
        Key_Deal();
        Infor_Deal();
        Other_Deal();
    }
}


