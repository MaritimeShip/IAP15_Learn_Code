#include <STC15F2K60S2.H>
#include "LED.h"


unsigned char LED_temp_new = 0x00;
unsigned char LED_temp_old = 0xff;


void LED_Show(unsigned char addr, enable)
{
    if (enable)
        LED_temp_new |= 0x01 << addr;
    else
        LED_temp_new &= ~(0x01 << addr);
    
    if (LED_temp_new != LED_temp_old)
    {
        P0 = ~LED_temp_new;
        P2 = P2 & 0x1f | 0x80;
        P2 &= 0x1f;
        LED_temp_old = LED_temp_new;
    }
}

void Beep(unsigned char enable)
{
    if (enable)
        LED_temp_new |= 0x40;
    else
        LED_temp_new &= ~0x40;
    
    if (LED_temp_new != LED_temp_old)
    {
        P0 = ~LED_temp_new;
        P2 = P2 & 0x1f | 0x80;
        P2 &= 0x1f;
        LED_temp_old = LED_temp_new;
    }
}


void Relay(unsigned char enable)
{
    if (enable)
        LED_temp_new |= 0x10;
    else
        LED_temp_new &= ~0x10;
    
    if (LED_temp_new != LED_temp_old)
    {
        P0 = ~LED_temp_new;
        P2 = P2 & 0x1f | 0x80;
        P2 &= 0x1f;
        LED_temp_old = LED_temp_new;
    }
}

