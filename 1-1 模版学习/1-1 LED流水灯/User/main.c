#include <STC15F2K60S2.H>
#include <intrins.h>

void Delay_ms(unsigned int x_ms)     //@11.0592MHz
{
    unsigned char i, j;
    while (x_ms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        x_ms--;
    }
}

/**
  * @brief   ������
  * @param   Latch_Num --> ��Ҫ����ļĴ���
  * @retval  ��
  */
void Select_Latch(unsigned char Latch_Channel)
{
    switch (Latch_Channel)
    {
        case 4:  P2 = (P2 & 0x1F) | 0x80;  P2 &= 0x1F;  break;    //Y4����
        case 5:  P2 = (P2 & 0x1F) | 0xA0;  P2 &= 0x1F;  break;    //Y5����
        case 6:  P2 = (P2 & 0x1F) | 0xC0;  P2 &= 0x1F;  break;    //Y6����
        case 7:  P2 = (P2 & 0x1F) | 0xE0;  P2 &= 0x1F;  break;    //Y7����
    }
  
}

void Init()
{
    P0 = 0x00;
    Latch(4);
}

void main()
{
    unsigned char i = 0;
    Init();
    
    while (1)
    {
        for (i = 0; i < 8; i++)
        {
            P0 = ~(0x01 << i);
            Latch(4);
            Delay_ms(500);
        }
    }
}











