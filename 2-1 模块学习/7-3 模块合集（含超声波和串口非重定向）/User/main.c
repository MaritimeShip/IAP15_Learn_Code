/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include <stdio.h>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "ds1302.h"
#include "iic.h"
#include "onewire.h"
#include "Uwave.h"
#include "Uart.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag;                                       //按键数码管减速标志位
idata unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};      //数码管显示缓冲区
idata unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};            //数码管小数点显示缓冲区
idata unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};              //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned char Seg_Show_Mode;                        //数码管显示模式  0-实时时钟  1-温度读取  2-RB2显示  3-NE555测频率  4-超声波测距
idata unsigned char Rtc_Buf[3] = {23, 59, 55};      //实时时钟数组
idata unsigned char Rtc_Buf_Init[3] = {23, 59, 55}; //实时时钟初始化数组
float T, AD_DA_Data;                                //实时温度，实时读取输出电压值
unsigned int Freq, Timer1_1000ms;                   //实时频率测量, 定时器1000ms计时专用变量
unsigned char Uwave_D;                              //读取超声波距离专业变量
unsigned int Timer1_500ms;                          //定时器500ms计时
bit S6_Time_Flag;                                   //按键S6计时标志位
unsigned char Uart_Rx_Buf[20];                      //串口接收数组
unsigned char Uart_Tx_Buf[20];                      //串口发送数组
unsigned char Uart_Index;                           //串口数组指针
bit Uart_Rx_Flag;                                   //串口接收标志位

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_Down = 0, Key_UP = 0;  //变量
    static unsigned char Key_New = 0, Key_Old = 0;  //创建

    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                              //减速
    
    Key_New = Key_Read();                           //获取键码
    Key_Down = Key_New & ( Key_New ^ Key_Old);      //捕获下降沿
    Key_UP = ~Key_New & ( Key_New ^ Key_Old);       //捕获上升沿
    Key_Old = Key_New;                              //辅助扫描
    
    //S6按下过0.5s
    if (Key_Old == 6 && Seg_Show_Mode == 0)
    {
            S6_Time_Flag = 1;   //开始计时
        if (Timer1_500ms >= 500)
        {
            DS1302_Write_Rtc(Rtc_Buf_Init);//计时初始化
            DS1302_Read_Rtc(Rtc_Buf_Init);//计时初始化
            S6_Time_Flag = Timer1_500ms = 0;   //计数清零
        }
    }
    else
        S6_Time_Flag = 0;   //停止计时
    
    switch (Key_Down)
    {
        //模式+
        case 4:
            if (++Seg_Show_Mode == 5) Seg_Show_Mode = 0;
        break;
        //模式-
        case 5:
            if (--Seg_Show_Mode == 255) Seg_Show_Mode = 4;
        break;
        //时钟数据保存
        case 6:
            if (Seg_Show_Mode == 0)
                EEPROM_Write(Rtc_Buf, 16, 3);
        break;
    }
    
    //S7  串口发送数据
    if (Key_Down == 7)
    {
        switch (Seg_Show_Mode)
        {
            //时钟发送
            case 0:
                sprintf(Uart_Tx_Buf, "  时间:%2d-%2d-%d\r\n", (unsigned int)Rtc_Buf[0], (unsigned int)Rtc_Buf[1], (unsigned int)Rtc_Buf[2]);
                Uart_Tx_String(Uart_Tx_Buf);
            break;
            //温度发送
            case 1:
                sprintf(Uart_Tx_Buf, "  温度:%.1f度\r\n", T);
                Uart_Tx_String(Uart_Tx_Buf);
            break;
            //电压发送
            case 2:
                sprintf(Uart_Tx_Buf, "  电压:%.2f伏\r\n", AD_DA_Data);
                Uart_Tx_String(Uart_Tx_Buf);
            break;
            //频率发送
            case 3:
                sprintf(Uart_Tx_Buf, "  频率:%.0lf赫兹\r\n", (double)Freq);
                Uart_Tx_String(Uart_Tx_Buf);
            break;
            //距离发送
            case 4:
                sprintf(Uart_Tx_Buf, "  距离:%d厘米\r\n", (unsigned int)Uwave_D);
                Uart_Tx_String(Uart_Tx_Buf);
            break;
        }
    }
}

/* 信息处理函数 */
void Infor_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;                              //减速
    
    /* 信息获取区 */
    DS1302_Read_Rtc(Rtc_Buf);               //时钟数据读取
    T = DS18B20_Read_T();                   //实时温度读取
    AD_DA_Data = AD_Read(0x43) / 51.0;      //RB2实时读取
    Uwave_D = Uwave_Read();                 //距离实时读取
    
    /* 信息显示区 */
    switch (Seg_Show_Mode)
    {
        //实时时钟显示界面
        case 0:
            Seg_Point[6] = Seg_Point[5] = 0;    //熄灭小数点
            Seg_Buf[0] = Rtc_Buf[0] / 10;       //显示时十位
            Seg_Buf[1] = Rtc_Buf[0] % 10;       //显示时个位
            Seg_Buf[2] = 11;                    //显示 -
            Seg_Buf[3] = Rtc_Buf[1] / 10;       //显示分十位
            Seg_Buf[4] = Rtc_Buf[1] % 10;       //显示分个位
            Seg_Buf[5] = 11;                    //显示 -
            Seg_Buf[6] = Rtc_Buf[2] / 10;       //显示秒十位
            Seg_Buf[7] = Rtc_Buf[2] % 10;       //显示秒个位
        break;
        //实时温度读取界面
        case 1:
            Seg_Buf[0] = 14;                            //显示C
            for (i = 1; i < 5; i++)                     //循环
                Seg_Buf[i] = 10;                        //熄灭
            Seg_Buf[5] = (unsigned char)T / 10 % 10;    //显示温度十位
            Seg_Buf[6] = (unsigned char)T % 10;         //显示温度个位
            Seg_Point[6] = 1;                           //显示小数点
            Seg_Buf[7] = (unsigned int)(T*10) % 10;     //显示温度小数点一位
        break;
        //AD_DA显示界面
        case 2:
            Seg_Buf[0] = 18;                                    //显示P
            Seg_Point[5] = 1;                                   //显示小数点
            Seg_Point[6] = 0;                                   //熄灭小数点
            for (i = 1; i < 5; i++)                             //循环
                Seg_Buf[i] = 10;                                //熄灭
            Seg_Buf[5] = (unsigned char)AD_DA_Data % 10;        //显示电压个位
            Seg_Buf[6] = (unsigned char)(AD_DA_Data*10) % 10;   //显示电压小数点1位
            Seg_Buf[7] = (unsigned int)(AD_DA_Data*100) % 10;   //显示电压小数点2位
        break;
        //频率显示界面
        case 3:
            Seg_Point[5] = 0;                               //熄灭小数点
            Seg_Buf[0] = 17;                                //显示F
            Seg_Buf[1] = Seg_Buf[2] = 10;                   //熄灭
            Seg_Buf[3] = Freq / 10000 % 10;                 //显示频率万位
            Seg_Buf[4] = Freq / 1000 % 10;                  //显示频率千位
            Seg_Buf[5] = Freq / 100 % 10;                   //显示频率百位
            Seg_Buf[6] = Freq / 10 % 10;                    //显示频率十位
            Seg_Buf[7] = Freq % 10;                         //显示频率个位
            //高位熄灭
            for (i = 3; i < 7; i++)
            {
                if (Seg_Buf[i] == 0)
                    Seg_Buf[i] = 10;    //熄灭
                else
                    break;              //跳出循环
            }
        break;
        //距离显示界面
        case 4:
            Seg_Buf[1] = Seg_Buf[2] = Seg_Buf[3] = Seg_Buf[4] = 10;//熄灭
            Seg_Buf[0] = 15;                //显示d
            Seg_Buf[5] = Uwave_D / 100 % 10;//显示距离百位
            Seg_Buf[6] = Uwave_D / 10 % 10; //显示距离十位
            Seg_Buf[7] = Uwave_D % 10;      //显示距离个位
            //高位熄灭
            for (i = 5; i < 7; i++)
            {
                if (Seg_Buf[i] == 0)
                    Seg_Buf[i] = 10;                //熄灭
                else
                    break;                          //跳出循环
            }
        break;
    }
    
}

/* 其他处理函数 */
void Other_Deal()
{
    /* DA输出 */
    DA_Write(AD_DA_Data * 51);
    
    /* LED显示 */
    
}

/* 串口处理函数 */
void Uart_Deal()
{
    if (Uart_Index == 8)
    {
        if (Uart_Rx_Buf[0] == 'L' && Uart_Rx_Buf[1] == 'E' && Uart_Rx_Buf[2] == 'D' && Uart_Rx_Buf[4] == ' ' && Uart_Rx_Buf[5] == '=' && Uart_Rx_Buf[6] == ' ')
        {
            if (Uart_Rx_Buf[3] - 48 > 7 || Uart_Rx_Buf[7] - 48 > 1)
            {
                sprintf(Uart_Tx_Buf, "  EEROR\r\n");
                Uart_Tx_String(Uart_Tx_Buf);
            }
            else
            {
                LED_Buf[Uart_Rx_Buf[3] - 48] = Uart_Rx_Buf[7] - 48;
                sprintf(Uart_Tx_Buf, "  OK\r\n");
                Uart_Tx_String(Uart_Tx_Buf);
            }
        }
        else
        {
            sprintf(Uart_Tx_Buf, "  EEROR\r\n");
            Uart_Tx_String(Uart_Tx_Buf);
        }
        Uart_Index = 0;
    }
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0x7F;			//定时器时钟12T模式
	TMOD &= 0xF0;			//设置定时器模式
	TMOD |= 0x05;			//设置定时器模式
	TL0 = 0x00;				//设置定时初始值
	TH0 = 0x00;				//设置定时初始值
	TF0 = 0;				//清除TF0标志
	TR0 = 1;				//定时器0开始计时
}
void Timer1_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0xBF;			//定时器时钟12T模式
	TMOD &= 0x0F;			//设置定时器模式
	TL1 = 0x18;				//设置定时初始值
	TH1 = 0xFC;				//设置定时初始值
	TF1 = 0;				//清除TF1标志
	TR1 = 1;				//定时器1开始计时
    ET1 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    if (++Timer1_1000ms == 1000)
    {
        Freq = (TH0 << 8) | TL0;
        Timer1_1000ms = TH0 = TL0 = 0;
    }
    if (S6_Time_Flag == 1)
    {
        if (++Timer1_500ms >= 500)
            Timer1_500ms = 501; //卡住计时
    }
}

/* 串口中断服务函数 */
void Uart_Sevre() interrupt 4
{
    if (RI == 1)
    {
        Uart_Rx_Buf[Uart_Index++] = SBUF;
        RI = 0;
    }
}

/* 主函数 */
void main()
{
    unsigned char PID;          //PID密码
    Init();                         //系统初始化
    DS18B20_Read_T();               //读取时间
    Timer1_Init();                  //定时器1初始化
    Timer0_Init();                  //定时器0初始化
    Uart_Init();                    //串口1初始化
    
    /**PID校验**/
    EEPROM_Read(&PID, 8, 1);        //PID读取
    if (PID == 66)                  //PID判断
        EEPROM_Read(Rtc_Buf, 16, 3);//时钟读取
    else
    {
        PID = 66;                   //PID设置
        EEPROM_Write(&PID, 8, 1);   //PID写入
        EEPROM_Write(Rtc_Buf, 16, 3);//时钟写入
    }
    DS1302_Write_Rtc(Rtc_Buf);      //写入时间
    /***********/
    
    while (1)
    {
        Key_Deal();
        Infor_Deal();
        Other_Deal();
        Uart_Deal();
    }
}



