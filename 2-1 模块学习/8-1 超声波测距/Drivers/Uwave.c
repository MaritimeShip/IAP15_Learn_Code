#include <STC15F2K60S2.H>
#include <intrins.h>
#include "Uwave.h"

sbit Tx = P1^0;
sbit Rx = P1^1;

void Delay12us()		//@12.000MHz
{
    unsigned char i;

    _nop_();
    _nop_();
    i = 33;
    while (--i);
}

void Uwave_Init()
{
    unsigned char i = 0;
    for (i = 0; i < 8; i++)
    {
        Tx = 1;
        Delay12us();
        Tx = 0;
        Delay12us();
    }
}

unsigned int Uwave_Read()
{
    unsigned int time=0;
    TMOD &= 0x0f;
    TH1= TL1 = 0;
    Uwave_Init();
    TR1=1;
    while((Rx==1)&&(TF1==0));
    TR1=0;
    if(TF1==1)
    {
        TF1=0;
        return 0;
    }
    else
    {
        time = ((TH1<<8)|TL1);
        return (unsigned char)(time*0.017);
    }
}





