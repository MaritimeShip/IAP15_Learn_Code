#include <STC15F2K60S2.H>
#include "Seg.h"



code unsigned char Seg_Table[] =
{
    0xc0, //0
    0xf9, //1
    0xa4, //2
    0xb0, //3
    0x99, //4
    0x92, //5
    0x82, //6
    0xf8, //7
    0x80, //8
    0x90, //9
    0xff, //熄灭
    0xbf, //-
    0x88, //A
    0x83, //b
    0xc6, //C
    0xa1, //d
    0x86, //E
    0x8e  //F
};
code unsigned char Seg_Wula[] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};

void Seg_Show(unsigned char Wula, Table, Point)
{
    P0 = 0xff;                      //消影
    P2 = P2 & 0x1f | 0xe0;
    P2 &= 0x1f;

    P0 = Seg_Wula[Wula];            //位选
    P2 = P2 & 0x1f | 0xc0;
    P2 &= 0x1f;

    P0 = Seg_Table[Table];          //段选
    if (Point)
        P0 &= 0x7f;
    P2 = P2 & 0x1f | 0xe0;
    P2 &= 0x1f;
}













