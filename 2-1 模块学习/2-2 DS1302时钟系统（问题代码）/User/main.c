/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Init.h"
#include "ds1302.h"


/* 变量创建区 */
unsigned int Slow_Down;//按键数码管减速专用
unsigned char Key_UP, Key_Down;
bit Key_Slow_Flag;//按键减速标志位
bit Seg_Slow_Flag;//数码管减速标志位
unsigned char Seg_Buf[8] = {10, 10, 11, 10, 10, 11, 10, 10};//数码管数字显示数组
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};//数码管小数点数组
unsigned char Seg_Poc;//扫描专用
unsigned char usLED[8] = {0, 0, 0, 0, 0, 0, 0, 0};//LED亮灭数组
unsigned char Rec_Set_Mode;//0 --> 显示模式  1 --> 设置模式
unsigned char Rtc_Time_Mode;//0 --> 时分秒显示界面  1 --> 闹钟显示界面  2 -->年月日显示界面
unsigned char Rtc_Clock_Mode;//0 --> 闹钟1显示界面  1 --> 闹钟2显示界面  2 --> 闹钟3显示界面
//时钟显示数组
unsigned char usRtc_Day[3] = {0x22, 0x12, 0x12};//年月日
unsigned char usRtc_Sec[3] = {0x23, 0x59, 0x55};//时分秒
unsigned char usRtc_Ck1[5] = {0x00, 0x00, 0x00, 1, 1};//第4位是否有闹钟 1 --> 有
unsigned char usRtc_Ck2[5] = {0x00, 0x01, 0x00, 1, 1};//第5位是启用闹钟 1 --> 启用
unsigned char usRtc_Ck3[5] = {0x00, 0x00, 0x00, 0, 0};
unsigned char usRtc_Day_Buf[3] = {0x00, 0x00, 0x00};//显示更改
unsigned char usRtc_Sec_Buf[3] = {0x00, 0x00, 0x00};
unsigned char usRtc_Clock_Buf[5] = {0x00, 0x00, 0x00, 0, 0};
unsigned char usRtc_Seg_Buf[8] = {10, 10, 11, 10, 10, 11, 10, 10};//数码管数字显示数组
unsigned char i, j, k, clk;//用于将数据转移到数码管缓冲区当中

/* 延时函数 */
void Delay_ms(unsigned char xms)     //@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Val = 0;
    static unsigned char Key_Old = 0;//按键捕获专用
    
    unsigned char KeyNum = 0x00;//每次清零
    static unsigned char Count = 0; //标记数组位专用
    Count %= 3;
        
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;//按键减速

    Key_Val = Key_Read();
    Key_UP = ~Key_Val & (Key_Val ^ Key_Old);
    Key_Down = Key_Val & (Key_Val ^ Key_Old);
    Key_Old = Key_Val;//捕获键码
    
    if (Rec_Set_Mode)//当为设置模式（4~13,16）
    {
        switch (Key_UP)
        {
            case 4:case 5:case 6:case 7:case 8:case 9:case 10:case 11:case 12:case 13://设置时间
                Key_UP -= 4;
                KeyNum = ((Key_UP / 16) << 4) | (Key_UP % 16);//数据储存
                switch (Rtc_Time_Mode)
                {
                    case 0: usRtc_Sec_Buf[Count++] = KeyNum; break;
                    case 1: usRtc_Clock_Buf[Count++] = KeyNum; break;
                    case 2: usRtc_Day_Buf[Count++] = KeyNum; break;
                }
            break;
            case 16: Rec_Set_Mode = 0; break;//退出设置模式
            default: break;
        }
    }
    else             //当为非设置模式（14,18）
    {
        switch (Key_UP)
        {
            case 14: Rtc_Time_Mode++; Rtc_Time_Mode %= 3; break;
            case 18:// 18  闹钟禁用
                switch (Rtc_Clock_Mode)
                {
                    case 0: usRtc_Ck1[4] = 0; break;
                    case 1: usRtc_Ck2[4] = 0; break;
                    case 2: usRtc_Ck3[4] = 0; break;
                }
            break;
            default: break;
        }
    }
    if (Key_UP == 15) //切换为设置模式  15
    {
        Rec_Set_Mode = 1;
        Read_Rtc_Day(usRtc_Day_Buf);
        Read_Rtc_Sec(usRtc_Sec_Buf);
        for (i = 0, j = 0; i < 3; i++)
        {
            k = j;
            while (j < k + 2)
            {
                switch (Rtc_Time_Mode)
                {
                    case 0:
                        usRtc_Seg_Buf[j] = usRtc_Sec_Buf[i] / 16; j++;
                        usRtc_Seg_Buf[j] = usRtc_Sec_Buf[i] % 16; j += 2;
                    break;
                    case 1:
                        switch (Rtc_Clock_Mode)
                        {
                            case 0:
                                for (clk = 0; clk < 5; clk++)
                                {
                                    usRtc_Clock_Buf[clk] = usRtc_Ck1[clk];
                                }
                                usRtc_Seg_Buf[j] = usRtc_Clock_Buf[i] / 16; j++;
                                usRtc_Seg_Buf[j] = usRtc_Clock_Buf[i] % 16; j += 2;
                            break;
                            case 1:
                                for (clk = 0; clk < 5; clk++)
                                {
                                    usRtc_Clock_Buf[clk] = usRtc_Ck2[clk];
                                }
                                usRtc_Seg_Buf[j] = usRtc_Clock_Buf[i] / 16; j++;
                                usRtc_Seg_Buf[j] = usRtc_Clock_Buf[i] % 16; j += 2;
                            break;
                            case 2:
                                for (clk = 0; clk < 5; clk++)
                                {
                                    usRtc_Clock_Buf[clk] = usRtc_Ck3[clk];
                                }
                                usRtc_Seg_Buf[j] = usRtc_Clock_Buf[i] / 16; j++;
                                usRtc_Seg_Buf[j] = usRtc_Clock_Buf[i] % 16; j += 2;
                            break;
                        }
                    break;
                    case 2:
                        usRtc_Seg_Buf[j] = usRtc_Day_Buf[i] / 16; j++;
                        usRtc_Seg_Buf[j] = usRtc_Day_Buf[i] % 16; j += 2;
                    break;
                }
            }
        }
    }
    if (Rec_Set_Mode != 0 && Rec_Set_Mode != 4)//闹钟界面 17,19
    {
        if (Key_UP == 17)//  17 --> 控制闹钟编号
        {
            Rtc_Clock_Mode++;
            Rtc_Clock_Mode %= 3;
        }
        if (Rec_Set_Mode && Key_UP == 19)//  19
        {
            switch (Rtc_Clock_Mode)
            {
                case 0: usRtc_Ck1[3] = 0; break;
                case 1: usRtc_Ck2[3] = 0; break;
                case 2: usRtc_Ck3[3] = 0; break;
                default: break;
            }
        }
    }
}

/* 信息处理函数 */
void Seg_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;//数码管减速

    Read_Rtc_Day(usRtc_Day);
    Read_Rtc_Sec(usRtc_Sec);
    
    switch (Rtc_Time_Mode)
    {
        //时分秒显示界面
        case 0:
            for (i = 0, j = 0; i < 3; i++)
            {
                k = j;
                while (j < k + 2)
                {
                    Seg_Buf[j] = usRtc_Sec[i] / 16; j++;
                    Seg_Buf[j] = usRtc_Sec[i] % 16; j += 2;
                }
            }
        break;
        //闹钟显示界面
        case 1:
            switch(Rtc_Clock_Mode)
            {
                case 0://闹钟1
                    if (usRtc_Ck1[3])
                    {
                        for (i = 0, j = 0; i < 3; i++)
                        {
                            k = j;
                            while (j < k + 2)
                            {
                                Seg_Buf[j] = usRtc_Ck1[i] / 16; j++;
                                Seg_Buf[j] = usRtc_Ck1[i] % 16; j += 2;
                            }
                        }
                    }
                    else 
                    {
                        for (j = 0; j < 8; j++)
                        {
                            Seg_Buf[j] = 11;
                        }
                    }
                break;
                case 1://闹钟2
                    if (usRtc_Ck2[3])
                    {
                        for (i = 0, j = 0; i < 3; i++)
                        {
                            k = j;
                            while (j < k + 2)
                            {
                                Seg_Buf[j] = usRtc_Ck2[i] / 16; j++;
                                Seg_Buf[j] = usRtc_Ck2[i] % 16; j += 2;
                            }
                        }
                    }
                    else
                    {
                        for (j = 0; j < 8; j++)
                        {
                            Seg_Buf[j] = 11;
                        }
                    }

                break;
                case 2://闹钟3
                    if (usRtc_Ck3[3])
                    {
                        for (i = 0, j = 0; i < 3; i++)
                        {
                            k = j;
                            while (j < k + 2)
                            {
                                Seg_Buf[j] = usRtc_Ck3[i] / 16; j++;
                                Seg_Buf[j] = usRtc_Ck3[i] % 16; j += 2;
                            }
                        }
                    }
                    else 
                    {
                        for (j = 0; j < 8; j++)
                        {
                            Seg_Buf[j] = 11;
                        }
                    }
                break;
            }
        break;
        //年月日显示界面
        case 2:
            for (i = 0, j = 0; i < 3; i++)
            {
                k = j;
                while (j < k + 2)
                {
                    Seg_Buf[j] = usRtc_Day[i] / 16; j++;
                    Seg_Buf[j] = usRtc_Day[i] % 16; j += 2;
                }
            }
        break;
        default: break;
    }
    
}

/* LED处理函数 */
void LED_Proc()
{
    
    
    
    // L8 
    switch (Rtc_Time_Mode)
    {
    case 1:
        if (usRtc_Ck1[3] == 1) usLED[7] = 1;
        else usLED[7] = 0;
    break;
    case 2:
        if (usRtc_Ck2[3] == 1) usLED[7] = 1;
        else usLED[7] = 0;
    break;
    case 3:
        if (usRtc_Ck3[3] == 1) usLED[7] = 1;
        else usLED[7] = 0;
    break;
    }
}

/* 定时器初始化函数 */
void Timer0Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer0_Serve() interrupt 1
{
    static unsigned int Seg_Laght = 0;
    
    if (++Slow_Down == 500) {Slow_Down = 0; Seg_Slow_Flag = 0;}
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    
    if (++Seg_Poc == 8) Seg_Poc = 0;
    LED_Disp(Seg_Poc, usLED[Seg_Poc]);
    
    if (Rec_Set_Mode)//设置模式
    {
        if (++Seg_Laght > 1000 && Seg_Laght < 500)
        {
            Seg_Laght = 0;
            Seg_Disp(Seg_Poc, 10, 0);
        }
        else Seg_Disp(Seg_Poc, usRtc_Seg_Buf[Seg_Poc], Seg_Point[Seg_Poc]);
    }
    else             //显示模式
    {
        Seg_Disp(Seg_Poc, Seg_Buf[Seg_Poc], Seg_Point[Seg_Poc]);
    }
}

/* 主函数 */
void main()
{
    System_Init();//系统初始化
    Timer0Init();//定时器初始化

    Set_Rtc_Day(usRtc_Day);//写入年月日
    Set_Rtc_Sec(usRtc_Sec);//写入时分秒

    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}














