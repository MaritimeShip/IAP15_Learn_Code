#include "Key.h"
#include <STC15F2K60S2.H>



unsigned char Key_Read()
{
    unsigned char Key_Temp = 0;
    
    P34 = 0; P35 = 1; P42 = 1; P44 = 1;
    if (P30 == 0) Key_Temp = 19;
    if (P31 == 0) Key_Temp = 18;
    if (P32 == 0) Key_Temp = 17;
    if (P33 == 0) Key_Temp = 16;
    P34 = 1; P35 = 0; P42 = 1; P44 = 1;
    if (P30 == 0) Key_Temp = 15;
    if (P31 == 0) Key_Temp = 14;
    if (P32 == 0) Key_Temp = 13;
    if (P33 == 0) Key_Temp = 12;
    P34 = 1; P35 = 1; P42 = 0; P44 = 1;
    if (P30 == 0) Key_Temp = 11;
    if (P31 == 0) Key_Temp = 10;
    if (P32 == 0) Key_Temp = 9;
    if (P33 == 0) Key_Temp = 8;
    P34 = 1; P35 = 1; P42 = 1; P44 = 0;
    if (P30 == 0) Key_Temp = 7;
    if (P31 == 0) Key_Temp = 6;
    if (P32 == 0) Key_Temp = 5;
    if (P33 == 0) Key_Temp = 4;
    
    return Key_Temp;
}













