#ifndef __DS1302_H
#define __DS1302_H


void Write_Ds1302(unsigned char temp);
void Write_Ds1302_Byte( unsigned char address,unsigned char dat );
unsigned char Read_Ds1302_Byte( unsigned char address );
void Set_Rtc_Day(unsigned char* ucRtc);
void Set_Rtc_Sec(unsigned char* ucRtc);
void Read_Rtc_Day(unsigned char* ucRtc);
void Read_Rtc_Sec(unsigned char* ucRtc);



#endif
