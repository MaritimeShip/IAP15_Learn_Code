#include "ds1302.h"
#include <reg52.h>
#include <intrins.h>

sbit SCK = P1^7;		
sbit SDA = P2^3;		
sbit RST = P1^3; 

//写字节
void Write_Ds1302(unsigned  char temp) 
{
    unsigned char i;
    for (i=0;i<8;i++)     	
    { 
        SCK = 0;
        SDA = temp&0x01;
        temp>>=1; 
        SCK=1;
    }
}   

//向DS1302寄存器写入数据
void Write_Ds1302_Byte( unsigned char address,unsigned char dat )     
{
    RST=0;	_nop_();
    SCK=0;	_nop_();
    RST=1; 	_nop_();  
    Write_Ds1302(address);	
    Write_Ds1302(dat);		
    RST=0; 
}

//从DS1302寄存器读出数据
unsigned char Read_Ds1302_Byte ( unsigned char address )
{
    unsigned char i,temp=0x00;
    RST=0;	_nop_();
    SCK=0;	_nop_();
    RST=1;	_nop_();
    Write_Ds1302(address);
    for (i=0;i<8;i++) 	
    {		
        SCK=0;
        temp>>=1;	
        if(SDA)
        temp|=0x80;	
        SCK=1;
    } 
    RST=0;	_nop_();
    SCK=0;	_nop_();
    SCK=1;	_nop_();
    SDA=0;	_nop_();
    SDA=1;	_nop_();
    return (temp);			
}


//设置时钟
void Set_Rtc_Day(unsigned char* ucRtc)
{
    Write_Ds1302_Byte(0x8e, 0x00);  //关闭写保护

    Write_Ds1302_Byte(0x8c, ucRtc[0]);//年
    Write_Ds1302_Byte(0x88, ucRtc[1]);//月
    Write_Ds1302_Byte(0x86, ucRtc[2]);//日
    
    Write_Ds1302_Byte(0x8e, 0x80);  //开启写保护
}

void Set_Rtc_Sec(unsigned char* ucRtc)
{
    unsigned char i = 0;
    Write_Ds1302_Byte(0x8e, 0x00);  //关闭写保护
    for (i = 0; i < 3; i++)
        Write_Ds1302_Byte(0x84 - 2*i, ucRtc[i]);
    Write_Ds1302_Byte(0x8e, 0x80);  //开启写保护
}


//读取时钟
void Read_Rtc_Day(unsigned char* ucRtc)
{
    ucRtc[0] = Read_Ds1302_Byte(0x8d);
    ucRtc[1] = Read_Ds1302_Byte(0x89);
    ucRtc[2] = Read_Ds1302_Byte(0x87);
}

void Read_Rtc_Sec(unsigned char* ucRtc)
{
    unsigned char i = 0;
    for (i = 0; i < 3; i++)
        ucRtc[i] = Read_Ds1302_Byte(0x85 - 2*i);
}

















