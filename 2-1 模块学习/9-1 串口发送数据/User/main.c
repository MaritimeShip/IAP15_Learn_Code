/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include <stdio.h>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Uart.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag;                                       //按键数码管减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区

/* 变量创建区 */
bit Uart_Slow_Flag;                     //串口减速标志位
unsigned char Uart_Rxd_Buf[10];         //串口接收数据缓冲区
unsigned char Uart_Rxd_Poc;             //串口接收指针
unsigned char Uart_Txd_Buf[10];         //串口发送数据缓冲区
unsigned char Uart_Data;                //串口数据
unsigned int t;                         //串口数据

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Down = 0, Key_UP = 0;  //变量
    static unsigned char Key_New = 0, Key_Old = 0;  //创建

    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                              //减速
    
    Key_New = Key_Read();                           //获取键码
    Key_Down = Key_New & ( Key_New ^ Key_Old);      //捕获下降沿
    Key_UP = ~Key_New & ( Key_New ^ Key_Old);       //捕获上升沿
    Key_Old = Key_New;                              //辅助扫描
    
    switch(Key_Down)
    {

        case 4:
            sprintf(Uart_Txd_Buf,"T = %d\r\n",t++);
            Uart_Txd_String(Uart_Txd_Buf);
        break;
        case 5:
            Uart_Txd_String("Hello World!\r\n");
        break;
    }
}

/* 信息处理函数 */
void Infor_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;                              //减速
    
}

/* 其他处理函数 */
void Other_Proc()
{
    
}

/* 串口处理函数 */
void Uart_Proc()
{
    if (Uart_Slow_Flag) return;
    Uart_Slow_Flag = 1;                              //减速
    
    if (Uart_Rxd_Poc == 8)
    {
        if (Uart_Rxd_Buf[0] == 'L' && Uart_Rxd_Buf[1] == 'E' && Uart_Rxd_Buf[2] == 'D' && Uart_Rxd_Buf[4] == ' ' && Uart_Rxd_Buf[5] == '=' && Uart_Rxd_Buf[6] == ' ')
            LED_Buf[Uart_Rxd_Buf[3] - 48] = Uart_Rxd_Buf[7] - 48;
        Uart_Rxd_Poc = 0;
    }
    
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 200 == 0) Uart_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
}

/* 串口中断服务函数 */
void Uart_Serve() interrupt 4
{
    if (RI == 1)
    {
        Uart_Rxd_Buf[Uart_Rxd_Poc++] = SBUF;
        RI = 0;
    }
}

/* 延时函数 */
void Delay_ms(unsigned int xms)		//@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 主函数 */
void main()
{
    Init();
    Timer0_Init();
    Uart_Init();
    while (1)
    {
        Key_Proc();
        Infor_Proc();
        Other_Proc();
        Uart_Proc();
    }
}



