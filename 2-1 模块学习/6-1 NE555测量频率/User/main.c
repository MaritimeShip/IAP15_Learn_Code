/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"


/* 模版变量声明区 */
unsigned int Slow_Down;                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag;       //按键数码管减速标志位
unsigned char Key_UP, Key_Down;         //按键捕获上升下降沿
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};                //数码管数字显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                      //数码管小数点显示缓冲区
unsigned char usLED[8] = {0, 0, 0, 0, 0, 0, 0, 0};                          //LED显示缓冲区

/* 用户自定义变量声明区 */
unsigned int Freq, Time1s;

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Val = 0, Key_Old = 0;          //按键取键码，辅助扫描专用
    
    if (Key_Slow_Flag) return;                              //减速
    Key_Slow_Flag = 1;
    
    Key_Val = Key_Read();                                   //获取键码
    Key_Down = Key_Val & (Key_Val ^ Key_Old);               //捕获下降沿
    Key_Old = Key_Val;                                      //辅助扫描
    
    switch (Key_Down)
    {
        case 4:
            Seg_Buf[0] = 4;
            usLED[0] ^= 1;
        break;
    }
}

/* 数码管处理函数 */
void Seg_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    Seg_Buf[0] = Freq / 10000 % 10;
    Seg_Buf[1] = Freq / 1000 % 10;
    Seg_Buf[2] = Freq / 100 % 10;
    Seg_Buf[3] = Freq / 10 % 10;
    Seg_Buf[4] = Freq % 10;
}

/* LED处理函数 */
void LED_Proc()
{
    
}

/* 定时器初始化函数 */
void Timer1_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0xBF;			//定时器时钟12T模式
    TMOD &= 0x0F;			//设置定时器模式
    TL1 = 0x18;				//设置定时初始值
    TH1 = 0xFC;				//设置定时初始值
    TF1 = 0;				//清除TF1标志
    TR1 = 1;				//定时器1开始计时
    ET1 = 1;
    EA  = 1;
}
void Timer0_Init(void)		//0毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TMOD |= 0x05;
    TL0 = 0x00;				//设置定时初始值
    TH0 = 0x00;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
}

/* 定时器中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Disp(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Disp(Slow_Down%8, usLED[Slow_Down%8]);
    
    if (++Time1s == 1000)
    {
        Time1s = 0;
        Freq = TH0 << 8 | TL0;
        TH0 = 0;
        TL0 = 0;
    }
}

/* 主函数 */
void main()
{
    Timer1_Init();
    Timer0_Init();
    System_Init();
    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}










