/************************************************************************/
//头文件声明区
#include <STC15F2K60S2.H>
#include <intrins.h>
#include "Key.h"
#include "Latch.h"

/************************************************************************/
//变量声明区
unsigned char LED_Mode = 1;         //LED模式位
unsigned char LED_Flag;             //启动停止标志位
unsigned char LED_12_Time_Flag;     //模式1,2时间控制位
unsigned char LED_3_Time_Flag;      //模式3时间控制位
unsigned char LED_4_Time_Flag;      //模式4时间控制位
unsigned char KeyNum;               //用用获取按键值
unsigned char Key_Val, Key_Down, Key_Old;   //用于下降沿检测

//LED模式3,4彩灯数组
unsigned char LED_3_Control[4] = { ~0x81, ~0x42, ~0x24, ~0x18 };
unsigned char LED_4_Control[4] = { ~0x18, ~0x24, ~0x42, ~0x81 };

/************************************************************************/
//定时器初始化函数
void Timer0_Init(void)      //1毫秒@12.000MHz
{
    AUXR &= 0x7F;           //定时器时钟12T模式
    TMOD &= 0xF0;           //设置定时器模式
    TL0 = 0x18;             //设置定时初始值
    TH0 = 0xFC;             //设置定时初始值
    TF0 = 0;                //清除TF0标志
    TR0 = 1;                //定时器0开始计时
    ET0 = 1;                //开启定时器0中断
    EA  = 1;                //开启总中断
}

/************************************************************************/
//LED模式选择函数
void LED_Mode_Select()
{
    //直接背下来就行
    Key_Val = Key_Read();
    Key_Down = Key_Val & (Key_Val ^ Key_Old);
    Key_Old = Key_Val;    
    
    switch (Key_Down)
    {
        //启动
        case 4:
            LED_Flag = 1;
            ET0 = 1;
            break;
        //停止
        case 5:
            LED_Flag = 0;
            ET0 = 0;
            break;
        //模式加
        case 6:
            LED_Mode++;
            if (LED_Mode == 5)
                LED_Mode = 1;
        
            LED_12_Time_Flag = 0;
            LED_3_Time_Flag  = 0;
            LED_4_Time_Flag  = 0;
            break;
        //模式减
        case 7:
            LED_Mode--;
            if (LED_Mode == 0)
                LED_Mode = 4;
        
            LED_12_Time_Flag = 0;
            LED_3_Time_Flag  = 0;
            LED_4_Time_Flag  = 0;
            break;
        //没有按下时
        default: break;
    }

}

/************************************************************************/
//主函数
void main()
{
    Timer0_Init();          //定时器0初始化
    P0 = 0xFF; Latch(4);    //熄灭所有LED
    while (1)
    {
        LED_Mode_Select();  //LED模式控制
        if (LED_Flag)
        {
            switch (LED_Mode)
            {
                case 1:     //LED模式1
                    P0 = ~(0x01 << LED_12_Time_Flag); Latch(4);
                    break;
                case 2:     //LED模式2
                    P0 = ~(0x80 >> LED_12_Time_Flag); Latch(4);
                    break;
                case 3:     //LED模式3
                    P0 = LED_3_Control[LED_3_Time_Flag]; Latch(4);
                    break;
                case 4:     //LED模式4
                    P0 = LED_4_Control[LED_4_Time_Flag]; Latch(4);
                    break;
                default: break;
            }
        }
    }
}

/************************************************************************/
//定时器0中断服务函数
void Timer0_Serve() interrupt 1
{
    static unsigned int Timer0_Count = 0;
        
    if (++Timer0_Count > 500)
    {
        Timer0_Count = 0;
        if (LED_Flag)
        {
            LED_12_Time_Flag++;
            LED_12_Time_Flag %= 8;
            
            LED_3_Time_Flag++;
            LED_3_Time_Flag %= 4;
            
            LED_4_Time_Flag++;
            LED_4_Time_Flag %= 4;
        }
    }
}









