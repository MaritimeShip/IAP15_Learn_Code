/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include <stdio.h>
#include <string.h>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "ds1302.h"
#include "onewire.h"
#include "iic.h"
#include "Uwave.h"
#include "Uart.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用变量
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;       //按键数码管LED信息处理减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned char Seg_Show_Mode;                                    //数码管显示模式  0-实时时钟  1-实时温度  2-实时电压  3-实时频率  4-实时距离
unsigned char Rtc_Real_Buf[3] = {23, 59, 56};                   //实时时钟储存数据
float T, AD_DA_Data;                                            //实时温度, 实时电压
unsigned char Time_75ms;                                        //75ms计时
bit Beep_Flag;                                                  //蜂鸣器标志位  0-不叫  1-叫
unsigned int Freq, Time_1000ms;                                 //实时频率，1000ms计时
unsigned char Dist;                                             //实时距离
unsigned char Uart_Rx_Index;                                    //串口接收指针
idata unsigned char Uart_Rx_Buf[20];                            //串口接收数据缓冲区
bit Uart_Rx_Flag;                                               //串口接收标志位
unsigned char Uart_10ms;                                        //串口接收10ms计时

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;  //按键获取
    static unsigned char Key_UP = 0, Key_Down = 0;  //专用变量
    
    if (Key_Slow_Flag) return;                  //减速
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();                       //获取键码
    Key_UP = ~Key_New & (Key_New ^ Key_Old);    //获取上升沿
    Key_Down = Key_New & (Key_New ^ Key_Old);   //获取下降沿
    Key_Old = Key_New;                          //辅助扫描
    
    //按键处理
    switch (Key_Down)
    {
        //模式切换
        case 4:
            if (++Seg_Show_Mode == 5) Seg_Show_Mode = 0;
        break;
        //时钟保存
        case 5:
            if (Seg_Show_Mode == 0)     //时钟显示模式
            {
                EEPROM_Write(Rtc_Real_Buf, 0, 3);   //数据写入
                Beep_Flag = 1;                      //开蜂鸣器
                Time_75ms = 0;                      //计时清0
            }
        break;
        //时钟初始化
        case 6:
            if (Seg_Show_Mode == 0)     //时钟显示模式
            {
                Rtc_Real_Buf[0] = 23;           //时重置
                Rtc_Real_Buf[1] = 59;           //分重置
                Rtc_Real_Buf[2] = 56;           //秒重置
                DS1302_Write_Rtc(Rtc_Real_Buf); //数据写入
            }
        break;
        //串口打印数据
        case 7:
            switch (Seg_Show_Mode)
            {
                case 0: printf("时间：%2d-%2d-%2d\r\n", (unsigned int)Rtc_Real_Buf[0], (unsigned int)Rtc_Real_Buf[1], (unsigned int)Rtc_Real_Buf[2]); break;
                case 1: printf("温度：%.1f度\r\n", T); break;
                case 2: printf("电压：%.2f伏\r\n", AD_DA_Data); break;
                case 3: printf("频率：%u赫兹\r\n", Freq); break;
                case 4: printf("距离：%d厘米\r\n", (unsigned int)Dist); break;
            }
        break;
    }
    
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;      //减速
    Seg_Slow_Flag = 1;
    
    switch (Seg_Show_Mode)
    {
        //实时时钟显示
        case 0:
            Seg_Point[5] = 0;                       //小数点
            Seg_Point[6] = 0;                       //小数点
            Seg_Buf[0] = Rtc_Real_Buf[0] / 10;      //时十位
            Seg_Buf[1] = Rtc_Real_Buf[0] % 10;      //时个位
            Seg_Buf[2] = 11;                        //显示-
            Seg_Buf[3] = Rtc_Real_Buf[1] / 10;      //分十位
            Seg_Buf[4] = Rtc_Real_Buf[1] % 10;      //分个位
            Seg_Buf[5] = 11;                        //显示-
            Seg_Buf[6] = Rtc_Real_Buf[2] / 10;      //秒十位
            Seg_Buf[7] = Rtc_Real_Buf[2] % 10;      //秒个位
        break;
        //实时温度显示
        case 1:
            Seg_Point[5] = 0;                       //小数点
            Seg_Point[6] = 1;                       //小数点
            Seg_Buf[0] = 14;                        //显示C
            for (i = 1; i < 5; i++)                 //熄灭
                Seg_Buf[i] = 10;
            Seg_Buf[5] = (unsigned char)T / 10 % 10;//温度十位
            Seg_Buf[6] = (unsigned char)T % 10;     //温度个位
            Seg_Buf[7] = (unsigned int)(T*10) % 10; //温度小数1位
        break;
        //实时电压显示
        case 2:
            Seg_Point[6] = 0;                                   //小数点
            Seg_Point[5] = 1;                                   //小数点
            for (i = 1; i < 5; i++)                             //熄灭
                Seg_Buf[i] = 10;
            Seg_Buf[0] = 18;                                    //显示U
            Seg_Buf[5] = (unsigned char)AD_DA_Data % 10;        //电压个位
            Seg_Buf[6] = (unsigned int)(AD_DA_Data*10) % 10;    //电压小数1位
            Seg_Buf[7] = (unsigned int)(AD_DA_Data*100) % 10;   //电压小数2位
        break;
        //实时频率显示
        case 3:
            Seg_Point[5] = 0;                       //小数点
            Seg_Point[6] = 0;                       //小数点
            Seg_Buf[0] = 17;                        //显示F
            Seg_Buf[1] = Seg_Buf[2] = 10;           //熄灭
            Seg_Buf[3] = Freq / 10000 % 10;         //频率万位
            Seg_Buf[4] = Freq / 1000 % 10;          //频率千位
            Seg_Buf[5] = Freq / 100 % 10;           //频率百位
            Seg_Buf[6] = Freq / 10 % 10;            //频率十位
            Seg_Buf[7] = Freq % 10;                 //频率个位
            for (i = 3; i < 7; i++)     //高位熄灭
            {
                if (Seg_Buf[i] == 0)    //高位为0
                    Seg_Buf[i] = 10;    //熄灭
                else                    //高位不为0
                    break;              //跳出循环
            }
        break;
        //实时距离显示
        case 4:
            Seg_Point[5] = 0;                       //小数点
            Seg_Point[6] = 0;                       //小数点
            Seg_Buf[0] = 15;                        //显示d
            for (i = 1; i < 5; i++)                 //熄灭
                Seg_Buf[i] = 10;
            Seg_Buf[5] = Dist / 100 % 10;           //距离百位
            Seg_Buf[6] = Dist / 10 % 10;            //距离十位
            Seg_Buf[7] = Dist % 10;                 //距离个位
        break;
    }
    
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;      //减速
    LED_Slow_Flag = 1;
    
    //L1~L5互斥点亮
    for (i = 0; i < 5; i++)
        LED_Buf[i] = (i == Seg_Show_Mode);
}

/* 信息处理函数 */
void Infor_Deal()
{
    /* 信息输出区 */
    DA_Write(AD_DA_Data * 51);          //DA输出电压
    Beep(Beep_Flag);                    //蜂鸣器
    Relay((Dist > 50));
    
    /* 减速 */
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 信息读取区 */
    DS1302_Read_Rtc(Rtc_Real_Buf);      //实时时钟读取
    T = DS18B20_Read_T();               //实时温度读取
    AD_DA_Data = AD_Read(0x43) / 51.0;  //实时电压读取
    Dist = Uwave_Read_Distance();       //实时距离读取
    
    /* 信息处理区 */
    
}

/* 串口服务函数 */
void Uart_Deal()
{
    if (Uart_Rx_Index == 0) return;
    
    if (Uart_10ms >= 10)
    {
        Uart_10ms = Uart_Rx_Flag = 0;
        
        if (Uart_Rx_Index == 17)
        {
            if (Uart_Rx_Buf[0] == 'S' && Uart_Rx_Buf[1] == 'e' && Uart_Rx_Buf[2] == 'g' && Uart_Rx_Buf[3] == '_' && Uart_Rx_Buf[4] == 'S' && Uart_Rx_Buf[5] == 'h' && Uart_Rx_Buf[6] == 'o' && Uart_Rx_Buf[7] == 'w' && 
                Uart_Rx_Buf[8] == '_' && Uart_Rx_Buf[9] == 'M' && Uart_Rx_Buf[10] == 'o' && Uart_Rx_Buf[11] == 'd' && Uart_Rx_Buf[12] == 'e' && Uart_Rx_Buf[13] == ' ' && Uart_Rx_Buf[14] == '=' && Uart_Rx_Buf[15] == ' ')
            {
                if (Uart_Rx_Buf[16] >= '0' && Uart_Rx_Buf[16] <= '4')
                {
                    Seg_Show_Mode = Uart_Rx_Buf[16] - 48;
                    printf("OK\r\n");
                }
                else
                    printf("ERROR\r\n");
            }
            else
                printf("ERROR\r\n");
            
        }
        
        memset(Uart_Rx_Buf, 0, Uart_Rx_Index);
        Uart_Rx_Index = 0;
    }
    
}

/* 定时器初始化函数 */
void Timer0_Init(void)      //1毫秒@12.000MHz
{
    AUXR &= 0x7F;           //定时器时钟12T模式
    TMOD &= 0xF0;           //设置定时器模式
    TMOD |= 0x05;
    TL0 = 0x00;             //设置定时初始值
    TH0 = 0x00;             //设置定时初始值
    TF0 = 0;                //清除TF0标志
    TR0 = 1;                //定时器0开始计时
}
void Timer1_Init(void)      //1毫秒@12.000MHz
{
    AUXR &= 0xBF;           //定时器时钟12T模式
    TMOD &= 0x0F;           //设置定时器模式
    TL1 = 0x18;             //设置定时初始值
    TH1 = 0xFC;             //设置定时初始值
    TF1 = 0;                //清除TF1标志
    TR1 = 1;                //定时器1开始计时
    ET1 = 1;                //开启定时器0中断
    EA  = 1;                //开启总中断
}

/* 中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;    //信息处理减速
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;                 //按键减速
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;                 //LED减速
    if (Slow_Down % 200 == 0) Seg_Slow_Flag = 0;                //数码管减速
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);    //数码管显示
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);                            //LED显示
    
    //蜂鸣器75ms计时
    if (Beep_Flag == 1 && ++Time_75ms == 75)   
        Beep_Flag = Time_75ms = 0;
    
    //频率计算
    if (++Time_1000ms == 1000)                 
    {
        Freq = (TH0 << 8) | TL0;
        Time_1000ms = TL0 = TH0 = 0;
    }
    
    //串口计时
    if (Uart_Rx_Flag)
        Uart_10ms++;
}
void Uart_Serve() interrupt 4
{
    if (RI == 1)
    {
        Uart_Rx_Flag = 1;
        Uart_10ms = 0;
        Uart_Rx_Buf[Uart_Rx_Index++] = SBUF;
        RI = 0;
    }
    
    if (Uart_10ms == 5 && Uart_Rx_Index > 17) Uart_Rx_Index = 0;
}

/* 主函数 */
void main()
{
    unsigned char Password = 0;
    
    /* 上电初始化 */
    System_Init();
    Timer1_Init();
    Timer0_Init();
    Uart_Init();
    DS18B20_Read_T();
    
    /* 密码验证 */
    EEPROM_Read(&Password, 8, 1);        //读取密码
    if (Password == 55)                       //若密码正确
        EEPROM_Read(Rtc_Real_Buf, 0, 3); //时钟读取
    else                                 //若密码错误
    {
        Password = 55;                   //写入密码
        EEPROM_Write(&Password, 8, 1);
    }
    DS1302_Write_Rtc(Rtc_Real_Buf);      //时钟写入
    
    while (1)
    {
        Key_Deal();
        Seg_Deal();
        LED_Deal();
        Infor_Deal();
        Uart_Deal();
    }
}




