#include <STC15F2K60S2.H>
#include <intrins.h>
#include "Uwave.h"

sbit Tx = P1^0;
sbit Rx = P1^1;


void Delay12us()		//@12.000MHz
{
    unsigned char i;

    _nop_();
    _nop_();
    i = 33;
    while (--i);
}

void Uwave_Init()
{
    unsigned char i = 0;
    for (i = 0; i < 8; i++)
    {
        Tx = 1;
        Delay12us();
        Tx = 0;
        Delay12us();
    }
}

unsigned char Uwave_Read_Distance()
{
    unsigned int time = 0;
    CMOD = 0x00;
    CH = CL = 0;
    Uwave_Init();
    CR = 1;
    while (Rx == 1 && CF == 0);
    CR = 0;
    if (CF == 1)
    {
        CF = 0;
        return 0;
    }
    else
    {
        time = (CH << 8) | CL;
        return (unsigned char)(time * 0.017);
    }
}

