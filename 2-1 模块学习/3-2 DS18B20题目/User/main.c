/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Init.h"
#include "onewire.h"

/* 变量创建区 */
//模版变量
unsigned int Slow_Down; //按键数码管减速专用
bit Key_Slow_Flag = 0, Seg_Slow_Flag = 0; //按键数码管减速标志位
unsigned char Key_Val, Key_Old, Key_Down, Key_UP; //按键获取上升下降沿
unsigned char Sign_Poc;//扫描显示数组专用
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};//数码管数字缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};//数码管小数点缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};//LED显示缓冲区
float T;//温度
//题目变量
unsigned char Tem_Mode;             // 0 --> 温度显示界面;  1 --> 参数设置界面;
unsigned char TMAX = 30;            //最高温初始化设置为30度
unsigned char TMIN = 20;            //最低温初始化设置为20度
unsigned char T_Temp;               //控制温度加减的中间变量
unsigned char T_Set_Flag;           // 0 --> TMAX;  1 --> TMIN;
unsigned char TMAX_Flag, TMIN_Flag; //控制闪烁专用
unsigned int T_Num;                 //控制闪烁专用

/* 按键处理函数 */
void Key_Proc()
{
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_Val = Key_Read();//获取键码
    Key_Down = Key_Val & (Key_Val ^ Key_Old);
    Key_UP = ~Key_Val & (Key_Val ^ Key_Old);//获取上升下降沿
    Key_Old = Key_Val;//辅助扫描

    switch (Key_UP)
    {
        //界面切换
        case 12:
            Tem_Mode ^= 1; 
            //控制总是TMAX先闪烁
            if (Tem_Mode) 
            {
                T_Temp = TMAX;//温度记录
                T_Set_Flag = 0;
                TMAX_Flag = 1;
                TMIN_Flag = 1;
            }
            //若参数不合法
            if (TMAX <= TMIN)
            {
                if (T_Set_Flag)//当之前是在设置TMAX的时候
                    TMIN = T_Temp;//令TMAX不保存
                else
                    TMAX = T_Temp;//令TMIN不保存
            }
        break;
        //参数切换
        case 13:
            T_Set_Flag ^= 1; 
            T_Num = 0;       //计数清零
            //判断参数是否合法
            if (TMAX <= TMIN)//若参数不合法
            {
                if (T_Set_Flag)//当之前是在设置TMAX的时候
                    TMAX = T_Temp;//令TMAX不保存
                else
                    TMIN = T_Temp;//令TMIN不保存
            }
            //保持一个闪烁另一个常亮
            if (T_Set_Flag)
            {
                TMAX_Flag = 1;
                T_Temp = TMIN;//温度记录
            }
            else 
            {
                TMIN_Flag = 1;
                T_Temp = TMAX;//温度记录
            }
            break;
        //参数重置
        case 16:
            Tem_Mode = 0;
            TMAX = 30;
            TMIN = 20;
        break;
    }
    if (Tem_Mode)
    {
        switch (Key_UP)
        {
            //参数加
            case 14:
                if (T_Set_Flag) TMIN++;
                else TMAX++;
            break;
            //参数减
            case 15:
                if (T_Set_Flag) TMIN--;
                else TMAX--;
            break;
        }
    }
}

/* 信息处理函数 */
void Seg_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;  //数码管减速
    
    T = DS18B20_ReadTem();  //读取温度
    
    switch (Tem_Mode)
    {
        //温度显示界面
        case 0:
            Seg_Buf[0] = 13;                            //显示 C
            Seg_Buf[3] = 10;                            //熄灭
            Seg_Buf[4] = (unsigned char)T / 10 % 10;    //显示十位
            Seg_Buf[5] = (unsigned char)T % 10;         //显示个位
            Seg_Point[5] = 1;                           //显示小数点
            Seg_Buf[6] = (unsigned int)(T * 10) % 10;   //小数位
            Seg_Buf[7] = 13;                            //显示 C
        break;
        //参数设置界面
        case 1:
            //TMAX
            Seg_Buf[3] = TMAX_Flag ? (TMAX / 10 % 10) : 10;  //显示十位或闪烁
            Seg_Buf[4] = TMAX_Flag ? (TMAX % 10) : 10;       //显示个位
            //TMIN
            Seg_Buf[6] = TMIN_Flag ? (TMIN / 10 % 10) : 10;  //显示十位
            Seg_Buf[7] = TMIN_Flag ? (TMIN % 10) : 10;       //显示个位
            //其他基础显示
            Seg_Buf[0] = 14;                            //显示 P
            Seg_Buf[5] = 11;                            //显示 '-'
            Seg_Point[5] = 0;                           //不显示小数点
        break;
    }
}

/* LED处理函数 */
void LED_Proc()
{
    //实测温度报警
    if (T > TMAX)
    {
        LED_Buf[0] = 1;
        LED_Buf[1] = 0;
        LED_Buf[2] = 0;
    }
    else if (T < TMIN)
    {
        LED_Buf[0] = 0;
        LED_Buf[1] = 0;
        LED_Buf[2] = 1;
    }
    else
    {
        LED_Buf[0] = 0;
        LED_Buf[1] = 1;
        LED_Buf[2] = 0;
    }
    //设置高低温错误
    if (TMAX <= TMIN)
        LED_Buf[3] = 1;
    else
        LED_Buf[3] = 0;
}

/* 中断初始化函数 */
void Timer0Init(void)       //1毫秒@12.000MHz
{
    AUXR &= 0x7F;           //定时器时钟12T模式
    TMOD &= 0xF0;           //设置定时器模式
    TL0 = 0x18;             //设置定时初始值
    TH0 = 0xFC;             //设置定时初始值
    TF0 = 0;                //清除TF0标志
    TR0 = 1;                //定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down >= 500) {Slow_Down = 0;Seg_Slow_Flag = 0;}
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (++Sign_Poc == 8) Sign_Poc = 0;//显示数组专用
    Seg_Disp(Sign_Poc, Seg_Buf[Sign_Poc], Seg_Point[Sign_Poc]);
    LED_Disp(Sign_Poc, LED_Buf[Sign_Poc]);
    if (Tem_Mode)
    {
        if (T_Set_Flag == 0)
        {
            
            if (++T_Num >= 500) {TMAX_Flag ^= 1;T_Num = 0;}//500ms闪烁
        }
        else
        {
            if (++T_Num >= 500) {TMIN_Flag ^= 1;T_Num = 0;}//500ms闪烁
        }
    }
}

/* 延时函数 */
void Delay_ms(unsigned int xms)     //@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 主函数 */
void main()
{
    Timer0Init();
    System_Init();
    DS18B20_ReadTem();//读取温度
    Delay_ms(750);
    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}







