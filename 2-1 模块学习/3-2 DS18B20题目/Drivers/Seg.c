#include <STC15F2K60S2.H>
#include "Seg.h"
                           //     0     1     2      3    4      5    6     7     8     9     空    '-'   A     C      P
code unsigned char Seg_Dula[] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90, 0xFF, 0xBF, 0x88, 0xC6, 0x8c};
code unsigned char Seg_Wula[] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};


void Seg_Disp(unsigned char Wula, Dula, Point)
{
    P0 = 0xFF;//消影
    P2 = P2 & 0x1f | 0xe0;
    P2 &= 0x1f;
    
    P0 = Seg_Wula[Wula];//位选
    P2 = P2 & 0x1f | 0xc0;
    P2 &= 0x1f;
    
    P0 = Seg_Dula[Dula];//段选
    if (Point){P0 &= 0x7f;}//小数点
    P2 = P2 & 0x1f | 0xe0;
    P2 &= 0x1f;
}









