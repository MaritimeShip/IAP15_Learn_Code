/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "iic.h"

/* 变量创建区 */
//模版变量
unsigned int Slow_Down;         //减速扫描专用
unsigned char Key_Slow_Flag;    //按键减速标志位
unsigned char Seg_Slow_Flag;    //数码管减速标志位
unsigned char Key_UP, Key_Down; //捕获按键上升下降沿专用
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};    //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};          //数码管小数点显示缓冲区
unsigned char usLED[8] = {0, 0, 0, 0, 0, 0, 0, 0};              //LED显示缓冲区
//题目变量
unsigned char dat[2] = {30, 20};

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Val = 0, Key_Old = 0;//按键捕获键码专用
    
    if (Key_Slow_Flag) return;              //按键减速
    Key_Slow_Flag = 1;
    
    Key_Val = Key_Read();                   //获取键码
    Key_UP = ~Key_Val & (Key_Val ^ Key_Old);//捕获上升沿
    Key_Old = Key_Val;                      //辅助扫描
    
    switch (Key_UP)
    {
        case 4: dat[0]++; break;
        case 5: dat[1]++; break;
        case 6: EEPROM_Write(dat, 0, 2); break;
        case 7: EEPROM_Read(dat, 0, 2); break;
    }
}

/* 数码管处理函数 */
void Seg_Proc()
{
    if (Seg_Slow_Flag) return;              //按键减速
    Seg_Slow_Flag = 1;
    
    Seg_Buf[0] = dat[0] / 10; //十位
    Seg_Buf[1] = dat[0] % 10; //个位
    
    Seg_Buf[6] = dat[1] / 10; //十位
    Seg_Buf[7] = dat[1] % 10; //个位
}

/* LED处理函数 */
void LED_Proc()
{
    
}

/* 中断初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Disp(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);//数码管扫描显示
    LED_Disp(Slow_Down%8, usLED[Slow_Down%8]);                          //LED扫描显示
}

/* 主函数 */
void main()
{
    Timer0_Init();
    System_Init();
//    EEPROM_Read(dat, 0, 2);
    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}
