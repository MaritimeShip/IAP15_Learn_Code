/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Init.h"
#include "iic.h"

/* 变量创建区 */
//模版变量创建区
unsigned int Slow_Down;                //按键数码管计数专用
bit Key_Slow_Flag, Seg_Slow_Flag;       //按键数码管减速标志位
unsigned char Key_UP, Key_Down;         //按键上升下降沿捕获专用
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};    //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};          //数码管小数点缓冲区
unsigned char usLED[8] = {0, 0, 0, 0, 0, 0, 0, 0};              //LED显示缓冲区
//题目变量创建区
float dat1, dat2 = 2.0;       //AD-DA专用
unsigned char V_Mode;         //0 --> 电压显示界面;   1 --> 电压输出界面
unsigned char V_Input_Mode;   //0 --> 输出2.0V;   1 --> 输出电位器对应值
unsigned char LED_Mode;       //0 --> 开启LED;   1 --> 关闭LED
unsigned char Seg_Mode;       //0 --> 开启数码管;   1 --> 关闭数码管
unsigned char i;              //控制LED数码管显示熄灭

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Val = 0, Key_Old = 0;  //按键扫描专用
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;              //按键减速
    
    Key_Val = Key_Read();                       //获取键码值
    Key_UP = ~Key_Val & (Key_Val ^ Key_Old);    //捕获上升沿
    Key_Old = Key_Val;                          //辅助扫描
    
    switch (Key_UP)
    {
        //切换显示界面
        case 4: V_Mode ^= 1; break;
        //切换输出模式
        case 5: if (V_Mode) V_Input_Mode ^= 1; break;
        //控制LED显示
        case 6: LED_Mode ^= 1; break;
        //控制数码管显示
        case 7: Seg_Mode ^= 1; break;
    }
}

/* 数码管处理函数 */
void Seg_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;              //数码管减速
    
    dat1 = AD_ReadData(0x43) / 51.0;//读取电压
    DA_WriteData(dat2 * 51);        //输出电压
    if (V_Input_Mode) dat2 = dat1;  //设置电位器调整输出电压
    else dat2 = 2.0;                //设置输出位2.0V

    
    if (Seg_Mode == 0)
    {
        switch (V_Mode)
        {
            //电压显示界面
            case 0:
                Seg_Buf[0] = 12;                                 //显示 U
                Seg_Buf[5] = (unsigned char)dat1 % 10;           //取个位
                Seg_Point[5] = 1;                                //显示小数点
                Seg_Buf[6] = (unsigned char)(dat1 * 10) % 10;    //取小数点1位
                Seg_Buf[7] = (unsigned int)(dat1 * 100) % 10;    //取小数点2位
            break;
            //电压输出界面
            case 1:
                Seg_Buf[0] = 13;                                 //显示 F
                Seg_Buf[5] = (unsigned char)dat2 % 10;           //取个位
                Seg_Point[5] = 1;                                //显示小数点
                Seg_Buf[6] = (unsigned char)(dat2 * 10) % 10;    //取小数点1位
                Seg_Buf[7] = (unsigned int)(dat2 * 100) % 10;    //取小数点2位
            break;
        }
    }
    else 
    {
        for (i = 0; i < 8; i++)
        {
            Seg_Buf[i] = 10;
            Seg_Point[i] = 0;
        }
    }
}

/* LED处理函数 */
void LED_Proc()
{
    if (LED_Mode == 0)
    {
        //L1, L2
        if (V_Mode)
        {
            usLED[0] = 1;
            usLED[1] = 0;
        }
        else
        {
            usLED[0] = 0;
            usLED[1] = 1;
        }
        //L3
        if (dat1 < 1.5 || (dat1 >= 2.5 && dat1 < 3.5))
            usLED[2] = 0;
        else
            usLED[2] = 1;
    }
    else 
    {
        for (i = 0; i < 8; i++)
        {
            usLED[i] = 0;
        }
    }

}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer0_Sever() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Disp(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Disp(Slow_Down%8, usLED[Slow_Down%8]);
}

/* 主函数 */
void main()
{
    Timer0_Init();
    System_Init();
    
    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}


