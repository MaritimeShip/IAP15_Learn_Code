/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "ds1302.h"
#include "onewire.h"
#include "iic.h"

/* 模板变量创建区 */
unsigned int Slow_Down_zy;                                          //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag;                                   //按键数码管减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};        //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};              //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                //LED显示缓冲区
    
/* 题目变量创建区 */
unsigned char Seg_Disp_Mode;                            //0 --> 时钟   1 --> 温度   2 --> NE555   3 --> AD-DA   4 --> 温度上下值AT24C06
unsigned char Real_Time[3] = {0x13, 0x14, 0x00};        //实时时钟数组
unsigned int NE555, Timer1000ms;                        //频率，1秒定时
float T, Voltage;                                       //实时温度，电压
bit Voltage_Control;                                    //0 --> 电位器   1 --> 光敏电阻

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_Num = 0, Key_Old = 0;
    static unsigned char Key_UP = 0;
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_Num = Key_Read();
    Key_UP = ~Key_Num & (Key_Num ^ Key_Old);
    Key_Old = Key_Num;
    
    switch (Key_UP)
    {
        case 4:
            if (++Seg_Disp_Mode == 4) Seg_Disp_Mode = 0;
        break;
        case 5:
            Voltage_Control ^= 1;
        break;
    }
    
}

/* 信息处理函数 */
void Seg_Deal()
{
    unsigned char i = 0;
    
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    T = DS18B20_Read();
    DS1302_Read_Date(Real_Time);
    if (Voltage_Control == 0)
        Voltage = AD_Input(0x43) / 51.0;
    else
        Voltage = AD_Input(0x41) / 51.0;
    
    switch (Seg_Disp_Mode)
    {
        //时钟
        case 0:
            Seg_Buf[0] = Real_Time[0] / 16;     //小时十位
            Seg_Buf[1] = Real_Time[0] % 16;     //小时个位
            Seg_Buf[2] = 11;                    //杠
            Seg_Buf[3] = Real_Time[1] / 16;     //分钟十位
            Seg_Buf[4] = Real_Time[1] % 16;     //分钟个位
            Seg_Buf[5] = 11;                    //杠
            Seg_Point[5] = 0;                   //关闭小数点
            Seg_Buf[6] = Real_Time[2] / 16;     //秒钟十位
            Seg_Buf[7] = Real_Time[2] % 16;     //秒钟个位
        break;
        //温度
        case 1:
            Seg_Buf[0] = 12;                                //显示C
            Seg_Buf[1] = 10;                                //灭
            Seg_Buf[2] = 10;                                //灭
            Seg_Buf[3] = 10;                                //灭
            Seg_Buf[4] = (unsigned char)T / 10 % 10;        //显示十位
            Seg_Buf[5] = (unsigned char)T % 10;             //显示个位
            Seg_Point[5] = 1;                               //显示小数点
            Seg_Buf[6] = (unsigned int)(T * 10) % 10;       //显示小数1位
            Seg_Buf[7] = (unsigned int)(T * 100) % 10;      //显示小数2位
        break;
        //NE555
        case 2:
            Seg_Buf[0] = 13;                                //显示C
            Seg_Buf[1] = 10;                                //灭
            Seg_Buf[2] = 10;                                //灭
            Seg_Buf[3] = NE555 / 10000 % 10;                //万
            Seg_Buf[4] = NE555 / 1000 % 10;                 //千
            Seg_Buf[5] = NE555 / 100 % 10;                  //百
            Seg_Point[5] = 0;                               //关闭小数点
            Seg_Buf[6] = NE555 / 10 % 10;                   //十
            Seg_Buf[7] = NE555 % 10;                        //个
            for (i = 3; i < 7; i++)                         //高位为0则熄灭
            {
                if (Seg_Buf[i] == 0)
                    Seg_Buf[i] = 10;
                else
                    break;
            }
        break;
        //AD-DA
        case 3:
            Seg_Buf[0] = 14;                                //显示A
            Seg_Buf[1] = 10;                                //灭
            Seg_Buf[2] = 10;                                //灭
            Seg_Buf[3] = 10;                                //灭
            Seg_Buf[4] = 10;                                //灭
            Seg_Buf[5] = (unsigned char)Voltage % 10;       //显示个位
            Seg_Point[5] = 1;                               //显示小数点
            Seg_Buf[6] = (unsigned char)(Voltage * 10) % 10;//显示小数1位
            Seg_Buf[7] = (unsigned int)(Voltage * 100) % 10;//显示小数2位
        break;
    }
}

/* 其他处理函数 */
void LED_Deal()
{
    DA_Output(Voltage * 51);
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//0毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TMOD |= 0x05;
    TL0 = 0x00;				//设置定时初始值
    TH0 = 0x00;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
}
void Timer1_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0xBF;			//定时器时钟12T模式
    TMOD &= 0x0F;			//设置定时器模式
    TL1 = 0x18;				//设置定时初始值
    TH1 = 0xFC;				//设置定时初始值
    TF1 = 0;				//清除TF1标志
    TR1 = 1;				//定时器1开始计时
    ET1 = 1;
    EA  = 1;
}

/* 中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down_zy == 500) Slow_Down_zy = Seg_Slow_Flag = 0;
    if (Slow_Down_zy % 10 == 0) Key_Slow_Flag = 0;
    
    Seg_Disp(Slow_Down_zy%8, Seg_Buf[Slow_Down_zy%8], Seg_Point[Slow_Down_zy%8]);
    LED_xs(Slow_Down_zy%8, LED_Buf[Slow_Down_zy%8]);
    
    
    if (++Timer1000ms == 1000)
    {
        Timer1000ms = 0;
        NE555 = (TH0 << 8) | TL0;
        TH0 = 0;
        TL0 = 0;
    }
}

/* 延时函数 */
void Delay_ms(unsigned int xms)		//@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 主函数 */
void main()
{
    DS18B20_Read();
    Delay_ms(750);
    System_Init();
    DS1302_Write_Date(Real_Time);
    Timer0_Init();
    Timer1_Init();
    
    while (1)
    {
        Key_Deal();
        Seg_Deal();
        LED_Deal();
    }
}




