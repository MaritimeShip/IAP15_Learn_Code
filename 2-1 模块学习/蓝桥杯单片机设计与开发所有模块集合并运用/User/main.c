/**-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  * @name:  蓝桥杯单片机设计与开发所有模块集合并运用
  *-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  * @brief: 数码管：  0-实时时钟  1-实时温度  2-实时电压(电压来自于RB2,同时DA也输出这个值)  3-实时频率  4-实时距离
  *
  *         按  键：  S4-模式切换   S5-时钟保存到EEPROM(有PIN验证)，并下次上电读取(在实时时钟界面生效)   S6-初始化时钟(在实时时钟界面生效)   S7-通过串口发送当前界面的数据
  *
  *         L E D ：  L1-L5对应数码管的模式，L6-L8由串口控制
  *
  *         蜂鸣器：  当时钟保存成功，蜂鸣器会鸣叫75ms
  *
  *         继电器：  如果距离大于50，则继电器闭合，反之断开
  *
  *         串  口：  可控制L6-L8(有错误反馈)，接收格式为： LED6 = 1;     （注意LED控制后面有个“;”,显示模式控制没有）
                      可控制数码管显示模式模式(有错误反馈)，接收格式为： Seg_Show_Mode = 1
  *-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  * @Author: 海上的船
  *-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  * @Date:   2024.5.27
  *-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  */
  
  
/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include <stdio.h>
#include <string.h>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "ds1302.h"
#include "onewire.h"
#include "iic.h"
#include "Uwave.h"
#include "Uart.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用变量
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;       //按键数码管LED信息处理减速专用
idata unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};      //数码管显示缓冲区
idata unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};            //数码管小数点显示缓冲区
idata unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};              //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned char Seg_Show_Mode;                                    //数码管显示模式  0-实时时钟  1-实时温度  2-实时电压  3-实时频率  4-实时距离
unsigned char Rtc_Real_Buf[3] = {23, 59, 56};                   //实时时钟储存数据
float T, AD_DA_Data;                                            //实时温度, 实时电压
unsigned char Time_75ms;                                        //75ms计时
bit Beep_Flag;                                                  //蜂鸣器标志位  0-不叫  1-叫
unsigned int Freq, Time_1000ms;                                 //实时频率，1000ms计时
unsigned char Dist;                                             //实时距离
unsigned char Uart_Rx_Index;                                    //串口接收指针
idata unsigned char Uart_Rx_Buf[20];                            //串口接收缓冲区
unsigned char Uart_Ticks;                                       //串口计时
bit Uart_Rx_Flag;                                               //串口接收数据标志位

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;      //按键获取
    static unsigned char Key_UP = 0, Key_Down = 0;      //专用变量
    
    if (Key_Slow_Flag) return;                  //减速
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();                       //捕获键码
    Key_UP = ~Key_New & (Key_New ^ Key_Old);    //捕获上升沿
    Key_Down = Key_New & (Key_New ^ Key_Old);   //捕获下降沿
    Key_Old = Key_New;                          //辅助扫描
    
    //按键处理
    switch (Key_Down)
    {
        //模式切换
        case 4:
            if (++Seg_Show_Mode == 5) Seg_Show_Mode = 0;
        break;
        //时钟保存
        case 5:
            if (Seg_Show_Mode == 0)     //时钟显示模式
            {
                EEPROM_Write(Rtc_Real_Buf, 0, 3);   //数据写入
                Beep_Flag = 1;                      //开蜂鸣器
                Time_75ms = 0;                      //计时清0
            }
        break;
        //时钟初始化
        case 6:
            if (Seg_Show_Mode == 0)     //时钟显示模式
            {
                Rtc_Real_Buf[0] = 23;           //时重置
                Rtc_Real_Buf[1] = 59;           //分重置
                Rtc_Real_Buf[2] = 56;           //秒重置
                DS1302_Write_Rtc(Rtc_Real_Buf); //数据写入
            }
        break;
        //串口打印数据
        case 7:
            if (Uart_Rx_Flag == 0)            //在发送数据时有概率莫名发送出去按键里面的内容，用这个可以降低这种可能性的发生
            {
                switch (Seg_Show_Mode)
                {
                    case 0: printf("时间：%2d-%2d-%2d\r\n", (unsigned int)Rtc_Real_Buf[0], (unsigned int)Rtc_Real_Buf[1], (unsigned int)Rtc_Real_Buf[2]); break;
                    case 1: printf("温度：%.1f度\r\n", T); break;
                    case 2: printf("电压：%.2f伏\r\n", AD_DA_Data); break;
                    case 3: printf("频率：%u赫兹\r\n", Freq); break;
                    case 4: printf("距离：%d厘米\r\n", (unsigned int)Dist); break;
                }
            }
        break;
    }
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;      //减速
    Seg_Slow_Flag = 1;
    
    switch (Seg_Show_Mode)
    {
        //实时时钟显示
        case 0:
            Seg_Point[5] = 0;                       //小数点
            Seg_Point[6] = 0;                       //小数点
            Seg_Buf[0] = Rtc_Real_Buf[0] / 10;      //时十位
            Seg_Buf[1] = Rtc_Real_Buf[0] % 10;      //时个位
            Seg_Buf[2] = 11;                        //显示-
            Seg_Buf[3] = Rtc_Real_Buf[1] / 10;      //分十位
            Seg_Buf[4] = Rtc_Real_Buf[1] % 10;      //分个位
            Seg_Buf[5] = 11;                        //显示-
            Seg_Buf[6] = Rtc_Real_Buf[2] / 10;      //秒十位
            Seg_Buf[7] = Rtc_Real_Buf[2] % 10;      //秒个位
        break;
        //实时温度显示
        case 1:
            Seg_Point[5] = 0;                       //小数点
            Seg_Point[6] = 1;                       //小数点
            Seg_Buf[0] = 14;                        //显示C
            for (i = 1; i < 5; i++)                 //熄灭
                Seg_Buf[i] = 10;
            Seg_Buf[5] = (unsigned char)T / 10 % 10;//温度十位
            Seg_Buf[6] = (unsigned char)T % 10;     //温度个位
            Seg_Buf[7] = (unsigned int)(T*10) % 10; //温度小数1位
        break;
        //实时电压显示
        case 2:
            Seg_Point[6] = 0;                                   //小数点
            Seg_Point[5] = 1;                                   //小数点
            for (i = 1; i < 5; i++)                             //熄灭
                Seg_Buf[i] = 10;
            Seg_Buf[0] = 18;                                    //显示U
            Seg_Buf[5] = (unsigned char)AD_DA_Data % 10;        //电压个位
            Seg_Buf[6] = (unsigned int)(AD_DA_Data*10) % 10;    //电压小数1位
            Seg_Buf[7] = (unsigned int)(AD_DA_Data*100) % 10;   //电压小数2位
        break;
        //实时频率显示
        case 3:
            Seg_Point[5] = 0;                       //小数点
            Seg_Point[6] = 0;                       //小数点
            Seg_Buf[0] = 17;                        //显示F
            Seg_Buf[1] = Seg_Buf[2] = 10;           //熄灭
            Seg_Buf[3] = Freq / 10000 % 10;         //频率万位
            Seg_Buf[4] = Freq / 1000 % 10;          //频率千位
            Seg_Buf[5] = Freq / 100 % 10;           //频率百位
            Seg_Buf[6] = Freq / 10 % 10;            //频率十位
            Seg_Buf[7] = Freq % 10;                 //频率个位
            for (i = 3; i < 7; i++)     //高位熄灭
            {
                if (Seg_Buf[i] == 0)    //高位为0
                    Seg_Buf[i] = 10;    //熄灭
                else                    //高位不为0
                    break;              //跳出循环
            }
        break;
        //实时距离显示
        case 4:
            Seg_Point[5] = 0;                       //小数点
            Seg_Point[6] = 0;                       //小数点
            Seg_Buf[0] = 15;                        //显示d
            for (i = 1; i < 5; i++)                 //熄灭
                Seg_Buf[i] = 10;
            Seg_Buf[5] = Dist / 100 % 10;           //距离百位
            Seg_Buf[6] = Dist / 10 % 10;            //距离十位
            Seg_Buf[7] = Dist % 10;                 //距离个位
        break;
    }
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;
    
    //L1~L5互斥点亮
    for (i = 0; i < 5; i++)
        LED_Buf[i] = (i == Seg_Show_Mode);
}

/* 信息处理函数 */
void Infor_Deal()
{
    /* 信息输出区 */
    DA_Write(AD_DA_Data * 51);          //DA输出电压
    Beep(Beep_Flag);                    //蜂鸣器
    Relay((Dist > 50));
    
    /* 减速 */
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 信息读取区 */
    DS1302_Read_Rtc(Rtc_Real_Buf);      //实时时钟读取
    T = DS18B20_Read_T();               //实时温度读取
    AD_DA_Data = AD_Read(0x43) / 51.0;  //实时电压读取
    Dist = Uwave_Read_Dist();       //实时距离读取
    
    /* 信息处理区 */
    //因为只是调用一下，所以并没有涉及到信息处理的内容
}

/* 串口处理函数 */
void Uart_Deal()
{
    if (!Uart_Rx_Index) return;
    
    if (Uart_Ticks > 10)
    {
        Uart_Ticks = Uart_Rx_Flag = 0;
        /* 逻辑处理区 */
        switch (Uart_Rx_Index)
        {
            //LED控制L6-L8
            case 9:
                if (Uart_Rx_Buf[0] == 'L' && Uart_Rx_Buf[1] == 'E' && Uart_Rx_Buf[2] == 'D' && Uart_Rx_Buf[4] == ' ' && Uart_Rx_Buf[5] == '=' && Uart_Rx_Buf[6] == ' ' && Uart_Rx_Buf[8] == ';')
                {
                    if ((Uart_Rx_Buf[3] >= '6' && Uart_Rx_Buf[3] <= '8') && (Uart_Rx_Buf[7] == '1' || Uart_Rx_Buf[7] == '0'))
                    {
                        LED_Buf[Uart_Rx_Buf[3]-'0' - 1] = Uart_Rx_Buf[7] - '0';
                        printf("OK!!! %c = %c\r\n", Uart_Rx_Buf[3], Uart_Rx_Buf[7]);
                    }
                    else
                        printf("EEROR!!! %c = %c\r\n",  Uart_Rx_Buf[3], Uart_Rx_Buf[7]);
                }
                else
                    printf("EEROR!!! %s\r\n",  Uart_Rx_Buf);
            break;
            //数码管界面控制
            case 17:
                if (Uart_Rx_Buf[0] == 'S' && Uart_Rx_Buf[1] == 'e' && Uart_Rx_Buf[2] == 'g' && Uart_Rx_Buf[3] == '_' && Uart_Rx_Buf[4] == 'S' && Uart_Rx_Buf[5] == 'h' && Uart_Rx_Buf[6] == 'o' && Uart_Rx_Buf[7] == 'w' && 
                    Uart_Rx_Buf[8] == '_' && Uart_Rx_Buf[9] == 'M' && Uart_Rx_Buf[10] == 'o' && Uart_Rx_Buf[11] == 'd' && Uart_Rx_Buf[12] == 'e' && Uart_Rx_Buf[13] == ' ' && Uart_Rx_Buf[14] == '=' && Uart_Rx_Buf[15] == ' ')
                {
                    if (Uart_Rx_Buf[16] >= '0' && Uart_Rx_Buf[16] <= '4')
                    {
                        Seg_Show_Mode = Uart_Rx_Buf[16] - 48;
                        printf("OK\r\n");
                    }
                    else
                        printf("ERROR\r\n");
                }
                else
                    printf("ERROR\r\n");
            break;
            //错误数据
            default:
                printf("EEROR!!! Uart_Rx_Index = %u\r\n", (unsigned int)Uart_Rx_Index);
            break;
        }
        
        /* 逻辑处理结束 */
        memset(Uart_Rx_Buf, 0, Uart_Rx_Index);
        Uart_Rx_Index = 0;
    }
    
}

/* 定时器初始化函数 */
void Timer0_Init(void)      //1毫秒@12.000MHz
{
    AUXR &= 0x7F;           //定时器时钟12T模式
    TMOD &= 0xF0;           //设置定时器模式
    TMOD |= 0x05;
    TL0 = 0x00;             //设置定时初始值
    TH0 = 0x00;             //设置定时初始值
    TF0 = 0;                //清除TF0标志
    TR0 = 1;                //定时器0开始计时
}
void Timer1_Init(void)      //1毫秒@12.000MHz
{
    AUXR &= 0xBF;       //定时器时钟12T模式
    TMOD &= 0x0F;       //设置定时器模式
    TL1 = 0x18;     //设置定时初值
    TH1 = 0xFC;     //设置定时初值
    TF1 = 0;        //清除TF1标志
    TR1 = 1;        //定时器1开始计时
    EA  = 1;
    ET1 = 1;
}

/* 中断服务函数 */
//定时器1中断服务函数
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 100 == 0) Seg_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    //蜂鸣器75ms计时
    if (Beep_Flag == 1 && ++Time_75ms == 75)   
        Beep_Flag = Time_75ms = 0;
    
    //频率计算
    if (++Time_1000ms == 1000)                 
    {
        Freq = (TH0 << 8) | TL0;
        Time_1000ms = TL0 = TH0 = 0;
    }

    //串口空闲处理
    if (Uart_Rx_Flag && ++Uart_Ticks > 10)
        Uart_Ticks = 11;
}
//串口中断服务函数
void Uart_Serve() interrupt 4
{
    //数据接受
    if (RI == 1)
    {
        Uart_Ticks = RI = 0;
        Uart_Rx_Flag = 1;
        Uart_Rx_Buf[Uart_Rx_Index++] = SBUF;
    }
    //防止数组越界访问
    if (Uart_Rx_Index > 19)
    {
        memset(Uart_Rx_Buf, 0, 20);
    //    printf("EEROR! Uart_Rx_Index = %d", (unsigned int)Uart_Rx_Index);
        Uart_Rx_Index = 0;
        Uart_Ticks = Uart_Rx_Flag = 0;
    }
}

/* 主函数 */
void main()
{
    unsigned char PIN = 0;
    
    /* 上电初始化 */
    System_Init();
    Timer1_Init();
    Timer0_Init();
    Uart_Init();
    DS18B20_Read_T();
    
    /* PIN验证 */
    EEPROM_Read(&PIN, 8, 1);             //读取密码
    if (PIN == 52)                       //若密码正确
        EEPROM_Read(Rtc_Real_Buf, 0, 3); //时钟读取
    else                                 //若密码错误
    {
        PIN = 52;                        //写入密码
        EEPROM_Write(&PIN, 8, 1);
    }
    DS1302_Write_Rtc(Rtc_Real_Buf);      //时钟写入
    
    while (1)
    {
        Key_Deal();
        Seg_Deal();
        LED_Deal();
        Uart_Deal();
        Infor_Deal();
    }
}
