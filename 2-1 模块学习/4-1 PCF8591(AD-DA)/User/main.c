/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Init.h"
#include "iic.h"

/* 变量创建区 */
//模版变量创建区
unsigned int Slow_Down;                //按键数码管计数专用
bit Key_Slow_Flag, Seg_Slow_Flag;       //按键数码管减速标志位
unsigned char Key_UP, Key_Down;         //按键上升下降沿捕获专用
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};    //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};          //数码管小数点缓冲区
unsigned char usLED[8] = {0, 0, 0, 0, 0, 0, 0, 0};              //LED显示缓冲区
//题目变量创建区
unsigned int dat1, dat2;      //AD-DA专用

/* 按键处理函数 */
void Key_Proc()
{
    static unsigned char Key_Val = 0, Key_Old = 0;  //按键扫描专用
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;              //按键减速
    
    Key_Val = Key_Read();                       //获取键码值
    Key_UP = ~Key_Val & (Key_Val ^ Key_Old);    //捕获上升沿
    Key_Old = Key_Val;                          //辅助扫描
    
    
}

/* 数码管处理函数 */
void Seg_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;              //数码管减速
    
    
    DA_WriteData(1 * 51);
    
    dat2 = AD_ReadData(0x41);       //AD_DA特性
    dat1 = AD_ReadData(0x43);       //实际调反

    Seg_Buf[0] = dat1 / 100 % 10;    //取百位
    Seg_Buf[1] = dat1 / 10 % 10;     //取十位
    Seg_Buf[2] = dat1 % 10;          //取个位
    
    Seg_Buf[4] = dat2 / 100 % 10;    //取百位
    Seg_Buf[5] = dat2 / 10 % 10;     //取十位
    Seg_Buf[6] = dat2 % 10;          //取个位
}

/* LED处理函数 */
void LED_Proc()
{
    
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;			//定时器时钟12T模式
    TMOD &= 0xF0;			//设置定时器模式
    TL0 = 0x18;				//设置定时初始值
    TH0 = 0xFC;				//设置定时初始值
    TF0 = 0;				//清除TF0标志
    TR0 = 1;				//定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 定时器中断服务函数 */
void Timer0_Sever() interrupt 1
{
    if (++Slow_Down == 400) Slow_Down = Seg_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    Seg_Disp(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Disp(Slow_Down%8, usLED[Slow_Down%8]);
}

/* 主函数 */
void main()
{
    Timer0_Init();
    System_Init();
    
    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}


