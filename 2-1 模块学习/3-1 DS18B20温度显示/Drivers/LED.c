#include <STC15F2K60S2.H>
#include "LED.h"





void LED_Disp(unsigned char addr, bit enable)
{
    static unsigned char Temp = 0x00;
    static unsigned char Temp_Old = 0xff;
    
    if (enable)
        Temp |= 0x01 << addr;
    else
        Temp &= ~(0x01 << addr);
    
    if (Temp != Temp_Old)
    {
        P0 = ~Temp;
        P2 = P2 & 0x1f | 0x80;
        P2 &= 0x1f;
    }
}







