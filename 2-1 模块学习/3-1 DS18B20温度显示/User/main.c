/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Init.h"
#include "onewire.h"

/* 变量创建区 */
unsigned int Slow_Down; //按键数码管减速专用
bit Key_Slow_Flag = 0, Seg_Slow_Flag = 0; //按键数码管减速标志位
unsigned char Key_Val, Key_Old, Key_Down, Key_UP; //按键获取上升下降沿
unsigned char Sign_Poc;//扫描显示数组专用
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};//数码管数字缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};//数码管小数点缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};//LED显示缓冲区
float T;//温度


/* 按键处理函数 */
void Key_Proc()
{
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_Val = Key_Read();//获取键码
    Key_Down = Key_Val & (Key_Val ^ Key_Old);
    Key_UP = ~Key_Val & (Key_Val ^ Key_Old);//获取上升下降沿
    Key_Old = Key_Val;//辅助扫描

    
}

/* 信息处理函数 */
void Seg_Proc()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;  //数码管减速
    
    T = DS18B20_ReadTem();  //读取温度
    
    Seg_Buf[0] = (unsigned char)T / 10 % 10;    //显示十位
    Seg_Buf[1] = (unsigned char)T % 10;         //显示个位
    Seg_Point[1] = 1;                           //显示小数点
    Seg_Buf[2] = (unsigned int)(T * 10) % 10;   //小数位
}

/* LED处理函数 */
void LED_Proc()
{
    
}

/* 中断初始化函数 */
void Timer0Init(void)       //1毫秒@12.000MHz
{
    AUXR &= 0x7F;           //定时器时钟12T模式
    TMOD &= 0xF0;           //设置定时器模式
    TL0 = 0x18;             //设置定时初始值
    TH0 = 0xFC;             //设置定时初始值
    TF0 = 0;                //清除TF0标志
    TR0 = 1;                //定时器0开始计时
    ET0 = 1;
    EA  = 1;
}

/* 中断服务函数 */
void Timer0_Serve() interrupt 1
{
    if (++Slow_Down >= 500) {Slow_Down = 0;Seg_Slow_Flag = 0;}
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (++Sign_Poc == 8) Sign_Poc = 0;//显示数组专用
    Seg_Disp(Sign_Poc, Seg_Buf[Sign_Poc], Seg_Point[Sign_Poc]);
    LED_Disp(Sign_Poc, LED_Buf[Sign_Poc]);
    
}

/* 延时函数 */
void Delay_ms(unsigned int xms)     //@12.000MHz
{
    unsigned char i, j;
    while (xms)
    {
        i = 12;
        j = 169;
        do
        {
            while (--j);
        } while (--i);
        xms--;
    }
}

/* 主函数 */
void main()
{
    Timer0Init();
    System_Init();
    DS18B20_ReadTem();//读取温度
    Delay_ms(750);
    while (1)
    {
        Key_Proc();
        Seg_Proc();
        LED_Proc();
    }
}







