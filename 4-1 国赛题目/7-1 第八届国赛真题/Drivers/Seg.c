#include <STC15F2K60S2.H>
#include "Seg.h"

code unsigned char Seg_Table[] =
{
    0xc0, //0
    0xf9, //1
    0xa4, //2
    0xb0, //3
    0x99, //4
    0x92, //5
    0x82, //6
    0xf8, //7
    0x80, //8
    0x90, //9
    0xff, //Ϩ��
    0xbf, //-
    0x8e, //F
    0x88, //A
    0x83, //b
    0xc6, //C
    0xa1, //d
    0x86  //E
};

void Seg_Show(unsigned char addr, Table, Point)
{
    P0 = 0xff;
    P2 = P2 & 0x1f | 0xe0;
    P2 &= 0x1f;
    
    P0 = 0x01 << addr;
    P2 = P2 & 0x1f | 0xc0;
    P2 &= 0x1f;
    
    P0 = Seg_Table[Table];
    if (Point)
        P0 &= 0x7f;
    P2 = P2 & 0x1f | 0xe0;
    P2 &= 0x1f;
}
