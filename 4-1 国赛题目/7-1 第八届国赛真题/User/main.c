/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Seg.h"
#include "Key.h"
#include "LED.h"
#include "Uwave.h"
#include "iic.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;       //按键数码管LED信息处理减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned char Seg_Show_Mode;                                        //数码管显示界面   0-测距显示界面   1-数据回显界面   2-参数设置界面
bit Dist_Operate_Mode;                                              //测距操作模式   0-无操作   1-加操作
unsigned char Dist, Dist_Old, Dist_Index;                           //实时距离, 上次距离数据，距离存储指针
unsigned char Dist_Return_Data, Dist_Return_Index;                  //距离回显数据, 距离回显指针
unsigned char Dead_Zone_Show = 20, Dead_Zone_Set = 20;              //距离盲区数据
float DA_Output_Data;                                               //DA输出数据
unsigned char L1_Count, L1_Time_200ms;                              //L1亮灭计时控制（亮灭都记，应为20次），L1_200ms计时控制
bit L1_Flag, L1_Star_Flag;                                          //L1启用标志位，L1闪烁标志位

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;
    static unsigned char Key_UP = 0, Key_Down = 0;
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();
    Key_UP = ~Key_New & (Key_New ^ Key_Old);
    Key_Down = Key_New & (Key_New ^ Key_Old);
    Key_Old = Key_New;
    
    switch (Key_Down)
    {
        //启动测量
        case 4:
            Dist_Old = Dist;                                        //获取上次距离数据
            Dist = Uwave_Read_D();                                  //实时距离
            EEPROM_Write(&Dist, Dist_Index, 1);                     //写入数据
            if (++Dist_Index == 10) Dist_Index = 0;                 //控制循环写入  地址为0-9
            L1_Flag = 1;                                            //L1启用
        break;
        //数据回显
        case 5:
            if (Seg_Show_Mode != 1) Seg_Show_Mode = 1;                  //进入数据回显界面
            else Seg_Show_Mode = 0;                                     //进入测距显示界面
            if (Seg_Show_Mode == 1)
            {
                Dist_Return_Index = 0;                                      //回显编号清零
                EEPROM_Read(&Dist_Return_Data, Dist_Return_Index , 1);      //读出数据
            }
        break;
        //参数设置
        case 6:
            if (Seg_Show_Mode != 2) Seg_Show_Mode = 2;                  //进入参数设置界面
            else Seg_Show_Mode = 0;                                     //进入测距显示界面
        break;
        //辅助按键
        case 7:
            //测距显示界面
            if (Seg_Show_Mode == 0) Dist_Operate_Mode ^= 1;                 //操作模式切换
            //数据回显界面
            else if (Seg_Show_Mode == 1)
            {
                if (++Dist_Return_Index == 10)                              //翻页
                    Dist_Return_Index = 0;                                  //控制页
                EEPROM_Read(&Dist_Return_Data, Dist_Return_Index , 1);      //读出数据
            }
            //参数设置界面
            else
            {
                Dead_Zone_Show += 10;                               //盲区加
                if (Dead_Zone_Show > 90)                            //控制边界
                    Dead_Zone_Show = 0;                             //归零
            }
        break;
    }
    
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    switch (Seg_Show_Mode)
    {
        //测距显示界面
        case 0:
            Seg_Buf[1] = 10;                            //熄灭
            Seg_Buf[0] = Dist_Operate_Mode;             //显示操作
            
            Seg_Buf[2] = (Dist_Operate_Mode ? Dist_Old+Dist : Dist_Old) / 100 % 10;//旧距离百位
            Seg_Buf[3] = (Dist_Operate_Mode ? Dist_Old+Dist : Dist_Old) / 10 % 10; //旧距离十位
            Seg_Buf[4] = (Dist_Operate_Mode ? Dist_Old+Dist : Dist_Old) % 10;      //旧距离个位
            if (Seg_Buf[2] == 0) Seg_Buf[2] = 10;       //熄灭
        
            Seg_Buf[5] = Dist / 100 % 10;               //距离百位
            Seg_Buf[6] = Dist / 10 % 10;                //距离十位
            Seg_Buf[7] = Dist % 10;                     //距离个位
        break;
        //数据回显界面
        case 1:
            for (i = 2; i < 5; i++)
                Seg_Buf[i] = 10;                               //熄灭
            Seg_Buf[0] = (Dist_Return_Index + 1) / 10;         //显示数据编号
            Seg_Buf[1] = (Dist_Return_Index + 1) % 10;         //显示数据编号
            Seg_Buf[5] = Dist_Return_Data / 100 % 10;          //距离百位
            Seg_Buf[6] = Dist_Return_Data / 10 % 10;           //距离十位
            Seg_Buf[7] = Dist_Return_Data % 10;                //距离个位
        break;
        //参数设置界面
        case 2:
            for (i = 1; i < 6; i++)
                Seg_Buf[i] = 10;                        //熄灭
            Seg_Buf[0] = 12;                            //显示F
            Seg_Buf[6] = Dead_Zone_Show / 10;           //盲区十位
            Seg_Buf[7] = Dead_Zone_Show % 10;           //盲区个位
        break;
    }
    
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;
    
    //L1        启动测量
    LED_Buf[0] = L1_Star_Flag;
    //L7        参数设置
    LED_Buf[6] = (Seg_Show_Mode == 2);
    //L8        数据回显
    LED_Buf[7] = (Seg_Show_Mode == 1);
}

/* 信息处理函数 */
void Infor_Deal()
{
    /* 信息输出区 */
    DA_Write(DA_Output_Data * 51);
    
    /* 减速 */
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 信息处理区 */
    //数据保存
    if (Seg_Show_Mode != 2)
        Dead_Zone_Set = Dead_Zone_Show;
    
    //DA输出处理
    if (Dead_Zone_Set >= Dist)
        DA_Output_Data = 0;
    else
        DA_Output_Data = (Dist - Dead_Zone_Set) * 0.02;
    //DA输出异常处理
    if (DA_Output_Data > 5) 
        DA_Output_Data = 5;
    
}

/* 定时器初始化函数 */
void Timer1_Init(void)      //1毫秒@12.000MHz
{
    AUXR &= 0xBF;       //定时器时钟12T模式
    TMOD &= 0x0F;       //设置定时器模式
    TL1 = 0x18;         //设置定时初值
    TH1 = 0xFC;         //设置定时初值
    TF1 = 0;            //清除TF1标志
    TR1 = 1;            //定时器1开始计时
    EA  = 1;
    ET1 = 1;
}

/* 中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 100 == 0) Seg_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    if (L1_Flag && ++L1_Time_200ms == 200)
    {
        L1_Time_200ms = 0;
        L1_Star_Flag ^= 1;
        if (++L1_Count == 20)
            L1_Count = L1_Star_Flag = L1_Flag = 0;
    }
}

/* 主函数 */
void main()
{
    System_Init();
    Timer1_Init();
    
    while (1)
    {
        Key_Deal();
        Seg_Deal();
        LED_Deal();
        Infor_Deal();
    }
}
