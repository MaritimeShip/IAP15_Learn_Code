/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Uwave.h"
#include "ds1302.h"
#include "iic.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;       //按键数码管LED信息处理减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned char Dist;                                     //实时距离
unsigned char Rtc_Real_Buf[3] = {20, 20, 1};            //实时时钟
float DA_Data;                                          //DA输出数据
unsigned char AD_Data_New, AD_Data_Old;                 //AD数据
bit Seg_Show_Mode;                                      //数码管显示界面  0-数据显示界面  1-参数设置界面
unsigned char Data_Show_Mode;                           //数据显示界面   0-时间数据显示界面   1-距离数据显示界面   2-数据记录显示界面
unsigned char Data_Record_Index;                        //数据记录指针   0-最大   1-平均   2-最小
unsigned char Dist_Max, Dist_Min;                       //数据记录最大值，数据记录最小值
float Dist_Average;                                     //数据记录平均值
bit Num_Set_Mode;                                       //参数设置模式   0-采集时间设置界面   1-距离参数设置界面
bit Dist_Refresh_Mode;                                  //距离刷新模式   0-触发模式   1-定时模式
unsigned char Get_Time_Set = 2, Get_Time_Show = 2;      //采集时间   2  3  5  7  9
unsigned char Dist_Num_Set = 20, Dist_Num_Show = 20;    //距离参数
unsigned char Sec;                                      //秒数保存
bit Get_Dist_Flag;                                      //数据采集标志位
unsigned int Get_Time;                                  //获取时间计时
unsigned int Dist_Count, Dist_Sum;                      //采集计次, 距离和
unsigned char Dist_Old;                                 //距离参数
unsigned char L5_Dist_New, L5_Dist_Old;                 //L5控制

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;      //按键获取
    static unsigned char Key_UP = 0, Key_Down = 0;      //专用变量
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                          //减速
    
    Key_New = Key_Read();                       //获取键码
    Key_UP = ~Key_New & (Key_New ^ Key_Old);    //捕获上升沿
    Key_Down = Key_New & (Key_New ^ Key_Old);   //捕获下降沿
    Key_Old = Key_New;                          //辅助扫描
    
    switch (Key_Down)
    {
        //数码管界面切换
        case 4:
            Seg_Show_Mode ^= 1;
        break;
        //数据或者参数切换
        case 5:
            if (Seg_Show_Mode == 0)  //数据显示界面
            {
                if (++Data_Show_Mode == 3) 
                    Data_Show_Mode = 0;
            }
            else                     //参数设置界面
                Num_Set_Mode ^= 1;
        break;
        //最大值、 最小值、 平均值
        case 8:
            if (Seg_Show_Mode == 0)  //数据显示界面
            {
                if (++Data_Record_Index == 3) Data_Record_Index = 0;
                if (Data_Show_Mode == 1) Dist_Refresh_Mode ^= 1;
            }
        break;
        //参数调整
        case 9:
            if (Seg_Show_Mode == 1 && Num_Set_Mode == 0)        //采集时间设置界面
            {
                if (Get_Time_Show == 2) Get_Time_Show++;
                else Get_Time_Show += 2;
                if (Get_Time_Show > 9) Get_Time_Show = 2;
            }
            else if (Seg_Show_Mode == 1 && Num_Set_Mode == 1)   //距离参数设置界面
            {
                Dist_Num_Show += 10;
                if (Dist_Num_Show > 80)
                    Dist_Num_Show = 10;
            }
        break;
    }
    
    
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    //小数点处理
    Seg_Point[6] = (Seg_Show_Mode == 0 && Data_Record_Index == 1 && Data_Show_Mode == 2);
    
    //数据显示界面
    if (Seg_Show_Mode == 0)     
    {
        switch (Data_Show_Mode)
        {
            //时间数据显示界面
            case 0:
                Seg_Buf[0] = Rtc_Real_Buf[0] / 10;      //时十位
                Seg_Buf[1] = Rtc_Real_Buf[0] % 10;      //时个位
                Seg_Buf[2] = 11;                        // -
                Seg_Buf[3] = Rtc_Real_Buf[1] / 10;      //分十位
                Seg_Buf[4] = Rtc_Real_Buf[1] % 10;      //分个位
                Seg_Buf[5] = 11;                        // -
                Seg_Buf[6] = Rtc_Real_Buf[2] / 10;      //秒十位
                Seg_Buf[7] = Rtc_Real_Buf[2] % 10;      //秒个位
            break;
            //距离数据显示界面
            case 1:
                for (i = 2; i < 5; i++) Seg_Buf[i] = 10;        //熄灭
                Seg_Buf[0] = 12;                                //L
                Seg_Buf[1] = Dist_Refresh_Mode ? 14 : 13;       //显示C或者F
                Seg_Buf[5] = Dist / 100 % 10;                   //距离百位
                Seg_Buf[6] = Dist / 10 % 10;                    //距离十位
                Seg_Buf[7] = Dist % 10;                         //距离个位
                for (i = 5; i < 7; i++)                         //高位熄灭
                {
                    if (Seg_Buf[i] == 0)
                        Seg_Buf[i] = 10;
                    else
                        break;
                }
            break;
            //数据记录显示界面
            case 2:
                
                Seg_Buf[0] = 15;                                    //H
                switch (Data_Record_Index)
                {
                    //最大
                    case 0:
                        Seg_Buf[1] = 16;                     //上-
                        Seg_Buf[5] = Dist_Max / 100 % 10;    //距离百位
                        Seg_Buf[6] = Dist_Max / 10 % 10;     //距离十位
                        Seg_Buf[7] = Dist_Max % 10;          //距离个位
                    break;
                    //平均
                    case 1:
                        Seg_Buf[1] = 11;   //-
                        Seg_Buf[4] = (unsigned char)Dist_Average / 100 % 10;    //距离百位
                        Seg_Buf[5] = (unsigned char)Dist_Average / 10 % 10;     //距离十位
                        Seg_Buf[6] = (unsigned char)Dist_Average % 10;          //距离个位
                        Seg_Buf[7] = (unsigned int)(Dist_Average*10) % 10;      //距离小数1位
                        for (i = 4; i < 6; i++)                                 //高位熄灭
                        {
                            if (Seg_Buf[i] == 0)
                                Seg_Buf[i] = 10;
                            else
                                break;
                        }
                    break;
                    //最小
                    case 2:
                        Seg_Buf[1] = 17;                            //_
                        Seg_Buf[5] = Dist_Min / 100 % 10;    //距离百位
                        Seg_Buf[6] = Dist_Min / 10 % 10;     //距离十位
                        Seg_Buf[7] = Dist_Min % 10;          //距离个位
                    break;
                }
                for (i = 5; Data_Record_Index != 1 && i < 7; i++)   //高位熄灭
                {
                    if (Seg_Buf[i] == 0)
                        Seg_Buf[i] = 10;
                    else
                        break;
                }
            break;
        }
    }
    //参数设置界面
    else
    {
        for (i = 2; i < 6; i++) Seg_Buf[i] = 10;//熄灭
        Seg_Buf[0] = 18;                        //H
        Seg_Buf[1] = Num_Set_Mode;              //显示
        Seg_Buf[1]++;                           //编号
        //采集时间设置界面
        if (Num_Set_Mode == 0)
        {
            Seg_Buf[6] = 0;                     //显示 0
            Seg_Buf[7] = Get_Time_Show;         //采集时间
        }
        //距离参数设置界面
        else
        {
            Seg_Buf[6] = Dist_Num_Show / 10;    //距离参数十位
            Seg_Buf[7] = Dist_Num_Show % 10;    //距离参数个位
        }
    }
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;
    
    //L123
    for (i = 0; i < 3 ; i++)
        LED_Buf[i] = (i == Data_Show_Mode && Seg_Show_Mode == 0);
    //L4
    LED_Buf[3] = !Dist_Refresh_Mode;
    //L5
    LED_Buf[4] = ((L5_Dist_New < Dist + 5 || L5_Dist_New > Dist - 5) && (L5_Dist_Old < Dist + 5 || L5_Dist_Old > Dist - 5));
    //L6
    LED_Buf[5] = (AD_Data_New > 20);
}

/* 信息处理函数 */
void Infor_Deal()
{
    /* 信号输出区 */
    DA_Write(DA_Data * 51);
    if (Get_Dist_Flag == 1)
    {
        Dist = Uwave_Read_D();
        if (Dist_Old != Dist)
        {
            Dist_Count++;
            Dist_Sum += Dist;
            if (Dist_Max < Dist)
                Dist_Max = Dist;
            if (Dist_Min > Dist)
                Dist_Min = Dist;
            Dist_Old = Dist;
        }
    }
    
    /* 减速 */
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 信息读取区 */
    AD_Data_New = AD_Read(0x41);        //光敏读取
    DS1302_Read_Rtc(Rtc_Real_Buf);      //时钟读取

    /* 信息处理区 */
    //DA输出处理
    if (Dist <= 10)             //距离小于10
        DA_Data = 1;
    else if (Dist <= 80)        //距离大于80
        DA_Data = 5;
    else                        //距离为10-80
        DA_Data = 4.0 / 70 * (Dist - 10) + 1;
    
    //数据保存
    if (Seg_Show_Mode != 1 || Num_Set_Mode != 0)    //采集时间保存
        Get_Time_Set = Get_Time_Show;
    if (Seg_Show_Mode != 1 || Num_Set_Mode != 1)    //距离参数保存
        Dist_Num_Set = Dist_Num_Show;
    
    //界面切换回归
    if (Seg_Show_Mode == 0) Num_Set_Mode = 0;       //回归采集时间设置
    else Data_Show_Mode = 0;                        //回归时间数据显示
    if (Data_Show_Mode != 2) Data_Record_Index = 0; //回归测距最大值
    
    
    //采集处理
    if ((Dist_Refresh_Mode == 1 && Rtc_Real_Buf[2] % Get_Time_Set == 0) || (Dist_Refresh_Mode == 0 && AD_Data_New < 20 && AD_Data_Old > 20))
    {
        Get_Dist_Flag = Dist_Count = 1;
        Dist = Uwave_Read_D();
        Dist_Sum =Dist_Old = Dist_Min = Dist_Max = Dist;
    }
    
    //AD数据更新
    AD_Data_Old = AD_Data_New;
    
}

/* 定时器初始化函数 */
void Timer1_Init(void)      //1毫秒@12.000MHz
{
    AUXR &= 0xBF;       //定时器时钟12T模式
    TMOD &= 0x0F;       //设置定时器模式
    TL1 = 0x18;         //设置定时初值
    TH1 = 0xFC;         //设置定时初值
    TF1 = 0;            //清除TF1标志
    TR1 = 1;            //定时器1开始计时
    EA  = 1;
    ET1 = 1;
}

/* 中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 200 == 0) Seg_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    if (Get_Dist_Flag == 1 && ++Get_Time == Get_Time_Set * 1000)
    {
        Get_Dist_Flag = Get_Time = 0;
        Dist_Average = (Dist_Sum/1.0) / Dist_Count;
        L5_Dist_Old = L5_Dist_New;
        L5_Dist_New = Dist;
    }
}

/* 主函数 */
void main()
{
    System_Init();
    Timer1_Init();
    DS1302_Write_Rtc(Rtc_Real_Buf);
    AD_Read(0x41);
    
    while (1)
    {
        Key_Deal();
        Seg_Deal();
        LED_Deal();
        Infor_Deal();
    }
}

