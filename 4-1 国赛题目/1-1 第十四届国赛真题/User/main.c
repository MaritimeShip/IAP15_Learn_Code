/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "iic.h"
#include "onewire.h"
#include "Uwave.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用变量
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;       //按键数码管LED信息处理函数减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned int Dist;                                      //实时距离
unsigned int Dist_Time;                                 //距离时间
float T;                                                //实时温度
float DA_Output_Data;                                   //DA输出数据
unsigned char Seg_Show_Mode;                            //数码管显示模式   0-测距界面   1-参数界面   2-工厂界面
bit Dist_Unit_Flag;                                     //距离单位标志位   0-cm   1-m
bit Num_Show_Flag;                                      //参数模式标志位   0-距离   1-温度
unsigned char Dist_Show_Temp = 40, Dist_Set_Temp = 40;  //距离参数
unsigned char T_Show_Temp = 30, T_Set_Temp = 30;        //温度参数
unsigned char Factory_Mode;                             //工厂模式   0-校准值设置模式   1-介质设置模式   2-DAC输出模式
int Adjust_Num_Show, Adjust_Num_Set;                    //校准值
unsigned int Trans_Speed_Show = 340, Trans_Speed_Set = 340;//传输速度
float DA_Min_Data_Show = 1, DA_Min_Data_Set = 1;        //DAC输出下限
bit L1_Flag, L1_Star_Flag;                              //L1闪烁控制
unsigned char Time_100ms;                               //定时器100ms计时

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;      //按键捕获
    static unsigned char Key_UP = 0, Key_Down = 0;      //专用变量
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                              //减速
    
    Key_New = Key_Read();                           //捕获按键
    Key_UP = ~Key_New & (Key_New ^ Key_Old);        //捕获上升沿
    Key_Down = Key_New & (Key_New ^ Key_Old);       //捕获下降沿
    Key_Old = Key_New;                              //辅助扫描
    
    switch (Key_Down)
    {
        //界面切换
        case 4:
            //LED退出
            if (Seg_Show_Mode == 2)
                LED_Buf[0] = L1_Flag = 0;
            //界面切换
            if (++Seg_Show_Mode == 3) 
                Seg_Show_Mode = 0;
        break;
        //界面内模式切换
        case 5:
            //测距界面 
            if (Seg_Show_Mode == 0)
                Dist_Unit_Flag ^= 1;                            //单位切换
            //参数界面 
            else if (Seg_Show_Mode == 1)
                Num_Show_Flag ^= 1;                             //模式切换
            //工厂界面 
            else if (Seg_Show_Mode == 2)
                if (++Factory_Mode == 3) Factory_Mode = 0;      //模式切换
        break;
        //数据加
        case 8:
            //参数界面 
            if (Seg_Show_Mode == 1)
            {
                //距离
                if (Num_Show_Flag == 0)
                {
                    Dist_Show_Temp += 10;
                    if (Dist_Show_Temp > 90)
                        Dist_Show_Temp = 90;
                }
                //温度
                else
                {
                    if (++T_Show_Temp > 80)
                        T_Show_Temp = 80;
                }
            }
            //工厂模式
            else if (Seg_Show_Mode == 2)
            {
                switch (Factory_Mode)
                {
                    //校准值设置模式
                    case 0:
                        Adjust_Num_Show += 5;
                        if (Adjust_Num_Show > 90)
                            Adjust_Num_Show = 90;
                    break;
                    //介质设置模式
                    case 1:
                        Trans_Speed_Show += 10;
                        if (Trans_Speed_Show > 9990)
                            Trans_Speed_Show = 9990;
                    break;
                    //DAC输出设置模式
                    case 2:
                        DA_Min_Data_Show += 0.1;
                        if (DA_Min_Data_Show > 2)
                            DA_Min_Data_Show = 2;
                    break;
                }
            }
        break;
        //数据减
        case 9:
            //参数界面 
            if (Seg_Show_Mode == 1)
            {
                //距离
                if (Num_Show_Flag == 0)
                {
                    Dist_Show_Temp -= 10;
                    if (Dist_Show_Temp < 10)
                        Dist_Show_Temp = 10;
                }
                //温度
                else
                {
                    if (--T_Show_Temp > 200)
                        T_Show_Temp = 0;
                }
            }
            //工厂模式
            else if (Seg_Show_Mode == 2)
            {
                switch (Factory_Mode)
                {
                    //校准值设置模式
                    case 0:
                        Adjust_Num_Show -= 5;
                        if (Adjust_Num_Show < -90)
                            Adjust_Num_Show = -90;
                    break;
                    //介质设置模式
                    case 1:
                        Trans_Speed_Show -= 10;
                        if (Trans_Speed_Show < 10)
                            Trans_Speed_Show = 10;
                    break;
                    //DAC输出设置模式
                    case 2:
                        DA_Min_Data_Show -= 0.1;
                        if (DA_Min_Data_Show < 0.1)
                            DA_Min_Data_Show = 0.1;
                    break;
                }
            }
        break;
        case 17:
            Seg_Show_Mode = Num_Show_Flag = Dist_Unit_Flag = Factory_Mode = 0;
            Dist_Show_Temp = Dist_Set_Temp = 40;
            T_Show_Temp = T_Set_Temp = 30;
            Adjust_Num_Show = Adjust_Num_Set = 0;
            Trans_Speed_Show = Trans_Speed_Set = 340;
            DA_Min_Data_Show = DA_Min_Data_Set = 1;
            L1_Flag = L1_Star_Flag = Time_100ms = 0;
        break;
    }
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;              //减速
    
    switch (Seg_Show_Mode)
    {
        //测距界面
        case 0:
            Seg_Point[6] = 0;                             //熄灭小数点
            Seg_Buf[0] = (unsigned char)T / 10 % 10;      //温度十位
            Seg_Buf[1] = (unsigned char)T % 10;           //温度个位
            Seg_Point[1] = 1;                             //小数点
            Seg_Buf[2] = (unsigned int)(T * 10) % 10;     //温度小数1位
            Seg_Buf[3] = 11;                              //分隔符-
            Seg_Buf[5] = Dist / 100 % 10;                 //距离百位
            Seg_Point[5] = Dist_Unit_Flag;                //小数点
            Seg_Buf[6] = Dist / 10 % 10;                  //距离十位
            Seg_Buf[7] = Dist % 10;                       //距离个位
            for (i = 5; i < 7 && Dist_Unit_Flag == 0; i++)//高位熄灭
            {
                if (Seg_Buf[i] == 0)
                    Seg_Buf[i] = 10;
                else
                    break;
            }
        break;
        //参数界面
        case 1:
            Seg_Point[1] = Seg_Point[5] = 0;                //熄灭小数点
            for (i = 2; i < 6; i++)
                Seg_Buf[i] = 10;                            //熄灭
        
            Seg_Buf[0] = 12;                                //显示P
            if (Num_Show_Flag == 0)                         //模式1
            {
                Seg_Buf[1] = 1;                             //显示1
                Seg_Buf[6] = Dist_Show_Temp / 10 % 10;      //距离参数十位
                Seg_Buf[7] = Dist_Show_Temp % 10;           //距离参数个位
            }
            else                                            //模式2
            {
                Seg_Buf[1] = 2;                             //显示2
                Seg_Buf[6] = T_Show_Temp / 10 % 10;         //温度参数十位
                Seg_Buf[7] = T_Show_Temp % 10;              //温度参数个位
                if (Seg_Buf[6] == 0) Seg_Buf[6] = 10;       //高位熄灭
            }
        break;
        //工厂界面
        case 2:
            Seg_Buf[0] = 13;                                //显示F
            Seg_Buf[1] = Factory_Mode + 1;                  //显示模式
            switch (Factory_Mode)
            {
                //校准值设置模式
                case 0:
                    Seg_Point[6] = 0;                             //熄灭小数点
                    Seg_Buf[5] = 10;                              //熄灭
                    if (Adjust_Num_Show >= 0)                     //校准值非负
                    {
                        Seg_Buf[6] = Adjust_Num_Show / 10 % 10;   //校准值十位
                        Seg_Buf[7] = Adjust_Num_Show % 10;        //校准值个位
                        if (Seg_Buf[6] == 0)    
                            Seg_Buf[6] = 10;                      //高位熄灭
                    }
                    else                                          //校准值为负
                    {
                        Seg_Buf[6] = -Adjust_Num_Show / 10 % 10;  //校准值十位
                        Seg_Buf[7] = -Adjust_Num_Show % 10;       //校准值个位
                        if (Seg_Buf[6] == 0)                      //判断负号位置
                            Seg_Buf[6] = 11;                      //-
                        else
                            Seg_Buf[5] = 11;                      //-
                    }
                break;
                //介质设置模式
                case 1:
                    Seg_Buf[4] = Trans_Speed_Show / 1000 % 10;       //传输速度千位
                    Seg_Buf[5] = Trans_Speed_Show / 100 % 10;        //传输速度百位
                    Seg_Buf[6] = Trans_Speed_Show / 10 % 10;         //传输速度十位
                    Seg_Buf[7] = Trans_Speed_Show % 10;              //传输速度个位
                    for (i = 4; i < 7; i++)                           //高位熄灭
                    {
                        if (Seg_Buf[i] == 0)
                            Seg_Buf[i] = 10;
                        else
                            break;
                    }
                break;
                //DAC输出模式
                case 2:
                    for (i = 2; i < 6; i++)
                        Seg_Buf[i] = 10;                                        //熄灭
                    Seg_Buf[6] = (unsigned char)DA_Min_Data_Show % 10;          //DA下限个位
                    Seg_Point[6] = 1;                                           //小数点
                    Seg_Buf[7] = (unsigned char)(DA_Min_Data_Show * 10) % 10;   //DA下限小数1位
                break;
            }
        break;
    }
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;              //减速
    
    /* LED控制 */
    LED_Buf[7] = (Seg_Show_Mode == 1);  //参数界面
    if (Seg_Show_Mode == 2)             //工厂界面
    {
        L1_Flag = 1;
        LED_Buf[0] = L1_Star_Flag;
    }
}

/* 信息处理函数 */
void Infor_Deal()
{
    /* 数据输出区 */
    DA_Write(DA_Output_Data * 51);
    Relay((Dist_Set_Temp - 5 <= Dist && Dist_Set_Temp + 5 >= Dist && T <= T_Set_Temp));
    
    /* 减速 */
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 数据接收区 */
    T = DS18B20_Read_T();
    Dist_Time = Uwave_Read_D();
    Dist = (Trans_Speed_Set / 20000.0) * Dist_Time + Adjust_Num_Set;
    
    /* 数据保存区 */
    //非测距界面
    if (Seg_Show_Mode != 0) Dist_Unit_Flag = 0;     //单位归0
    //非参数界面
    if (Seg_Show_Mode != 1) Num_Show_Flag = 0;      //模式归0
    //非工厂界面
    if (Seg_Show_Mode != 2) Factory_Mode = 0;       //模式归0
    
    //距离参数保存
    if (Num_Show_Flag != 0) Dist_Set_Temp = Dist_Show_Temp;
    //距离参数保存
    else T_Set_Temp = T_Show_Temp;
    
    //校准值保存
    if (Factory_Mode != 0) Adjust_Num_Set = Adjust_Num_Show;
    //介质保存
    if (Factory_Mode != 1) Trans_Speed_Set = Trans_Speed_Show;
    //DAC输出保存
    if (Factory_Mode != 2) DA_Min_Data_Set = DA_Min_Data_Show;
    
    /* 数据处理区 */
    //DAC输出处理
    if (Dist <= 10) 
        DA_Output_Data = DA_Min_Data_Set;
    else if (Dist >= 90) 
        DA_Output_Data = 5;
    else
        DA_Output_Data = ((5 - DA_Min_Data_Set) / 80.0) * (Dist - 90) + 5;
}

/* 定时器初始化函数 */
void Timer1_Init(void)		//1毫秒@12.000MHz
{
	AUXR &= 0xBF;			//定时器时钟12T模式
	TMOD &= 0x0F;			//设置定时器模式
	TL1 = 0x18;				//设置定时初始值
	TH1 = 0xFC;				//设置定时初始值
	TF1 = 0;				//清除TF1标志
	TR1 = 1;				//定时器1开始计时
    ET1 = 1;
    EA  = 1;
}

/* 中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 200 == 0) Seg_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    if (Seg_Show_Mode == 0)
    {
        P0 = ~(unsigned char)Dist;
        P2 = P2 & 0x1f | 0x80;
        P2 &= 0x1f;
    }
    else
        LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    if (L1_Flag && ++Time_100ms == 100)
    {
        Time_100ms = 0;
        L1_Star_Flag ^= 1;
    }
    
    Seg_Deal();
}
/* 延时函数 */
void Delay750ms()		//@12.000MHz
{
    unsigned char i, j, k;
    i = 35;
    j = 51;
    k = 182;
    do
    {
        do
        {
            while (--k);
        } while (--j);
    } while (--i);
}

/* 主函数 */
void main()
{
    System_Init();
    Timer1_Init();
    DS18B20_Read_T();
    Delay750ms();
    
    while (1)
    {
        Infor_Deal();
        Key_Deal();
        LED_Deal();
    }
}
