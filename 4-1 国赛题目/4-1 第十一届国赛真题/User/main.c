/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "ds1302.h"
#include "iic.h"
#include "onewire.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;       //按键数码管LED信息处理减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
bit Seg_Show_Mode;                                              //数码管显示界面   0-数据界面   1-参数界面
unsigned char Data_Show_Mode;                                   //数据界面模式   0-时间   1-温度   2-光照
unsigned char Num_Show_Mode;                                    //参数界面模式   0-时间   1-温度   2-指示灯
unsigned char Rtc_Real_Buf[3] = {16, 59, 05};                   //实时时钟
float AD_Light, T;                                              //AD光亮，实时温度
bit Light_Flag, Light_Flag_Old;                                 //暗亮状态，暗亮旧状态   0-亮   1-暗
unsigned char Hour_Show = 17, Hour_Set = 17;                    //时参数
unsigned char T_Show = 25, T_Set = 25;                          //温度参数
unsigned char Lx_Show = 4, Lx_Set = 4;                          //指示灯参数
unsigned char L3_Record_Sec;                                    //L3记录秒
bit Light_Record_Flag = 1;                                      //亮度记录标志位

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;      //按键获取
    static unsigned char Key_UP = 0, Key_Down = 0;      //专用变量
        
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();
    Key_UP = ~Key_New & (Key_New ^ Key_Old);
    Key_Down = Key_New & (Key_New ^ Key_Old);
    Key_Old = Key_New;
    
    switch (Key_Down)
    {
        //界面切换
        case 4:
            Seg_Show_Mode ^= 1;
            if (Seg_Show_Mode == 0)
                Data_Show_Mode = 0;     //数据界面模式回归
            else
                Num_Show_Mode = 0;      //参数界面模式回归
        break;
        //界面内模式切换
        case 5:
            //数据界面模式
            if (Seg_Show_Mode == 0)
            {
                if (++Data_Show_Mode == 3)
                    Data_Show_Mode = 0;
            }
            //参数界面模式
            else
            {
                if (++Num_Show_Mode == 3)
                    Num_Show_Mode = 0;
            }
        break;
        //参数减
        case 8:
            //参数界面模式
            if (Seg_Show_Mode)
            {
                switch (Num_Show_Mode)
                {
                    //时间
                    case 0: if (--Hour_Show > 200) Hour_Show = 0; break;
                    //温度
                    case 1: if (--T_Show > 200) T_Show = 0; break;
                    //指示灯
                    case 2: if (--Lx_Show < 4) Lx_Show = 4; break;
                }
            }
        break;
        //参数加
        case 9:
            //参数界面模式
            if (Seg_Show_Mode)
            {
                switch (Num_Show_Mode)
                {
                    //时间
                    case 0: if (++Hour_Show > 23) Hour_Show = 23; break;
                    //温度
                    case 1: if (++T_Show > 99) T_Show = 99; break;
                    //指示灯
                    case 2: if (++Lx_Show > 8) Lx_Show = 8; break;
                }
            }
        break;
    }
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    //数据界面
    if (Seg_Show_Mode == 0)
    {
        switch (Data_Show_Mode)
        {
            //时间数据显示
            case 0:
                Seg_Buf[0] = Rtc_Real_Buf[0] / 10;              //时十位
                Seg_Buf[1] = Rtc_Real_Buf[0] % 10;              //时个位
                Seg_Buf[2] = 11;                                //-
                Seg_Buf[3] = Rtc_Real_Buf[1] / 10;              //分十位
                Seg_Buf[4] = Rtc_Real_Buf[1] % 10;              //分个位
                Seg_Buf[5] = 11;                                //-
                Seg_Buf[6] = Rtc_Real_Buf[2] / 10;              //秒十位
                Seg_Buf[7] = Rtc_Real_Buf[2] % 10;              //秒个位
            break;
            //温度数据显示
            case 1:
                for (i = 1; i < 5; i++)
                    Seg_Buf[i] = 10;                            //熄灭
                Seg_Buf[0] = 12;                                //C
                Seg_Buf[5] = (unsigned char)T / 10;             //温度十位
                Seg_Buf[6] = (unsigned char)T % 10;             //温度个位
                Seg_Buf[7] = (unsigned int)(T*10) % 10;         //温度小数1位
            break;
            //暗亮状态显示
            case 2:
                Seg_Buf[1] = Seg_Buf[5] = Seg_Buf[6] = 10;              //熄灭
                Seg_Buf[0] = 13;                                        //E
                Seg_Buf[2] = (unsigned char)AD_Light % 10;              //AD个位
                Seg_Buf[3] = (unsigned char)(AD_Light*10) % 10;         //AD小数1位
                Seg_Buf[4] = (unsigned int)(AD_Light*100) % 10;         //AD小数2位
                Seg_Buf[7] = Light_Flag;
            break;
        }
    }
    //参数界面
    else
    {
        for (i = 2; i < 6; i++)
            Seg_Buf[i] = 10;                    //熄灭
        Seg_Buf[0] = 14;                        //P
        Seg_Buf[1] = Num_Show_Mode + 1;         //模式编号
        switch (Num_Show_Mode)
        {
            //时间参数界面
            case 0:
                Seg_Buf[6] = Hour_Show / 10;        //时参数十位
                Seg_Buf[7] = Hour_Show % 10;        //时参数个位
            break;
            //温度参数界面
            case 1:
                Seg_Buf[6] = T_Show / 10;           //温度参数十位
                Seg_Buf[7] = T_Show % 10;           //温度参数个位
            break;
            //指示灯参数界面
            case 2:
                Seg_Buf[6] = 10;                    //熄灭
                Seg_Buf[7] = Lx_Show;               //指示灯
            break;
        }
    }
    
    //小数点处理
    Seg_Point[6] = (Seg_Show_Mode == 0 && Data_Show_Mode == 1);      //温度数据显示
    Seg_Point[2] = (Seg_Show_Mode == 0 && Data_Show_Mode == 2);      //暗亮状态显示
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;
    
    //L1
    if (Hour_Set < 8 && (Rtc_Real_Buf[0] < 8 && Rtc_Real_Buf[0] >= Hour_Set))
        LED_Buf[0] = 1;
    else if (Hour_Set > 8 && (Rtc_Real_Buf[0] < 8 || Rtc_Real_Buf[0] >= Hour_Set))
        LED_Buf[0] = 1;
    else
        LED_Buf[0] = 0;
    
    //L2
    LED_Buf[1] = (T_Set > T);
    
    //L3
    if (((L3_Record_Sec+3)%60 == Rtc_Real_Buf[2]) && Light_Record_Flag == 0)
        LED_Buf[2] = 1;
    else if (Light_Record_Flag == 1)
        LED_Buf[2] = 0;
    
    //L4-L8 互斥点亮或熄灭
    for (i = 3; i < 8 && Light_Flag; i++)
        LED_Buf[i] = (i == Lx_Set - 1);
    for (i = 3; i < 8 && !Light_Flag; i++)
        LED_Buf[i] = (i != Lx_Set - 1);
}

/* 信息处理函数 */
void Infor_Deal()
{
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 信息读取区 */
    DS1302_Read_Rtc(Rtc_Real_Buf);          //实时时钟读取
    AD_Light = AD_Read(0x41) / 51.0;        //AD亮度读取
    T = DS18B20_Read_T();                   //实时温度读取
    Light_Flag = (AD_Light < 0.8);            //亮暗状态读取
    
    /* 信息处理区 */
    //参数保存
    if (Seg_Show_Mode != 1 || Num_Show_Mode != 0)       //时参数保存
        Hour_Set = Hour_Show;
    if (Seg_Show_Mode != 1 || Num_Show_Mode != 1)       //温度参数保存
        T_Set = T_Show;
    if (Seg_Show_Mode != 1 || Num_Show_Mode != 2)       //指示灯参数保存
        Lx_Set = Lx_Show;
    
    //L3记录数据处理
    if (Light_Flag == Light_Flag_Old)
    {
        if (Light_Record_Flag == 1)
        {
            L3_Record_Sec = Rtc_Real_Buf[2];
            Light_Record_Flag = 0;
        }
    }
    else
    {
        Light_Flag_Old = Light_Flag;        //状态更新
        Light_Record_Flag = 1;
    }
}

/* 定时器初始化函数 */
void Timer1_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0xBF;		//定时器时钟12T模式
    TMOD &= 0x0F;		//设置定时器模式
    TL1 = 0x18;		//设置定时初值
    TH1 = 0xFC;		//设置定时初值
    TF1 = 0;		//清除TF1标志
    TR1 = 1;		//定时器1开始计时
    EA  = 1;
    ET1 = 1;
}

/* 中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 100 == 0) Seg_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
}

/* 主函数 */
void main()
{
    System_Init();
    Timer1_Init();
    DS18B20_Read_T();
    DS1302_Write_Rtc(Rtc_Real_Buf);
    AD_Read(0x41);
    
    while (1)
    {
        Key_Deal();
        Seg_Deal();
        LED_Deal();
        Infor_Deal();
    }
}

