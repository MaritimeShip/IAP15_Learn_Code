/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "iic.h"
#include "onewire.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                                 //减速扫描专用变量
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;       //按键数码管LED信息处理减速标志位
unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};            //数码管显示缓冲区
unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};                  //数码管小数点显示缓冲区
unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};                    //LED显示缓冲区
unsigned char i;                                                        //循环专用变量

/* 变量创建区 */
unsigned char Seg_Show_Mode;                                        //数码管显示界面   0-数据显示界面   1-数据回显界面   2-电压阈值设置界面
unsigned char Data_Show_Mode;                                       //数据显示界面   0-电压显示界面   1-频率显示界面   2-温度显示界面
unsigned char Data_Return_Show_Mode;                                //数据回显界面   0-电压回显界面   1-频率回显界面   2-温度回显界面
unsigned int Freq_Time_1000ms, Freq, Freq_Temp;                     //频率1000ms计时，实时频率
unsigned char Freq_Record[2];                                       //频率数据记录
float T, T_Temp;                                                    //实时温度
unsigned char T_Record[2];                                          //温度数据记录
float AD_Data, AD_Data_Temp, AD_Data_Show = 0.1, AD_Data_Set = 0.1; //AD数据
unsigned char AD_Data_Record;                                       //AD数据记录
bit V_S6_Flag;                                                      //电压阈值S6按下标志位
unsigned int Time_800ms;                                            //0.8s计时
unsigned char V_S6_Time_100ms;                                      //S6_100ms计时
unsigned char L8_Time_200ms;                                        //L8_200ms计时
bit L8_Flag, L8_Star_Flag;                                          //L8标志位 L8闪烁标志位

/* 延时函数 */
void Delay10ms()		//@12.000MHz
{
    unsigned char i, j;

    i = 117;
    j = 184;
    do
    {
        while (--j);
    } while (--i);
}

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;
    static unsigned char Key_UP = 0, Key_Down = 0;
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;
    
    Key_New = Key_Read();
    Key_UP = ~Key_New & (Key_New ^ Key_Old);
    Key_Down = Key_New & (Key_New ^ Key_Old);
    Key_Old = Key_New;
    
    //电压阈值S6
    if (Key_Old == 6 && Seg_Show_Mode == 2)
        V_S6_Flag = 1;
    else
    {
        V_S6_Flag = 0;
        if (Time_800ms > 0 && Time_800ms < 800)
            AD_Data_Show += 0.1;
        if (AD_Data_Show > 5)
            AD_Data_Show = 5;
        Time_800ms = 0;
    }
    
    switch (Key_Down)
    {
        //小界面切换
        case 4:
            if (Seg_Show_Mode == 0 && ++Data_Show_Mode == 3) Data_Show_Mode = 0;
            if (Seg_Show_Mode == 1 && ++Data_Return_Show_Mode == 3) Data_Return_Show_Mode = 0;
        break;
        //数据保存
        case 5:
            //AD数据记录
            AD_Data_Temp = AD_Data;
            AD_Data_Record = AD_Data * 10;
            EEPROM_Write(&AD_Data_Record, 0, 1);        //AD数据写入
            Delay10ms();                                //不能连续写入，需加入延时
            //频率数据记录
            Freq_Record[0] = (unsigned char)(Freq / 256);
            Freq_Record[1] = (unsigned char)(Freq % 256);
            Freq_Temp = Freq;
            EEPROM_Write(Freq_Record, 16, 2);            //频率数据写入
            Delay10ms();                                 //不能连续写入，需加入延时
            //温度数据记录
            T_Temp = T;
            T_Record[0] = (unsigned char)T;
            T_Record[1] = (unsigned int)(T*100) % 100;
            EEPROM_Write(T_Record, 32, 2);              //温度数据写入
        break;
        //回显界面 & 电压阈值
        case 6:
            if (Seg_Show_Mode != 2) Seg_Show_Mode ^= 1;
        break;
        //电压阈值界面
        case 7:
            if (Seg_Show_Mode != 2) Seg_Show_Mode = 2;
            else Seg_Show_Mode = 0;
        break;
    }
    
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;
    
    switch (Seg_Show_Mode)
    {
        //数据显示界面
        case 0:
            switch (Data_Show_Mode)
            {
                //电压显示界面
                case 0:
                    for (i = 1; i < 6; i++)
                        Seg_Buf[i] = 10;                                //熄灭
                    Seg_Buf[0] = 12;                                    //U
                    Seg_Buf[6] = (unsigned char)AD_Data % 10;           //AD个位
                    Seg_Buf[7] = (unsigned char)(AD_Data*10) % 10;      //AD小数1位
                break;
                //频率显示界面
                case 1:
                    Seg_Buf[1] = Seg_Buf[2] = 10;       //熄灭
                    Seg_Buf[0] = 13;                    //F
                    Seg_Buf[3] = Freq / 10000 % 10;     //频率万位
                    Seg_Buf[4] = Freq / 1000 % 10;      //频率千位
                    Seg_Buf[5] = Freq / 100 % 10;       //频率百位
                    Seg_Buf[6] = Freq / 10 % 10;        //频率十位
                    Seg_Buf[7] = Freq % 10;             //频率个位
                    for (i = 3; i < 7; i++)             //高位熄灭
                    {
                        if (Seg_Buf[i] == 0)
                            Seg_Buf[i] = 10;
                        else
                            break;
                    }
                break;
                //温度显示界面
                case 2:
                    for (i = 1; i < 4; i++)
                        Seg_Buf[i] = 10;                            //熄灭
                    Seg_Buf[0] = 14;                                //C
                    Seg_Buf[4] = (unsigned char)T / 10;             //温度十位
                    Seg_Buf[5] = (unsigned char)T % 10;             //温度个位
                    Seg_Buf[6] = (unsigned int)(T*10) % 10;         //温度小数1位
                    Seg_Buf[7] = (unsigned int)(T*100) % 10;        //温度小数2位
                break;
            }
        break;
        //数据回显界面
        case 1:
            Seg_Buf[0] = 15;                                    //显示H
            Seg_Buf[1] = 12 + Data_Return_Show_Mode;            //U F C
            switch (Data_Return_Show_Mode)
            {
                //电压回显界面
                case 0:
                    for (i = 2; i < 6; i++)
                        Seg_Buf[i] = 10;                                     //熄灭
                    Seg_Buf[6] = (unsigned char)AD_Data_Temp % 10;           //回显AD个位
                    Seg_Buf[7] = (unsigned char)(AD_Data_Temp*10) % 10;      //回显AD小数1位
                break;
                //频率回显界面
                case 1:
                    Seg_Buf[2] = 10;            //熄灭
                    Seg_Buf[3] = Freq_Temp / 10000 % 10;     //回显频率万位
                    Seg_Buf[4] = Freq_Temp / 1000 % 10;      //回显频率千位
                    Seg_Buf[5] = Freq_Temp / 100 % 10;       //回显频率百位
                    Seg_Buf[6] = Freq_Temp / 10 % 10;        //回显频率十位
                    Seg_Buf[7] = Freq_Temp % 10;             //回显频率个位
                    for (i = 3; i < 7; i++)                  //回显高位熄灭
                    {
                        if (Seg_Buf[i] == 0)
                            Seg_Buf[i] = 10;
                        else
                            break;
                    }
                break;
                //温度回显界面
                case 2:
                    for (i = 2; i < 4; i++)
                        Seg_Buf[i] = 10;                                 //熄灭
                    Seg_Buf[4] = (unsigned char)T_Temp / 10;             //回显温度十位
                    Seg_Buf[5] = (unsigned char)T_Temp % 10;             //回显温度个位
                    Seg_Buf[6] = (unsigned int)(T_Temp*10) % 10;         //回显温度小数1位
                    Seg_Buf[7] = (unsigned int)(T_Temp*100) % 10;        //回显温度小数2位
                break;
            }
        break;
        //电压阈值设置界面
        case 2:
            for (i = 1; i < 6; i++)
                Seg_Buf[i] = 10;                                        //熄灭
            Seg_Buf[0] = 16;                                            //P
            Seg_Buf[6] = (unsigned char)AD_Data_Show % 10;              //AD个位
            Seg_Buf[7] = (unsigned char)(AD_Data_Show*10) % 10;         //AD小数1位
        break;
    }
    
    /* 小数点处理 */
    Seg_Point[6] = ((Seg_Show_Mode == 0 && Data_Show_Mode == 0) || (Seg_Show_Mode == 1 && Data_Return_Show_Mode == 0) || Seg_Show_Mode == 2);
    Seg_Point[5] = ((Seg_Show_Mode == 0 && Data_Show_Mode == 2) || (Seg_Show_Mode == 1 && Data_Return_Show_Mode == 2));
    
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;
    
    //L1 L2 L3
    for (i = 0; i < 3; i++)
        LED_Buf[i] = (i == Data_Show_Mode && Seg_Show_Mode == 0);
    
    //L8
    L8_Flag = (AD_Data_Set < AD_Data);
    if (L8_Flag)
        LED_Buf[7] = L8_Star_Flag;
    else
        LED_Buf[7] = L8_Time_200ms = 0;
}

/* 信息处理函数 */
void Infor_Deal()
{
    /* 减速 */
    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;
    
    /* 信息读取区 */
    T = DS18B20_Read_T();
    AD_Data = AD_Read(0x43) / 51.0;
    
    /* 信息处理区 */
    //界面回归
    if (Seg_Show_Mode != 0) Data_Show_Mode = 0;
    if (Seg_Show_Mode != 1) Data_Return_Show_Mode = 0;
    //电压保存
    if (Seg_Show_Mode != 2) AD_Data_Set = AD_Data_Show;
}

/* 定时器初始化函数 */
void Timer0_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0x7F;		//定时器时钟12T模式
    TMOD &= 0xF0;		//设置定时器模式
    TMOD |= 0x05;
    TL0 = 0x00;		//设置定时初值
    TH0 = 0x00;		//设置定时初值
    TF0 = 0;		//清除TF0标志
    TR0 = 1;		//定时器0开始计时
}
void Timer1_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0xBF;		//定时器时钟12T模式
    TMOD &= 0x0F;		//设置定时器模式
    TL1 = 0x18;		//设置定时初值
    TH1 = 0xFC;		//设置定时初值
    TF1 = 0;		//清除TF1标志
    TR1 = 1;		//定时器1开始计时
    EA  = 1;
    ET1 = 1;
}

/* 中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 100 == 0) Seg_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    //频率获取
    if (++Freq_Time_1000ms == 1000)
    {
        Freq_Time_1000ms = 0;
        Freq = TH0 << 8 | TL0;
        TH0 = TL0 = 0;
    }
    //参数加
    if (V_S6_Flag && ++Time_800ms >= 800)
    {
        Time_800ms = 800;
        if (++V_S6_Time_100ms == 100)
        {
            V_S6_Time_100ms = 0;
            AD_Data_Show += 0.1;
            if (AD_Data_Show > 5)
                AD_Data_Show = 5;
        }
    }
    //L8
    if (L8_Flag && ++L8_Time_200ms == 200)
    {
        L8_Time_200ms = 0;
        L8_Star_Flag ^= 1;
    }
    
    Seg_Deal();
}

/* 主函数 */
void main()
{
    unsigned char PIN = 0;
    
    System_Init();
    Timer1_Init();
    Timer0_Init();
    DS18B20_Read_T();
    AD_Read(0x43);
    
    /* PIN验证 */
    EEPROM_Read(&PIN, 56, 1);
    if (PIN == 66)                      //PIN正确
    {
        EEPROM_Read(&AD_Data_Record, 0, 1);                     //AD数据读出
        AD_Data_Temp = AD_Data_Record / 10.0;                   //AD数据获取
        
        EEPROM_Read(Freq_Record, 16, 2);                        //频率数据读出
        Freq_Temp = Freq_Record[0] << 8 | Freq_Record[1];       //频率数据获取
        
        EEPROM_Read(T_Record, 32, 2);                           //温度数据读出
        T_Temp  = T_Record[0] + T_Record[1] / 100.0;            //温度数据获取
    }
    else                                //PIN错误
    {
        PIN = 66;
        EEPROM_Write(&PIN, 56, 1);
    }

    while (1)
    {
        Key_Deal();
        LED_Deal();
        Infor_Deal();
    }
}

