/* 头文件声明区 */
#include <STC15F2K60S2.H>
#include <string.h>
#include <stdio.h>
#include "Init.h"
#include "Key.h"
#include "Seg.h"
#include "LED.h"
#include "Uwave.h"
#include "Uart.h"
#include "iic.h"
#include "onewire.h"

/* 基础变量创建区 */
unsigned int Slow_Down;                                             //减速扫描专用变量
bit Key_Slow_Flag, Seg_Slow_Flag, LED_Slow_Flag, Infor_Slow_Flag;   //按键数码管LED信息处理函数减速标志位
idata unsigned char Seg_Buf[8] = {10, 10, 10, 10, 10, 10, 10, 10};  //数码管显示缓冲区
idata unsigned char Seg_Point[8] = {0, 0, 0, 0, 0, 0, 0, 0};        //数码管小数点显示缓冲区
idata unsigned char LED_Buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};          //LED显示缓冲区
unsigned char i;                                                    //循环专用变量

/* 变量创建区 */
bit Seg_Show_Mode;                                      //数码管显示界面   0-数据显示界面   1-参数显示界面
bit Num_Show_Mode;                                      //参数显示界面   0-温度参数   1-距离参数
unsigned char Data_Show_Mode;                           //数据显示界面   0-温度数据   1-距离数据   2-变更次数
unsigned char Dist;                                     //实时距离
unsigned char Uart_Rx_Index;                            //串口接收指针
unsigned char Uart_Rx_Buf[10];                          //串口接收缓冲区
unsigned char Uart_Rx_Time;                             //串口接收时间
bit Uart_Rx_Flag;                                       //串口接收标志位
float T, DA_Data;                                       //实时温度，DA输出数据
unsigned int Change_Num, Change_Num_Old;                //更变次数
unsigned char Chang_Num_Char[2];                        //改变次数char
unsigned char T_Temp_Show = 30, T_Temp_Set = 30;        //温度参数
unsigned char Dist_Temp_Show = 35, Dist_Temp_Set = 35;  //距离参数
unsigned int S12_Time;                                  //S12定时
bit S12_Flag;                                           //S12按下标志位
unsigned int S13_Time;                                  //S13定时
bit S13_Flag;                                           //S13按下标志位
bit DA_Output_Flag = 1;                                 //DA输出标志位
bit S13_Long_Flag, S12_Long_Flag;                       //S12, S12长按标志位

/* 按键处理函数 */
void Key_Deal()
{
    static unsigned char Key_New = 0, Key_Old = 0;      //按键捕获
    static unsigned char Key_UP = 0, Key_Down = 0;      //专用变量
    
    if (Key_Slow_Flag) return;
    Key_Slow_Flag = 1;                  //减速
    
    Key_New = Key_Read();                       //获取键码
    Key_UP = ~Key_New & (Key_New ^ Key_Old);    //捕获上升沿
    Key_Down = Key_New & (Key_New ^ Key_Old);   //捕获下降沿
    Key_Old = Key_New;                          //辅助扫描
    
    //S12长按
    if (Key_Old == 12)
    {
        S12_Flag = 1;
        if (S12_Time > 1000)
        {
            S12_Long_Flag = 1;
            Change_Num = 0;
            Chang_Num_Char[0] = Chang_Num_Char[1] = 0;
            EEPROM_Write(Chang_Num_Char, 0, 2);
        }
    }
    else
        S12_Flag = S12_Time = 0;
    
    //S13长按
    if (Key_Old == 13)
    {
        S13_Flag = 1;
        if (S13_Time > 1000)
        {
            S13_Long_Flag = 1;
            DA_Output_Flag ^= 1;
            S13_Flag = S13_Time = 0;
        }
    }
    else
        S13_Flag = S13_Time = 0;
    
    switch (Key_UP)
    {
        //界面切换
        case 13:
            if (S13_Long_Flag == 1)
                S13_Long_Flag = 0;
            else
            {
                Seg_Show_Mode ^= 1;
                if (Seg_Show_Mode == 0)
                    Data_Show_Mode = 0;
                else
                    Num_Show_Mode = 0;
            }
        break;
        //小界面切换
        case 12:
            if (S12_Long_Flag == 1)
                S12_Long_Flag = 0;
            else
            {
                if (Seg_Show_Mode == 0)
                {
                    if (++Data_Show_Mode == 3)
                        Data_Show_Mode = 0;
                }
                else
                    Num_Show_Mode ^= 1;
            }
        break;
        //参数减
        case 16:
            //参数界面
            if (Seg_Show_Mode)
            {
                //温度参数
                if (Num_Show_Mode == 0)
                {
                    T_Temp_Show -= 2;
                    if (T_Temp_Show > 200)
                        T_Temp_Show = 0;
                }
                else
                {
                    Dist_Temp_Show -= 5;
                    if (Dist_Temp_Show > 200)
                        Dist_Temp_Show = 0;
                }
            }
        break;
        //参数加
        case 17:
            //参数界面
            if (Seg_Show_Mode)
            {
                //温度参数
                if (Num_Show_Mode == 0)
                {
                    T_Temp_Show += 2;
                    if (T_Temp_Show > 99)
                        T_Temp_Show = 99;
                }
                else
                {
                    Dist_Temp_Show += 5;
                    if (Dist_Temp_Show > 99)
                        Dist_Temp_Show = 99;
                }
            }
        break;
    }
}

/* 数码管处理函数 */
void Seg_Deal()
{
    if (Seg_Slow_Flag) return;
    Seg_Slow_Flag = 1;                  //减速
    
    //数据显示界面
    if (Seg_Show_Mode == 0)
    {
        switch (Data_Show_Mode)
        {
            //温度数据
            case 0:
                for (i = 1; i < 4; i++) Seg_Buf[i] = 10;        //熄灭
                Seg_Buf[0] = 12;                                //显示F
                Seg_Buf[4] = (unsigned char)T / 10 % 10;        //温度十位
                Seg_Buf[5] = (unsigned char)T % 10;             //温度个位
                Seg_Buf[6] = (unsigned int)(T * 10) % 10;       //小数点1位
                Seg_Buf[7] = (unsigned int)(T * 100) % 10;      //小数点2位
            break;
            //距离数据
            case 1:
                for (i = 1; i < 6; i++) Seg_Buf[i] = 10;        //熄灭
                Seg_Buf[0] = 13;                                //显示L
                Seg_Buf[6] = Dist / 10 % 10;                    //距离十位
                Seg_Buf[7] = Dist % 10;                         //距离个位
            break;
            //变更次数
            case 2:
                for (i = 1; i < 3; i++) Seg_Buf[i] = 10;        //熄灭
                Seg_Buf[0] = 0;                                 //显示Q
                Seg_Buf[3] = Change_Num / 10000 % 10;           //更改次数万位
                Seg_Buf[4] = Change_Num / 1000 % 10;            //更改次数千位
                Seg_Buf[5] = Change_Num / 100 % 10;             //更改次数百位
                Seg_Buf[6] = Change_Num / 10 % 10;              //更改次数十位
                Seg_Buf[7] = Change_Num % 10;                   //更改次数个位
                for (i = 3; i < 7; i++)                         //高位熄灭
                {
                    if (Seg_Buf[i] == 0)
                        Seg_Buf[i] = 10;
                    else
                        break;
                }
            break;
        }
    }
    //参数显示界面
    else
    {
        Seg_Buf[1] = Seg_Buf[2] = Seg_Buf[4] = Seg_Buf[5] = 10;     //熄灭
        Seg_Buf[0] = 5;                                             //显示S
        Seg_Buf[3] = Num_Show_Mode;                                 //显示编号
        Seg_Buf[3] += 4;
        //温度参数
        if (Num_Show_Mode == 0)
        {
            Seg_Buf[6] = T_Temp_Show / 10;
            Seg_Buf[7] = T_Temp_Show % 10;
        }
        //距离参数
        else
        {
            Seg_Buf[6] = Dist_Temp_Show / 10;
            Seg_Buf[7] = Dist_Temp_Show % 10;
        }
    }
    
    //小数点处理
    Seg_Point[5] = (Data_Show_Mode == 0 && Seg_Show_Mode == 0);
    Seg_Point[0] = (Data_Show_Mode == 2 && Seg_Show_Mode == 0);
}

/* LED处理函数 */
void LED_Deal()
{
    if (LED_Slow_Flag) return;
    LED_Slow_Flag = 1;                  //减速
    
    //L1
    LED_Buf[0] = (T > T_Temp_Set);
    
    //L2
    LED_Buf[1] = (Dist < Dist_Temp_Set);
    
    //L3
    LED_Buf[2] = DA_Output_Flag;
}

/* 信息处理函数 */
void Infor_Deal()
{
    /* 信息输出区 */
    if (DA_Output_Flag)
        DA_Write(DA_Data * 51);
    else
        DA_Write(0);

    if (Infor_Slow_Flag) return;
    Infor_Slow_Flag = 1;                //减速
    
    /* 信息获取区 */
    Dist = Uwave_Read_Dist();
    T = DS18B20_Read_T();
    
    /* 信息处理区 */
    //数据保存
    if (Seg_Show_Mode == 0)
    {
        if (T_Temp_Show != T_Temp_Set)
        {
            T_Temp_Set = T_Temp_Show;
            Change_Num++;
        }
        if (Dist_Temp_Show != Dist_Temp_Set)
        {
            Dist_Temp_Set = Dist_Temp_Show;
            Change_Num++;
        }
    }
    if (Change_Num_Old != Change_Num)
    {
        Change_Num_Old = Change_Num;
        Chang_Num_Char[0] = Change_Num / 256;
        Chang_Num_Char[1] = Change_Num % 256;
        EEPROM_Write(Chang_Num_Char, 0, 2);
    }
    
    //DA输出
    DA_Data = (Dist <= Dist_Temp_Set) ? 2 : 4;
}

/* 串口处理函数 */
void Uart_Deal()
{
    if (!Uart_Rx_Index) return;
    
    if (Uart_Rx_Time > 10)
    {
        Uart_Rx_Time = Uart_Rx_Flag = 0;
        /* 逻辑处理 */
        switch (Uart_Rx_Index)
        {
            //查询数据指令
            case 4:
                if (Uart_Rx_Buf[0] == 'S' && Uart_Rx_Buf[1] == 'T' && Uart_Rx_Buf[2] == '\r' && Uart_Rx_Buf[3] == '\n')
                    printf("$%d,%.2f\r\n", (unsigned int)Dist, T);
                else
                    printf("EEROR!\r\n");
            break;
            //查询参数指令
            case 6:
                if (Uart_Rx_Buf[0] == 'P' && Uart_Rx_Buf[1] == 'A' && Uart_Rx_Buf[2] == 'R' && Uart_Rx_Buf[3] == 'A' && Uart_Rx_Buf[4] == '\r' && Uart_Rx_Buf[5] == '\n')
                    printf("#%d,%d\r\n", (unsigned int)Dist_Temp_Set, (unsigned int)T_Temp_Set);
                else
                    printf("EEROR!\r\n");
            break;
            //错误指令
            default:
                printf("EEROR!\r\n");
            break;
        }
        memset(Uart_Rx_Buf, 0, Uart_Rx_Index);
        Uart_Rx_Index = 0;
    }
}

/* 定时器初始化函数 */
void Timer1_Init(void)		//1毫秒@12.000MHz
{
    AUXR &= 0xBF;			//定时器时钟12T模式
    TMOD &= 0x0F;			//设置定时器模式
    TL1 = 0x18;				//设置定时初始值
    TH1 = 0xFC;				//设置定时初始值
    TF1 = 0;				//清除TF1标志
    TR1 = 1;				//定时器1开始计时
    EA  = 1;
    ET1 = 1;
}

/* 中断服务函数 */
void Timer1_Serve() interrupt 3
{
    if (++Slow_Down == 400) Slow_Down = Infor_Slow_Flag = 0;
    if (Slow_Down % 10 == 0) Key_Slow_Flag = 0;
    if (Slow_Down % 20 == 0) LED_Slow_Flag = 0;
    if (Slow_Down % 100 == 0) Seg_Slow_Flag = 0;
    Seg_Show(Slow_Down%8, Seg_Buf[Slow_Down%8], Seg_Point[Slow_Down%8]);
    LED_Show(Slow_Down%8, LED_Buf[Slow_Down%8]);
    
    if (Uart_Rx_Flag && ++Uart_Rx_Time == 10)
        Uart_Rx_Time = 11;
    
    if (S12_Flag && ++S12_Time >= 1000)
        S12_Time = 1001;


    if (S13_Flag && ++S13_Time >= 1000)
        S13_Time = 1001;
    
    Seg_Deal();
}
void Uart_Serve() interrupt 4
{
    if (RI == 1)
    {
        Uart_Rx_Time = RI = 0;
        Uart_Rx_Flag = 1;
        Uart_Rx_Buf[Uart_Rx_Index++] = SBUF;
    }
    if (Uart_Rx_Index >= 7)
    {
        memset(Uart_Rx_Buf, 0, Uart_Rx_Index);
        Uart_Rx_Index = 0;
    }
    
}

/* 延时函数 */
void Delay750ms()		//@12.000MHz
{
    unsigned char i, j, k;
    i = 35;
    j = 51;
    k = 182;
    do
    {
        do
        {
            while (--k);
        } while (--j);
    } while (--i);
}

/* 主函数 */
void main()
{
    System_Init();
    Timer1_Init();
    Uart_Init();
    DS18B20_Read_T();
    Delay750ms();
    while (1)
    {
        Infor_Deal();
        Key_Deal();
        LED_Deal();
        Uart_Deal();
    }
}



