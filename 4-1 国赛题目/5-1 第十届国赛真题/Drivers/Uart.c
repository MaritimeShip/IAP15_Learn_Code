#include <STC15F2K60S2.H>
#include <stdio.h>
#include "Uart.h"


void Uart_Init(void)     //4800bps@12.000MHz
{
    SCON = 0x50;		//8位数据,可变波特率
    AUXR |= 0x01;		//串口1选择定时器2为波特率发生器
    AUXR &= 0xFB;		//定时器2时钟为Fosc/12,即12T
    T2L = 0xCC;		//设定定时初值
    T2H = 0xFF;		//设定定时初值
    AUXR |= 0x10;		//启动定时器2    
    EA = 1;
    ES = 1;
}

extern char putchar (char ch)
{
    SBUF = ch;
    while (TI == 0);
    TI = 0;
    return ch;
}



